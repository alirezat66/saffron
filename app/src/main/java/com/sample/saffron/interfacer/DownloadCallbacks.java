package com.sample.saffron.interfacer;

import com.sample.saffron.Database.PrivateMessageDataBase;

/**
 * Created by Abderrahim El imame on 7/28/16.
 *
 * @Email : abderrahim.elimame@gmail.com
 * @Author : https://twitter.com/bencherif_el
 */

public interface DownloadCallbacks {
    void onUpdate(int percentage, String mid);

    void onError(String mid);

    void onFinish(String mid);

}
