package com.sample.saffron.interfacer;

/**
 * Created by Aseman on 12/8/2015.
 */
public interface EventData {

    public String toJsonString();

}
