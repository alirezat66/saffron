package com.sample.saffron.helper;

/**
 * Created by alireza on 9/1/2017.
 */

public interface NetworkListener {
    void onNetworkConnectionChanged(boolean isConnecting, boolean isConnected);
}
