package com.sample.saffron.helper;



import android.view.View;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Payam on 8/10/15.
 */
public class MessengerBus {

    String message;
    String mes;
    JSONArray jsonArray;
    String mid;
    String sender;
    Object obj;
    ArrayList<String>mids;
    int maxchunk;
    int initial;
    View view;
    public String getMes() {
        return mes;
    }

    public Object getObj() {
        return obj;
    }

    public MessengerBus(String message, View view){
        this.message=message;
        this.view=view;
    }

    public View getView() {
        return view;
    }
    public MessengerBus(String message){
        this.message = message;
    }
    public MessengerBus(String message, String mes) {
        this.message = message;
        this.mes = mes;
    }
    public MessengerBus(String message, String mes,ArrayList<String>mids) {
        this.message = message;
        this.mes = mes;
        this.mids=mids;
    }

    public ArrayList<String> getMids() {
        return mids;
    }

    public MessengerBus(String message, Object value){
        this.message=message;
        this.obj=value;
    }
    public MessengerBus(String message,int position){
        this.message=message;
        this.position=position;
    }
    public String getSender() {
        return sender;
    }

    public MessengerBus(String message, String mes, String sender) {

        this.message = message;
        this.mes = mes;
        this.sender = sender;
    }

    public String getMid() {
        return mid;
    }

    public int getMaxchunk() {
        return maxchunk;
    }

    public int getInitial() {
        return initial;
    }

    int position;

    public MessengerBus(String message, int position, JSONArray conversation) {
        this.message = message;
        this.position = position;
        this.jsonArray = conversation;
    }

    public MessengerBus(String message, int position, String mid,int maxchunk,int initial) {
        this.message = message;
        this.position = position;
        this.mid = mid;
        this.maxchunk = maxchunk;
        this.initial = initial;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public String getMessage() {
        return message;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public int getPosition() {
        return position;
    }
}
