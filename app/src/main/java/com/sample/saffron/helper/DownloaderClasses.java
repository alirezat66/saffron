package com.sample.saffron.helper;

import android.graphics.PorterDuff;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import com.sample.saffron.R;
import com.sample.saffron.interfacer.DownloadCallbacks;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;

/**
 * Created by alireza on 8/15/2017.
 */

public class DownloaderClasses  {
    DownloadCallbacks callbacks;
    Uri url;
    String mid;

    public DownloaderClasses(Uri url, String mid) {
        this.url = url;
        this.mid = mid;


    }

    public DownloadCallbacks getCallbacks() {
        return callbacks;
    }

    public void setCallbacks(DownloadCallbacks callbacks) {
        this.callbacks = callbacks;
    }

}
