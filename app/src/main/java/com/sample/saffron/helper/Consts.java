package com.sample.saffron.helper;

import android.Manifest;
import android.app.Activity;
import android.os.Environment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import com.sample.saffron.activities.AppController;

import java.io.File;

/**
 * Created by alireza on 1/21/2017.
 */

public class Consts {
    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }
    public interface ACTION {

        public static String STARTFOREGROUND_ACTION = "com.truiton.foregroundservice.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.truiton.foregroundservice.action.stopforeground";
    }
    public static File ImagesDir =  new File(
            Environment.getExternalStoragePublicDirectory(
                    Environment.getRootDirectory().getParent()
            ),
            "/Saffron/Images"
    );
    public static File VideoDir =  new File(
            Environment.getExternalStoragePublicDirectory(
                    Environment.getRootDirectory().getParent()
            ),
            "/Saffron/Videos"
    );
    public static File MusicDir =  new File(
            Environment.getExternalStoragePublicDirectory(
                    Environment.getRootDirectory().getParent()
            ),
            "/Saffron/Audios"
    );
    public static File Documents =  new File(
            Environment.getExternalStoragePublicDirectory(
                    Environment.getRootDirectory().getParent()
            ),
            "/Saffron/Documents"
    );
   // public static String serverIp = "192.168.1.11";
//    public static String serverIp = "94.182.215.114";
    public static final int REQUEST_CODE_PERMISSION = 218;
    public static final int RESULT_LOAD_IMAGE = 113;


    public static String[] mPermission = {
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA




    };
    public static String Server_Ip="130.185.73.17";
    public static void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }
    public static void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) AppController.getCurrentActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = AppController.getCurrentActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(AppController.getCurrentActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    /**
     * images size
     */
    public static final int NOTIFICATIONS_IMAGE_SIZE = 150;
    public static final int ROWS_IMAGE_SIZE = 90;
    public static final int PROFILE_PREVIEW_IMAGE_SIZE = 500;
    public static final int PROFILE_PREVIEW_BLUR_IMAGE_SIZE = 50;
    public static final int PROFILE_IMAGE_SIZE = 500;
    public static final int SETTINGS_IMAGE_SIZE = 100;
    public static final int EDIT_PROFILE_IMAGE_SIZE = 500;
    public static final int MESSAGE_IMAGE_SIZE = 300;
    public static final int PRE_MESSAGE_IMAGE_SIZE = 40;
    public static final int FULL_SCREEN_IMAGE_SIZE = 640;
    public static final int BLUR_RADIUS = 1;


}
