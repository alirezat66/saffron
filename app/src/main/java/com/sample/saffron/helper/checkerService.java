package com.sample.saffron.helper;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import com.sample.saffron.activities.AppController;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by alireza on 9/1/2017.
 */

public class checkerService extends IntentService {
    Context context;
    NetworkListener resultReceiver;
    Timer timer = new Timer();
    MyTimerTask timerTask;
    public checkerService() {
        super(checkerService.class.getName());
    }
    class MyTimerTask extends TimerTask {
        public MyTimerTask() {
            Bundle bundle = new Bundle();
            bundle.putString("start", "Timer Started....");
        }

        public void run() {
            int a;
            if (checkerService.isOnline() ) {
                a = 1;
            } else  {
                a = 0;
            }

            if(a==1) {
                resultReceiver.onNetworkConnectionChanged(true, true);
            }else {
                resultReceiver.onNetworkConnectionChanged(false,false);
            }

        }
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        context= AppController.getCurrentContext();
        this.resultReceiver = (NetworkListener) this.context;
        this.timerTask = new MyTimerTask();
        this.timer.scheduleAtFixedRate(this.timerTask, 100, 5000);
    }



    public static boolean isOnline() {
        try {
            if (Runtime.getRuntime().exec("ping -c 1 192.168.1.10").waitFor() == 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


}