package com.sample.saffron.helper;

import com.google.gson.Gson;
import com.sample.saffron.entity.userData.SignInResponseData;

import org.json.JSONObject;

/**
 * Created by alireza on 5/2/2017.
 */

public class JsonToAns  {
    public static SignInResponseData getSignInAsn(JSONObject object){
        String objStr = object.toString();
        Gson gson = new Gson();
        SignInResponseData resp = gson.fromJson(objStr,SignInResponseData.class);
        return resp;
    }
}
