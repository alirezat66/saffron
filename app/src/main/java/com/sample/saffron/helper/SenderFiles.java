package com.sample.saffron.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.JsonObject;
import com.sample.saffron.Database.DatabaseHandler;
import com.sample.saffron.Database.PrivateMessageDataBase;
import com.sample.saffron.activities.*;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.client.Security;
import com.sample.saffron.entity.messageData.BinaryMessageRequestData;
import com.sample.saffron.entity.messageData.DataChunkData;
import com.sample.saffron.entity.messageData.PrivateMessageData;

import org.java_websocket.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by alireza on 6/28/2017.
 */

public class SenderFiles {

    public SenderFiles() {

    }
    public void MakeMessage(String picturePath, String to, @Nullable PrivateMessageDataBase oldpm,String type,String replyto) {
        Context context = AppController.getCurrentContext();
        BinaryMessageRequestData message = new BinaryMessageRequestData();
        message.username = PreferencesData.getString(context, "userName");
        message.from = PreferencesData.getString(context, "userName");
        message.to = to;
        message.replyto = replyto;
        message.type = "private";
        message.btype = type;

        File myfile = new File(Uri.parse(picturePath).getPath());

        if(type.equals("photo")){

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            try {
               Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(myfile), null, options);
               Bitmap resizedBitmap= AppHelper.resizeBitmap(bitmap,800);
                if(resizedBitmap!=null){
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(myfile));
                    resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, os);
                    os.close();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        message.size = myfile.length();
        Log.e("myapp","size of file  ="+message.size);
        message.replyto = "";
        if (oldpm == null) {
            message.timestamp = (System.currentTimeMillis() / 1000) + "";
            message.mid = com.sample.saffron.activities.Helper.sha256(message.btype + message.from + message.to + message.timestamp);
        } else {
            message.timestamp = oldpm.getTimestamp() + "";
            message.mid = oldpm.getMid();
        }
        String filename = picturePath.substring(picturePath.lastIndexOf("/") + 1);
        message.filename = filename;





        FileManager fileManager = new FileManager();
        String textFileName = message.getMid() + "." + picturePath.substring(picturePath.lastIndexOf(".") + 1);
        File destiny = new File(fileManager.makeFile(type).toString() + "/" + textFileName);

        boolean copyTrue=false;
        try {
            fileManager.copy(myfile, destiny);
            copyTrue=true;

        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject data2 = new JSONObject();
        JSONObject packet = new JSONObject();

        try
        {
            data2.put("from", message.getFrom());
            data2.put("to", to);

            data2.put("type", message.getType());
            data2.put("btype", message.getBtype());
            data2.put("timestamp", message.getTimestamp());
            data2.put("replyto", replyto);
            data2.put("size", message.getSize());
            data2.put("filename", message.getFilename());
            data2.put("mid", message.getMid());
            data2.put("orgFileName",filename);
            packet.put("data", data2);
            packet.put("tc", message.getTimestamp());
            packet.put("signature", PreferencesData.getString(context, "signature"));

            long size =message.getSize();


            //end making packet
            if(oldpm==null) {
                if(copyTrue){



                    SaveInDb(message,to,textFileName,type, (int) (((size)/100000)+1),replyto,filename);
                }else {
                    SaveInDb(message,to,picturePath,type, (int) (((size)/100000)+1),replyto,filename);
                }
            }

            SendInitialize(data2);

        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }





    }
    private void SaveInDb(BinaryMessageRequestData message,String to,String picturePath,String type,int count,String replyto,String fileName)
    {
        Context context = AppController.getCurrentContext();
        DatabaseHandler db = new DatabaseHandler(context);

        PrivateMessageDataBase pm = new PrivateMessageDataBase(message.getMid(), type,  message.getFrom(), to, Long.parseLong(message.getTimestamp()),picturePath, message.getSize() + "", "save", replyto, "",0,count);
        pm.setFileName(fileName);

        if(!db.isExist(message.getMid())) {
            db.InsertState(pm, context);
        }
            // TODO: 6/28/2017 update conversation
    }
    private void SendInitialize(JSONObject packet)
    {
        Context context = AppController.getCurrentContext();
        SaffronEmitter emitter = new SaffronEmitter();
        emitter.EmitBinaryMessageRequest(packet);
        /*    Security sec = new Security(context);
            sec.Pack(packet, "filesend");*/
    }
    /*public static void Update(String mid){
        MessengerBus event = new MessengerBus("initial",mid);
        EventBus bus  = EventBus.getDefault();
        bus.post(event);
    }*/
    public void SendFirst(String value,String type){
        Log.e("myapp","send first  =");
        Context context = AppController.getCurrentContext();
        try {
            JSONObject dt = new JSONObject(value);


            DataChunkData dataChunkData = new DataChunkData();
            int chunkedsize = dt.getInt("maxchunksize");
            dataChunkData.mid = dt.getString("mid");
            dataChunkData.sequence = dt.getInt("initialsequence");

            DatabaseHandler db = new DatabaseHandler(context);
            PrivateMessageDataBase pdb = db.GetSpecialPMessage(dataChunkData.mid);
            FileCoder fc = null;
            try {

                FileManager fileManager=new FileManager();
                File myfile = new File(Uri.parse(fileManager.makeFile(type).toString() + "/" +pdb.getContent()).getPath());


                byte[] bytesofimage = convertFileToByte(myfile);

                fc = spilit(bytesofimage, chunkedsize, ((int) myfile.length() / chunkedsize) + 1, 0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            dataChunkData.chunksize = fc.getSize();
            dataChunkData.content = fc.getContent();
            JSONObject data = new JSONObject();
            data.put("sequence", dataChunkData.sequence);
            data.put("mid", dataChunkData.mid);
            data.put("chunksize", dataChunkData.chunksize);
            PreferencesData.saveInt(context,"chunksize",chunkedsize);
            data.put("content", dataChunkData.content);
            JSONObject packet = new JSONObject();
            packet.put("data", data);
            packet.put("tc", System.currentTimeMillis() / 1000);
            packet.put("signature", PreferencesData.getString(context, "signature"));
            db.updatePercent(dataChunkData.mid,0);
            SaffronEmitter emitter = new SaffronEmitter();
            emitter.EmitDataChunk(data);
           /* Security sec = new Security(context);
            sec.Pack(packet, "datachunk");*/


        }catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void SendNThPart(String value,int count,String type){
        Log.e("myapp","send nth  =");
        Context context = AppController.getCurrentContext();
        try {
            JSONObject dt = new JSONObject(value);
            DataChunkData dataChunkData = new DataChunkData();
            dataChunkData.mid = dt.getString("mid");
            int chunks = PreferencesData.getInt(context,"chunksize",100000);
            DatabaseHandler db = new DatabaseHandler(context);

            dataChunkData.sequence = dt.getInt("sequence")+1;

            PrivateMessageDataBase pdb = db.GetSpecialPMessage(dataChunkData.mid);
            FileCoder fc=null;
            File myfile=null;
            try {

                FileManager fileManager=new FileManager();

                 myfile = new File(Uri.parse(fileManager.makeFile(type).toString() + "/" +pdb.getContent()).getPath());


                byte[] bytesofimage = convertFileToByte(myfile);

                fc = spilit(bytesofimage, chunks, ((int) myfile.length() /  chunks) + 1,count);
            } catch (IOException e) {
                e.printStackTrace();
            }


            dataChunkData.chunksize =fc.getSize();
             dataChunkData.content = fc.getContent();
            JSONObject data = new JSONObject();
            data.put("sequence", dataChunkData.sequence);
            data.put("mid", dataChunkData.mid);
            data.put("chunksize", dataChunkData.chunksize);
            data.put("content", dataChunkData.content);
            JSONObject packet = new JSONObject();
            packet.put("data", data);
            packet.put("tc", System.currentTimeMillis() / 1000);
            packet.put("signature", PreferencesData.getString(context, "signature"));

            Log.e("myapp","send  ");

            SaffronEmitter emitter = new SaffronEmitter();
            emitter.EmitDataChunk(data);
       /*     Security sec = new Security(context);
            sec.Pack(packet, "datachunk");
*/
            if(count*100000<myfile.length()){

                int chunknumbers = (int) (myfile.length()/100000);
                int mande = (int) (myfile.length()%100000);
                if(mande>0){
                    chunknumbers=chunknumbers+1;
                }

                int percent  = (int) (( (double)count/(double) chunknumbers)*100);
                db.updatePercent(dataChunkData.mid,percent);


            }else {
                db.updatePercent(dataChunkData.mid,100);
            }


            EventBus bus = EventBus.getDefault();
            MessengerBus b  = new MessengerBus("upfile",dataChunkData.mid);
            bus.post(b);


        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    private   byte[] convertFileToByte(File file) throws IOException {
//init array with file length
        byte[] bytesArray = new byte[(int) file.length()];

        FileInputStream fis = new FileInputStream(file);
        fis.read(bytesArray); //read file into bytes[]
        fis.close();

        return bytesArray;
    }
    private FileCoder spilit(byte[] bytearrayfile, int sizechunk, int parts,int n) throws IOException {

        int offset =0;
        int lenth = 0;
        offset = n * sizechunk;
        lenth=sizechunk;
        if(offset+sizechunk>bytearrayfile.length){
            lenth = bytearrayfile.length-offset;
        }
        String a=    Base64.encodeBytes(bytearrayfile,offset,lenth);
        FileCoder fc = new FileCoder(lenth,a);
        return fc;
    }
    class FileCoder {
        int size;
        String content;

        public FileCoder(int size, String content) {
            this.size = size;
            this.content = content;
        }

        public int getSize() {
            return size;
        }

        public String getContent() {
            return content;
        }
    }
}
