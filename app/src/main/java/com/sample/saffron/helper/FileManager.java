package com.sample.saffron.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by alireza on 5/23/2017.
 */

public class FileManager {
    public File makeFile(String type){

        if(type.equals("photo")){
            if(!Consts.ImagesDir.exists()){
                Consts.ImagesDir.mkdirs();
            }
            return Consts.ImagesDir;
        }else if(type.equals("voice")){
            if(!Consts.MusicDir.exists()){
                Consts.MusicDir.mkdirs();
            }
            return Consts.MusicDir;
        }else if(type.equals("video")){
            if(!Consts.VideoDir.exists()){
                Consts.VideoDir.mkdirs();
            }
            return Consts.VideoDir;
        }else if(type.equals("audio")){
            if(!Consts.MusicDir.exists()){
                Consts.MusicDir.mkdirs();
            }
            return Consts.MusicDir;
        }else {
            if(!Consts.Documents.exists()){
                Consts.Documents.mkdirs();
            }
            return Consts.Documents;
        }


    }


    public  boolean isExist(String filePath,String type){
        File file = new File(makeFile(type).toString()+"/"+filePath);
        boolean a = file.exists();
        if(a){
            return true;
        }
        else if(file.getAbsoluteFile().exists()) {
        return true;
        }
        else {
            return false;
        }
    }
    public int fileSize(String filePath,String type){
        File file = new File(makeFile(type).toString()+"/"+filePath);
        if(file.exists()){
         return (int) file.length();
        }else {
            return 0;
        }
    }
    public void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }
}
