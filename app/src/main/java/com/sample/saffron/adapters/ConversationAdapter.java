package com.sample.saffron.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sample.saffron.R;
import com.sample.saffron.entity.userData.ConversationResponseData;
import com.sample.saffron.typo.InfoOfServer;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alireza on 3/30/2017.
 */

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ViewHolder>{

    ArrayList<ConversationResponseData> SubjectValues;
    Context context;
    View view1;
    ViewHolder viewHolder1;
    TextView textView;

    public ConversationAdapter(Context context1,ArrayList<ConversationResponseData>list){

        SubjectValues = list;
        context = context1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtTitle,txtDes,txtTime,txtCounter;
        ImageView img;
        public ViewHolder(View v){

            super(v);

            txtTitle      = (TextView)v.findViewById(R.id.txt_title);
            txtDes = (TextView)v.findViewById(R.id.txt_desc);
            txtTime = (TextView)v.findViewById(R.id.txt_time);
            txtCounter = (TextView)v.findViewById(R.id.txt_counter);
            img = (ImageView) v.findViewById(R.id.imgicon);
        }
    }

    @Override
    public ConversationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        view1 = LayoutInflater.from(context).inflate(R.layout.item_conversation,parent,false);

        viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){

        ConversationResponseData data = SubjectValues.get(position);
        holder.txtTitle.setText(data.getFname()+" "+data.getLname());
        holder.txtDes.setText("unknown");
        holder.txtTime.setText(data.getLastonline());

        String s = InfoOfServer.MultiMediaIpServer+"/uploads/"+data.getImgUrl();
        Glide.with(context)
                .load(s).fitCenter()
                .placeholder(R.drawable.ic_logo)
                .error(R.drawable.ic_logo)
                .into(holder.img);
    }

    @Override
    public int getItemCount(){

        return SubjectValues.size();
    }
}