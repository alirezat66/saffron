package com.sample.saffron.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sample.saffron.R;
import com.sample.saffron.filter.CountryListFilter;
import com.sample.saffron.models.Country;

import java.text.DecimalFormat;
import java.util.List;

public class CountryCodeAdapter extends ArrayAdapter<Country> {
    private Context context;
    private CountryListFilter countryListFilter;

    public CountryCodeAdapter(Context ctx, int resourceId, List<Country> countries) {
        super(ctx, resourceId, countries);
        context = ctx;
    }

    public View getView(int pos, View convertView, ViewGroup parent) {
        LinearLayout row = (LinearLayout) convertView;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = (LinearLayout) inflater.inflate(R.layout.row_country_selection, null);
        }

        ImageView flag = (ImageView) row.findViewById(R.id.row_country_flag);
        TextView countryName = (TextView) row.findViewById(R.id.row_country_name);
        TextView countryPrefix = (TextView) row.findViewById(R.id.row_country_prefix);

        /*String dataString = "data" + (new DecimalFormat("000").format(pos));

        int holderint = context.getResources().getIdentifier(dataString, "array",
                context.getPackageName()); // You had used "name"
        TypedArray item = context.getResources().obtainTypedArray(holderint);*/


        countryName.setText(getItem(pos).getName());
        countryPrefix.setText(getItem(pos).getPrefix());
        flag.setImageDrawable(getItem(pos).getFlag());

        return row;
    }

    @Override
    public android.widget.Filter getFilter() {
        if (countryListFilter == null)
            countryListFilter = new CountryListFilter();
        return countryListFilter;
    }
}
