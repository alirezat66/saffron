package com.sample.saffron.adapters;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.lzyzsd.circleprogress.CircleProgress;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.sample.saffron.Database.DatabaseHandler;
import com.sample.saffron.Database.PrivateMessageDataBase;
import com.sample.saffron.R;
import com.sample.saffron.activities.ImageViewer;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.activities.VideoPlayer;
import com.sample.saffron.entity.messageData.PrivateMessageData;
import com.sample.saffron.helper.AppHelper;
import com.sample.saffron.helper.Consts;
import com.sample.saffron.helper.EmojiTextView;
import com.sample.saffron.helper.FileManager;
import com.sample.saffron.helper.MemoryCache;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.helper.PersianCalendar;
import com.sample.saffron.helper.emojiParser;
import com.sample.saffron.interfacer.DownloadCallbacks;
import com.sample.saffron.services.DownloadService;
import com.sample.saffron.typo.InfoOfServer;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by alireza on 3/30/2017.
 */

public class PrivateMessagesAdapter extends RecyclerView.Adapter<PrivateMessagesAdapter.MyViewHolder> {
    private ArrayList<PrivateMessageDataBase> list;

    Activity context;

    private MemoryCache memoryCache;
    DownloadCallbacks mDownloadCallbacks;



    @BindView(R.id.sender_name)
    TextView senderName;
    @BindView(R.id.status_messages)
    ImageView statusMessages;

    //var for links

    @BindView(R.id.link_preview)
    LinearLayout linkPreview;

    @BindView(R.id.image_preview)
    ImageView imagePreview;

    @BindView(R.id.title_link)
    TextView titleLink;

    @BindView(R.id.url)
    TextView urlLink;

    @BindView(R.id.description)
    TextView descriptionLink;



    //var for upload videos
    @BindView(R.id.video_layout)
    FrameLayout videoLayout;
    @BindView(R.id.video_thumbnail)
    ImageView videoThumbnailFile;
    @BindView(R.id.play_btn_video)
    ImageButton playBtnVideo;
    @BindView(R.id.progress_bar_upload_video)
    ProgressBar mProgressUploadVideo;
    @BindView(R.id.progress_bar_upload_video_init)
    ProgressBar mProgressUploadVideoInitial;
    @BindView(R.id.cancel_upload_video)
    ImageButton cancelUploadVideo;
    @BindView(R.id.retry_upload_video)
    LinearLayout retryUploadVideo;
    @BindView(R.id.progress_bar_download_video)
    ProgressBar mProgressDownloadVideo;
    @BindView(R.id.progress_bar_download_video_init)
    ProgressBar mProgressDownloadVideoInitial;
    @BindView(R.id.cancel_download_video)
    ImageButton cancelDownloadVideo;
    @BindView(R.id.download_video)
    LinearLayout downloadVideo;
    @BindView(R.id.file_size_video)
    TextView fileSizeVideo;

    @BindView(R.id.video_total_duration)
    TextView videoTotalDuration;

    //var for audio
    @BindView(R.id.audio_layout)
    LinearLayout audioLayout;
    @BindView(R.id.audio_user_image)
    ImageView userAudioImage;
    @BindView(R.id.progress_bar_upload_audio)
    ProgressBar mProgressUploadAudio;
    @BindView(R.id.progress_bar_upload_audio_init)
    ProgressBar mProgressUploadAudioInitial;
    @BindView(R.id.cancel_upload_audio)
    ImageButton cancelUploadAudio;
    @BindView(R.id.retry_upload_audio)
    LinearLayout retryUploadAudio;

    @BindView(R.id.retry_upload_audio_button)
    ImageButton retryUploadAudioButton;
    @BindView(R.id.progress_bar_download_audio)
    ProgressBar mProgressDownloadAudio;
    @BindView(R.id.progress_bar_download_audio_init)
    ProgressBar mProgressDownloadAudioInitial;
    @BindView(R.id.cancel_download_audio)
    ImageButton cancelDownloadAudio;

    @BindView(R.id.retry_download_audio_button)
    ImageButton retryDownloadAudioButton;
    @BindView(R.id.play_btn_audio)
    ImageButton playBtnAudio;
    @BindView(R.id.pause_btn_audio)
    ImageButton pauseBtnAudio;
    @BindView(R.id.audio_progress_bar)
    SeekBar audioSeekBar;
    @BindView(R.id.audio_current_duration)
    TextView audioCurrentDurationAudio;
    @BindView(R.id.audio_total_duration)
    TextView audioTotalDurationAudio;

    //for documents
    @BindView(R.id.document_layout)
    LinearLayout documentLayout;
    @BindView(R.id.progress_bar_upload_document)
    ProgressBar mProgressUploadDocument;
    @BindView(R.id.progress_bar_upload_document_init)
    ProgressBar mProgressUploadDocumentInitial;
    @BindView(R.id.cancel_upload_document)
    ImageButton cancelUploadDocument;
    @BindView(R.id.retry_upload_document)
    LinearLayout retryUploadDocument;
    @BindView(R.id.retry_upload_document_button)
    ImageButton retryUploadDocumentButton;
    @BindView(R.id.progress_bar_download_document)
    ProgressBar mProgressDownloadDocument;
    @BindView(R.id.progress_bar_download_document_init)
    ProgressBar mProgressDownloadDocumentInitial;
    @BindView(R.id.cancel_download_document)
    ImageButton cancelDownloadDocument;
    @BindView(R.id.retry_download_document)
    LinearLayout retryDownloadDocument;
    @BindView(R.id.retry_download_document_button)
    ImageButton retryDownloadDocumentButton;
    @BindView(R.id.document_title)
    TextView documentTitle;
    @BindView(R.id.document_image)
    ImageButton documentImage;

    @BindView(R.id.document_size)
    TextView fileSizeDocument;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        EmojiTextView message;

        //variable of images
        FrameLayout imageLayout;
        ImageView imageFile;
        ProgressBar mProgressUploadImage;
        ProgressBar mProgressUploadImageInitial;
        ImageButton cancelUploadImage;
        LinearLayout retryUploadImage;
        ProgressBar mProgressDownloadImage;
        ProgressBar mProgressDownloadImageInitial;
        ImageButton cancelDownloadImage;
        LinearLayout downloadImage;
        TextView fileSizeImage;
        TextView date;
        public MyViewHolder(View view) {
            super(view);

            message = (EmojiTextView) view.findViewById(R.id.message_text);
            date = (TextView) view.findViewById(R.id.date_message);
            //variable of images
            imageLayout = (FrameLayout) view.findViewById(R.id.image_layout);
            imageFile = (ImageView) view.findViewById(R.id.image_file);
            mProgressUploadImage = (ProgressBar) view.findViewById(R.id.progress_bar_upload_image);
            mProgressUploadImageInitial = (ProgressBar) view.findViewById(R.id.progress_bar_upload_image_init);

            cancelUploadImage = (ImageButton) view.findViewById(R.id.cancel_upload_image);

            retryUploadImage = (LinearLayout) view.findViewById(R.id.retry_upload_image);
            mProgressDownloadImage = (ProgressBar) view.findViewById(R.id.progress_bar_download_image);
            mProgressDownloadImageInitial = (ProgressBar) view.findViewById(R.id.progress_bar_download_image_init);
            cancelDownloadImage = (ImageButton) view.findViewById(R.id.cancel_download_image);
            downloadImage = (LinearLayout) view.findViewById(R.id.download_image);
            fileSizeImage = (TextView) view.findViewById(R.id.file_size_image);


            ///




        }



    }

    public PrivateMessagesAdapter(Activity context, ArrayList<PrivateMessageDataBase> list) {
        this.list = list;
        this.context = context;
        this.memoryCache = new MemoryCache();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
        switch (viewType) {
            case 0:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.buble_right, parent, false);
                break;
            case 1:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.buble_left, parent, false);
                break;

        }

        MyViewHolder holder = new MyViewHolder(itemView);

        holder.setIsRecyclable(false);

        return  holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final PrivateMessageDataBase data = list.get(position);

        makeTime(holder,data);
        if (data.getMtype().equals("text")) {
            holder.message.setText(data.getContent());
        } else if (data.getMtype().equals("photo")) {
            holder.message.setVisibility(View.GONE);
            managePhoto(holder,data);
        }


    }


    private void makeTime(MyViewHolder holder,PrivateMessageDataBase data){
        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone("GMT+4:30"));
        PersianCalendar p = new PersianCalendar(data.getTimestamp() * 1000);
        p.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
        long millis = p.getTimeInMillis();
        Date date = new Date(millis);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy' 'HH:mm:ss");
        holder.date.setText(simpleDateFormat.format(date));
    }
    private void managePhoto(final MyViewHolder holder, final PrivateMessageDataBase data){
        if (data.getSender().equals(PreferencesData.getString(context, "userName"))) {

        } else {
            // ax az samte user digar amade
            holder.imageLayout.setVisibility(View.VISIBLE);
            holder.imageFile.setVisibility(View.VISIBLE);
            final FileManager fileManager = new FileManager();
            int last = data.getContent().lastIndexOf(".");
            String s = "";
            if (fileManager.isExist(data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1), "photo")) {

                holder.mProgressDownloadImageInitial.setVisibility(View.GONE);
                holder.mProgressDownloadImage.setVisibility(View.GONE);
                //ax toye hafeze dar bakhshe axha hast
                s = "file://" + fileManager.makeFile("photo").toString() + "/" + data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
                Glide.with(context)
                        .load(s)
                        .asBitmap()
                        .centerCrop()
                        .transform(new BlurTransformation(context, Consts.BLUR_RADIUS))
                        .override(Consts.PRE_MESSAGE_IMAGE_SIZE, Consts.PRE_MESSAGE_IMAGE_SIZE)
                        .into(holder.imageFile);

                final String finalS = s;
                holder.imageFile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ImageViewer.class);
                        intent.putExtra("path", finalS);
                        context.startActivity(intent);

                    }
                });
            } else {
                //ax tooye hafeze dar bakhshe axha nist


               holder.downloadImage.setVisibility(View.VISIBLE);
                holder.downloadImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        holder.mProgressDownloadImageInitial.setVisibility(View.VISIBLE);
                        holder.downloadImage.setVisibility(View.GONE);
                      String  s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();
                        String sssa = "file://" + fileManager.makeFile("photo").toString() + "/" + data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);

                        Uri downloadUri = Uri.parse(s);
                        Uri destinationUri = Uri.parse(sssa);
                        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                                .setRetryPolicy(new DefaultRetryPolicy())
                                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                                .setDownloadListener(new DownloadStatusListener() {
                                    @Override
                                    public void onDownloadComplete(int id) {

                                    }

                                    @Override
                                    public void onDownloadFailed(int id, int errorCode, String errorMessage) {

                                    }

                                    @Override
                                    public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {

                                        holder.mProgressDownloadImageInitial.setVisibility(View.GONE);
                                        holder.mProgressDownloadImage.setVisibility(View.VISIBLE);
                                        holder.cancelDownloadImage.setVisibility(View.VISIBLE);
                                        holder.mProgressDownloadImage.setIndeterminate(false);
                                        holder.mProgressDownloadImage.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.light_green), PorterDuff.Mode.SRC_IN);
                                        holder.mProgressDownloadImage.setProgress(progress);
                                    }
                                });
                    }
                });




            }

        }
    }
    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public int getItemViewType(int position) {

        final PrivateMessageDataBase data = list.get(position);
        int type = 0;
        if (data.getSender().equals(PreferencesData.getString(context, "userName"))) {
            type = 0;
        } else {
            type = 1;
        }

        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        return type;
    }


    public void UpdateUi(String mid) {
        int index = IndexOfObject(mid);
        DatabaseHandler db = new DatabaseHandler(context);
        PrivateMessageDataBase pmd = db.GetSpecialPMessage(mid);

        list.set(index, pmd);

        notifyItemChanged(index, pmd);
    }

    public void UpdateList(PrivateMessageDataBase pmdb) {
        list.add(pmdb);
    }


    int IndexOfObject(String mid) {

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getMid().equals(mid)) {
                return i;
            }
        }
        return -1;
    }


    private class ImageDownloadAndSave extends AsyncTask<String, Void, Bitmap> {
        int position;
        String fileAddress;
        ImageView image;

        private ImageDownloadAndSave(int pos, String address, ImageView img) {
            image = img;
            fileAddress = address;
            position = pos;
        }

        @Override
        protected Bitmap doInBackground(String... arg0) {
            downloadImagesToSdCard(arg0[0], arg0[1], position, fileAddress, image);
            return null;
        }


        private void downloadImagesToSdCard(String downloadUrl, String imageName, final int position, final String fileaddress, final ImageView img) {
            try {
                URL url = new URL(downloadUrl);
                        /* making a directory in sdcard */
                String sdCard = Environment.getExternalStorageDirectory().toString();
                FileManager fileManager = new FileManager();
                File myDir = new File(fileManager.makeFile("photo").toString() + "/" + imageName);
                URLConnection ucon = url.openConnection();
                InputStream inputStream = null;
                HttpURLConnection httpConn = (HttpURLConnection) ucon;
                httpConn.setRequestMethod("GET");
                httpConn.connect();
                if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.getInputStream();
                }
                FileOutputStream fos = new FileOutputStream(myDir);
                final int totalSize = httpConn.getContentLength();
                int downloadedSize = 0;
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fos.write(buffer, 0, bufferLength);
                    downloadedSize += bufferLength;
                    Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);

                    final int finalDownloadedSize = downloadedSize;
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            double divide = (double) finalDownloadedSize / (double) totalSize;
                            int percent = (int) (100 * divide);
                            String mid = list.get(position).getMid();
                            PrivateMessageDataBase pm = list.get(position);
                            pm.setPercent(percent);
                            DatabaseHandler db = new DatabaseHandler(context);
                            db.updatePercent(pm.getMid(), percent);
                            list.set(position, pm);
                            UpdateUi(mid);
                            if (percent == 100) {
                                image.setVisibility(View.VISIBLE);
                                Glide.with(context).load(fileaddress).override(400, 800).diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter().into(img);


                            }
                        }
                    });

                }

                fos.close();
                Log.d("test", "Image Saved in sdcard..");
            } catch (IOException io) {
                io.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }




}



