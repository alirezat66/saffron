package com.sample.saffron.adapters;

import android.app.Activity;
import android.icu.text.RelativeDateTimeFormatter;
import android.icu.text.TimeZoneFormat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.sample.saffron.R;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.entity.userData.ConversationResponseData;
import com.sample.saffron.typo.InfoOfServer;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import cn.gavinliu.android.lib.shapedimageview.ShapedImageView;

/**
 * Created by alireza on 3/30/2017.
 */

public class ConversationListAdapter extends BaseAdapter {


        public List<ConversationResponseData> listItems;
        Activity context;

    private static final long NOW = new Date().getTime();

    View parentView;
        AdapterView.OnItemClickListener onItemClickListener;
        public boolean flagisupdate = false;
        public int positionItem;
        public int itemId;

        public ConversationListAdapter(Activity context/*, AdapterView.OnItemClickListener onItemClickListener*/, List<ConversationResponseData> list) {
            this.listItems = list;
            this.context = context;
            this.onItemClickListener = onItemClickListener;
        }


        @Override
        public int getCount() {
            return listItems.size();
        }

        @Override
        public ConversationResponseData getItem(int position) {
            return listItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            public TextView txtTitle,txtDes,txtCounter;
            RelativeTimeTextView txtTime;
            ShapedImageView img;

        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            LayoutInflater inflater = context.getLayoutInflater();
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_conversation, null);
                holder = new ViewHolder();
               holder.txtTitle      = (TextView)convertView.findViewById(R.id.txt_title);
               holder.txtDes = (TextView)convertView.findViewById(R.id.txt_desc);
               holder.txtTime = (RelativeTimeTextView) convertView.findViewById(R.id.txt_time);
               holder.txtCounter = (TextView)convertView.findViewById(R.id.txt_counter);
               holder.img = (ShapedImageView)convertView.findViewById(R.id.imgicon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


             ConversationResponseData data = listItems.get(position);

            holder.txtTitle.setText(data.getFname()+" "+data.getLname());

            String res = "";
            holder.txtTitle.setText(data.getFname()+" "+data.getLname());
            holder.txtDes.setText(data.getLastMessage());
            long lastonline = Long.parseLong(data.getLastonline());
            lastonline=lastonline*1000;
            Date messagedate = new Date(lastonline);


            String ipStr  = InfoOfServer.MultiMediaIpServer;
            holder.txtTime.setReferenceTime(lastonline);

            String s =ipStr+"/uploads/"+data.getImgUrl();

            int errorid =0;
            if(data.getType().equals("group")){
                errorid=R.drawable.default_group_64;
            }else {
                errorid=R.drawable.default_user_64;
            }
            Picasso.with(context)
                    .load(s)
                    .placeholder(errorid)
                    .error(errorid)
                    .into(holder.img);
            if(data.getUnseen().equals("0")){
                holder.txtCounter.setVisibility(View.INVISIBLE);
            }else {
                holder.txtCounter.setText(data.getUnseen());
            }
            return convertView;
        }
}
