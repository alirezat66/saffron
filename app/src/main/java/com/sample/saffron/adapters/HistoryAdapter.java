package com.sample.saffron.adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.lzyzsd.circleprogress.CircleProgress;
import com.github.nkzawa.emitter.Emitter;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sample.saffron.Database.DatabaseHandler;
import com.sample.saffron.Database.PrivateMessageDataBase;
import com.sample.saffron.R;
import com.sample.saffron.activities.AppController;
import com.sample.saffron.activities.ImageViewer;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.activities.SaffronEmitter;
import com.sample.saffron.activities.VideoPlayer;
import com.sample.saffron.client.Security;
import com.sample.saffron.entity.groupData.HistoryResponseData;
import com.sample.saffron.entity.messageData.PrivateMessageData;
import com.sample.saffron.helper.Consts;
import com.sample.saffron.helper.EmojiTextView;
import com.sample.saffron.helper.FileManager;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.helper.PersianCalendar;
import com.sample.saffron.helper.emojiParser;
import com.sample.saffron.services.DownloadService;
import com.sample.saffron.typo.InfoOfServer;
import com.squareup.picasso.Picasso;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import it.michelelacorte.elasticprogressbar.ElasticDownloadView;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static android.R.attr.bitmap;
import static android.R.attr.theme;
import static android.R.attr.visibility;
import static android.R.attr.wallpaperCloseEnterAnimation;

/**
 * Created by alireza on 3/30/2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
    private ArrayList<PrivateMessageDataBase> list;
    Activity context;
    String userName = "";
    private SparseBooleanArray selectedItems;
    Timer mytimer = new Timer();
    private boolean isActivated = false;
    ThinDownloadManager downloadManager = new ThinDownloadManager();
    final MediaPlayer[] mediaPlayer = new MediaPlayer[1];
    String playingMid = "";


    public class MyViewHolder extends RecyclerView.ViewHolder  implements SeekBar.OnSeekBarChangeListener {


        ImageView status_message;

        LinearLayout base;
        ImageView sticker;


        //reply layout

        RelativeLayout reply_layout;
        TextView txtMessreply;
        TextView txtMainMessage;
        EmojiTextView message;
        FrameLayout imageLayout;
        ImageView imageFile;
        ProgressBar mProgressUploadImage;
        ProgressBar mProgressUploadImageInitial;
        ImageButton cancelUploadImage;
        LinearLayout retryUploadImage;
        ProgressBar mProgressDownloadImage;
        ProgressBar mProgressDownloadImageInitial;
        ImageButton cancelDownloadImage;
        LinearLayout downloadImage;
        TextView fileSizeImage;
        TextView date;


        //video

        ImageView btn_download_video;
        TextView fileSizeVideo;
        FrameLayout videoLayout;
        LinearLayout downloadVideo;
        ImageButton cancelDownloadVideo;
        ProgressBar mProgressDownloadVideoInitial;

        ProgressBar mProgressDownloadVideo;

        LinearLayout retryUploadVideo;
        ImageButton cancelUploadVideo;
        ProgressBar mProgressUploadVideoInitial;
        ProgressBar mProgressUploadVideo;
        ImageView playBtnVideo;
        ImageView videoThumbnailFile;


        //


        //audio


        LinearLayout audioLayout;
        ImageView userAudioImage;
        ProgressBar mProgressUploadAudio;
        ProgressBar mProgressUploadAudioInitial;
        ImageButton cancelUploadAudio;
        LinearLayout retryUploadAudio;
        ImageButton retryUploadAudioButton;
        ProgressBar mProgressDownloadAudio;
        ProgressBar mProgressDownloadAudioInitial;
        ImageButton cancelDownloadAudio;
        ImageButton retryDownloadAudioButton;
        ImageButton playBtnAudio;
        ImageButton pauseBtnAudio;
        SeekBar audioSeekBar;
        TextView audioCurrentDurationAudio;
        TextView audioTotalDurationAudio;


        // files


        LinearLayout documentLayout;
        ProgressBar mProgressUploadDocument;
        ProgressBar mProgressUploadDocumentInitial;
        ImageButton cancelUploadDocument;
        LinearLayout retryUploadDocument;
        ImageButton retryUploadDocumentButton;
        ProgressBar mProgressDownloadDocument;
        ProgressBar mProgressDownloadDocumentInitial;
        ImageButton cancelDownloadDocument;
        LinearLayout retryDownloadDocument;
        ImageButton retryDownloadDocumentButton;
        TextView documentTitle;
        ImageButton documentImage;
        TextView fileSizeDocument;
        ImageView replyimg;

        public MyViewHolder(View view) {
            super(view);

            reply_layout = (RelativeLayout) view.findViewById(R.id.reply_layout);
            txtMessreply = (TextView) view.findViewById(R.id.txtMessreply);
            txtMainMessage = (TextView) view.findViewById(R.id.txtMainMessage);
            replyimg = (ImageView) view.findViewById(R.id.replyimg);

            status_message = (ImageView) view.findViewById(R.id.status_messages);
            message = (EmojiTextView) view.findViewById(R.id.message_text);
            date = (TextView) view.findViewById(R.id.date_message);
            //variable of images
            imageLayout = (FrameLayout) view.findViewById(R.id.image_layout);
            imageFile = (ImageView) view.findViewById(R.id.image_file);
            mProgressUploadImage = (ProgressBar) view.findViewById(R.id.progress_bar_upload_image);
            mProgressUploadImageInitial = (ProgressBar) view.findViewById(R.id.progress_bar_upload_image_init);

            cancelUploadImage = (ImageButton) view.findViewById(R.id.cancel_upload_image);

            retryUploadImage = (LinearLayout) view.findViewById(R.id.retry_upload_image);
            mProgressDownloadImage = (ProgressBar) view.findViewById(R.id.progress_bar_download_image);
            mProgressDownloadImageInitial = (ProgressBar) view.findViewById(R.id.progress_bar_download_image_init);
            cancelDownloadImage = (ImageButton) view.findViewById(R.id.cancel_download_image);
            downloadImage = (LinearLayout) view.findViewById(R.id.download_image);
            fileSizeImage = (TextView) view.findViewById(R.id.file_size_image);


            // variableimagefinished


            /// video variable start

            videoLayout = (FrameLayout) view.findViewById(R.id.video_layout);
            videoThumbnailFile = (ImageView) view.findViewById(R.id.video_thumbnail);
            playBtnVideo = (ImageView) view.findViewById(R.id.play_btn_video);
            mProgressUploadVideo = (ProgressBar) view.findViewById(R.id.progress_bar_upload_video);
            mProgressUploadVideoInitial = (ProgressBar) view.findViewById(R.id.progress_bar_upload_video_init);
            cancelUploadImage = (ImageButton) view.findViewById(R.id.cancel_upload_video);
            mProgressDownloadVideo = (ProgressBar) view.findViewById(R.id.progress_bar_download_video);
            mProgressDownloadVideoInitial = (ProgressBar) view.findViewById(R.id.progress_bar_download_video_init);
            cancelDownloadImage = (ImageButton) view.findViewById(R.id.cancel_download_video);
            downloadVideo = (LinearLayout) view.findViewById(R.id.download_video);
            fileSizeVideo = (TextView) view.findViewById(R.id.file_size_video);

            btn_download_video = (ImageView) view.findViewById(R.id.btn_download_video);
            // video finished


            /// audio

            audioLayout = (LinearLayout) view.findViewById(R.id.audio_layout);
            userAudioImage = (ImageView) view.findViewById(R.id.audio_user_image);
            mProgressUploadAudio = (ProgressBar) view.findViewById(R.id.progress_bar_upload_audio);
            mProgressUploadAudioInitial = (ProgressBar) view.findViewById(R.id.progress_bar_upload_audio_init);
            cancelUploadAudio = (ImageButton) view.findViewById(R.id.cancel_upload_audio);
            retryUploadAudio = (LinearLayout) view.findViewById(R.id.retry_upload_audio);
            retryUploadAudioButton = (ImageButton) view.findViewById(R.id.retry_upload_audio_button);
            mProgressDownloadAudio = (ProgressBar) view.findViewById(R.id.progress_bar_download_audio);
            mProgressDownloadAudioInitial = (ProgressBar) view.findViewById(R.id.progress_bar_download_audio_init);
            cancelDownloadAudio = (ImageButton) view.findViewById(R.id.cancel_download_audio);
            retryDownloadAudioButton = (ImageButton) view.findViewById(R.id.retry_download_audio_button);
            playBtnAudio = (ImageButton) view.findViewById(R.id.play_btn_audio);
            pauseBtnAudio = (ImageButton) view.findViewById(R.id.pause_btn_audio);
            audioSeekBar = (SeekBar) view.findViewById(R.id.audio_progress_bar);

            audioSeekBar.setOnSeekBarChangeListener(this);
            audioCurrentDurationAudio = (TextView) view.findViewById(R.id.audio_current_duration);
            audioTotalDurationAudio = (TextView) view.findViewById(R.id.audio_total_duration);


            ///audio finished


            //files start

            documentLayout = (LinearLayout) view.findViewById(R.id.document_layout);
            mProgressUploadDocument = (ProgressBar) view.findViewById(R.id.progress_bar_upload_document);
            mProgressUploadDocumentInitial = (ProgressBar) view.findViewById(R.id.progress_bar_upload_document_init);
            cancelUploadDocument = (ImageButton) view.findViewById(R.id.cancel_upload_document);
            retryUploadDocument = (LinearLayout) view.findViewById(R.id.retry_upload_document);
            retryUploadDocumentButton = (ImageButton) view.findViewById(R.id.retry_upload_document_button);
            mProgressDownloadDocument = (ProgressBar) view.findViewById(R.id.progress_bar_download_document);
            mProgressDownloadDocumentInitial = (ProgressBar) view.findViewById(R.id.progress_bar_download_document_init);
            cancelDownloadDocument = (ImageButton) view.findViewById(R.id.cancel_download_document);
            retryDownloadDocument = (LinearLayout) view.findViewById(R.id.retry_download_document);
            retryDownloadDocumentButton = (ImageButton) view.findViewById(R.id.retry_download_document_button);
            documentTitle = (TextView) view.findViewById(R.id.document_title);
            documentImage = (ImageButton) view.findViewById(R.id.document_image);
            fileSizeDocument = (TextView) view.findViewById(R.id.document_size);


            sticker = (ImageView) view.findViewById(R.id.stickerimage);
            base = (LinearLayout) view.findViewById(R.id.base);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus bus = EventBus.getDefault();
                    MessengerBus messengerBus = new MessengerBus("clicked", v);
                    bus.post(messengerBus);
                }
            });
        }


        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            if(b) {
                mediaPlayer[0].seekTo(i);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }

    public HistoryAdapter(Activity context, ArrayList<PrivateMessageDataBase> list) {
        this.list = list;
        this.context = context;
        this.userName = PreferencesData.getString(context, "userName");
        this.selectedItems = new SparseBooleanArray();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
        switch (viewType) {
            case 1:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.buble_right, parent, false);
                break;
            case 2:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.buble_left, parent, false);
                break;
            default:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.buble_right, parent, false);
                break;

        }


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final PrivateMessageDataBase data = list.get(position);

        makeTime(holder, data);
        checkStatus(holder, data);
        checkForwardAndReply(holder, data);

        if (data.getMtype().equals("photo")) {
            photoLayoutManager(holder, data, position);
        } else if (data.getMtype().equals("sticker")) {
            stickerLayoutManagment(holder, data);
        } else if (data.getMtype().equals("video")) {
            videoLayoutManager(holder, data, position);
        } else if (data.getMtype().equals("voice")) {
            voiceLayoutManager(holder, data, position);
        } else if (data.getMtype().equals("file")) {
            fileLayoutManager(holder, data, position);
        } else {
            textLayoutManager(holder, data, position);
        }

        holder.itemView.setActivated(selectedItems.get(position, false));


    }

    private void checkForwardAndReply(MyViewHolder holder, PrivateMessageDataBase data) {
        if (data.getReplyto().equals("")) {
            if (data.getForwarded().equals("")) {
                holder.reply_layout.setVisibility(View.GONE);
            } else {
                holder.reply_layout.setVisibility(View.VISIBLE);
                DatabaseHandler db = new DatabaseHandler(context);

                if (!data.getForwarded().equals("")) {

                    try {
                        JSONObject obj = new JSONObject(data.getForwarded());
                        String content = obj.getString("content");
                        holder.txtMessreply.setText("fileName");

                        if (content.toString().endsWith(".doc") || content.toString().endsWith(".docx")) {
                            // Word document
                            holder.replyimg.setVisibility(View.VISIBLE);
                            Glide.with(context).load(R.drawable.ic_docs_48dp).into(holder.replyimg);
                        } else if (content.toString().endsWith(".pdf")) {
                            holder.replyimg.setVisibility(View.VISIBLE);
                            Glide.with(context).load(R.drawable.ic_docs_48dp).into(holder.replyimg);
                            // PDF file
                        } else if (content.toString().endsWith(".ppt") || content.toString().endsWith(".pptx")) {
                            holder.replyimg.setVisibility(View.VISIBLE);
                            Glide.with(context).load(R.drawable.ic_slides_48dp).into(holder.replyimg);
                            // Powerpoint file
                        } else if (content.toString().endsWith(".xls") || content.toString().endsWith(".xlsx")) {
                            holder.replyimg.setVisibility(View.VISIBLE);
                            Glide.with(context).load(R.drawable.ic_sheets_48dp).into(holder.replyimg);
                            // Excel file
                        } else if (content.toString().endsWith(".zip") || content.toString().endsWith(".rar")) {
                            holder.replyimg.setVisibility(View.VISIBLE);
                            Glide.with(context).load(R.drawable.ic_docs_48dp).into(holder.replyimg);
                        } else if (content.toString().endsWith(".rtf")) {
                            // RTF file
                        } else if (content.toString().endsWith(".wav") || content.toString().endsWith(".mp3")) {
                            // WAV audio file
                            holder.replyimg.setVisibility(View.VISIBLE);
                            Glide.with(context).load(R.drawable.ic_music).into(holder.replyimg);
                        } else if (content.toString().endsWith(".gif")) {
                            // GIF file
                        } else if (content.toString().endsWith(".jpg") || content.toString().endsWith(".jpeg") || content.toString().endsWith(".png")) {


                            String tumbnailAddress = InfoOfServer.MultiMediaIpServer + "/uploads/" + content;
                            holder.replyimg.setVisibility(View.VISIBLE);
                            Glide.with(context).load(tumbnailAddress).into(holder.replyimg);
                            // JPG file
                        } else if (content.toString().endsWith(".txt")) {
                            holder.replyimg.setVisibility(View.VISIBLE);
                            Glide.with(context).load(R.drawable.ic_docs_48dp).into(holder.replyimg);
                            // Text file
                        } else if (content.toString().endsWith(".3gp") || content.toString().endsWith(".mpg") || content.toString().endsWith(".mpeg") || content.toString().endsWith(".mpe") || content.toString().endsWith(".mp4") || content.toString().endsWith(".avi")) {
                            holder.replyimg.setVisibility(View.VISIBLE);


                            String addressWithOutPostFix = content.substring(0, content.lastIndexOf("."));
                            int index = addressWithOutPostFix.lastIndexOf("/");
                            addressWithOutPostFix = addressWithOutPostFix.substring(index + 1);
                            String tumbnailAddress = InfoOfServer.MultiMediaIpServer + "/uploads/" + addressWithOutPostFix + "_1.png";
                            Glide.with(context).load(tumbnailAddress).into(holder.replyimg);
                            /*showTumbnail(tumbnailAddress,holder.videoThumbnailFile);*/
                            // Video files
                        } else {
                            holder.replyimg.setVisibility(View.GONE);

                            holder.txtMessreply.setText(content);
                            holder.txtMessreply.setVisibility(View.GONE);
                        }
                        String sender = obj.getString("from");
                        holder.txtMainMessage.setText("forwarded by: " + sender);


                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    holder.reply_layout.setVisibility(View.GONE);

                    holder.txtMainMessage.setText("loading ...");
                    holder.txtMessreply.setText("loading ...");

                }


            }
        } else {
            holder.reply_layout.setVisibility(View.VISIBLE);
            DatabaseHandler db = new DatabaseHandler(context);
            if (db.isExist(data.getReplyto())) {
                PrivateMessageDataBase special = db.GetSpecialPMessage(data.getReplyto());
                holder.txtMainMessage.setText(special.getSender());
                String content = special.getContent();
                if (special.getFileName().equals("")) {
                    holder.txtMessreply.setVisibility(View.GONE);
                } else {
                    holder.txtMessreply.setVisibility(View.VISIBLE);
                    holder.txtMessreply.setText(special.getFileName());
                }
                if (content.toString().endsWith(".doc") || content.toString().endsWith(".docx")) {
                    // Word document
                    holder.replyimg.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.drawable.ic_docs_48dp).into(holder.replyimg);

                } else if (content.toString().endsWith(".pdf")) {
                    holder.replyimg.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.drawable.ic_docs_48dp).into(holder.replyimg);
                    // PDF file
                } else if (content.toString().endsWith(".ppt") || content.toString().endsWith(".pptx")) {
                    holder.replyimg.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.drawable.ic_slides_48dp).into(holder.replyimg);
                    // Powerpoint file
                } else if (content.toString().endsWith(".xls") || content.toString().endsWith(".xlsx")) {
                    holder.replyimg.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.drawable.ic_sheets_48dp).into(holder.replyimg);
                    // Excel file
                } else if (content.toString().endsWith(".zip") || content.toString().endsWith(".rar")) {
                    holder.replyimg.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.drawable.ic_docs_48dp).into(holder.replyimg);
                } else if (content.toString().endsWith(".rtf")) {
                    // RTF file
                } else if (content.toString().endsWith(".wav") || content.toString().endsWith(".mp3")) {
                    // WAV audio file
                    holder.replyimg.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.drawable.ic_music).into(holder.replyimg);
                } else if (content.toString().endsWith(".gif")) {
                    // GIF file
                } else if (content.toString().endsWith(".jpg") || content.toString().endsWith(".jpeg") || content.toString().endsWith(".png")) {


                    String tumbnailAddress = InfoOfServer.MultiMediaIpServer + "/uploads/" + content;
                    holder.replyimg.setVisibility(View.VISIBLE);
                    Glide.with(context).load(tumbnailAddress).into(holder.replyimg);
                    // JPG file
                } else if (content.toString().endsWith(".txt")) {
                    holder.replyimg.setVisibility(View.VISIBLE);
                    Glide.with(context).load(R.drawable.ic_docs_48dp).into(holder.replyimg);
                    // Text file
                } else if (content.toString().endsWith(".3gp") || content.toString().endsWith(".mpg") || content.toString().endsWith(".mpeg") || content.toString().endsWith(".mpe") || content.toString().endsWith(".mp4") || content.toString().endsWith(".avi")) {
                    holder.replyimg.setVisibility(View.VISIBLE);


                    String addressWithOutPostFix = content.substring(0, content.lastIndexOf("."));
                    int index = addressWithOutPostFix.lastIndexOf("/");
                    addressWithOutPostFix = addressWithOutPostFix.substring(index + 1);
                    String tumbnailAddress = InfoOfServer.MultiMediaIpServer + "/uploads/" + addressWithOutPostFix + "_1.png";
                    Glide.with(context).load(tumbnailAddress).into(holder.replyimg);
                            /*showTumbnail(tumbnailAddress,holder.videoThumbnailFile);*/
                    // Video files
                } else {
                    holder.replyimg.setVisibility(View.GONE);
                    holder.txtMessreply.setText(content);
                    holder.txtMessreply.setVisibility(View.VISIBLE);

                }

            } else {
                holder.txtMainMessage.setText("loading ...");
                holder.txtMessreply.setText("loading ...");
            }
        }

    }

    public void checkStatus(MyViewHolder holder, PrivateMessageDataBase data) {
        if (data.getSender().equals(userName)) {
            holder.status_message.setVisibility(View.VISIBLE);
            if (data.getEvents().equals("save")) {
                holder.status_message.setImageResource(R.drawable.ic_waiting);
            } else if (data.getEvents().equals("sent")) {
                holder.status_message.setImageResource(R.drawable.checknotsend);

            } else if (data.getEvents().equals("delivered")) {
                holder.status_message.setImageResource(R.drawable.done);
            } else {
                holder.status_message.setImageResource(R.drawable.deliverd_icon);
            }
        } else {
            holder.status_message.setVisibility(View.INVISIBLE);
        }
    }

    private void makeTime(MyViewHolder holder, PrivateMessageDataBase data) {
        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone("GMT+4:30"));
        PersianCalendar p = new PersianCalendar(data.getTimestamp() * 1000);
        long millis = p.getTimeInMillis();
        Date date = new Date(millis);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy' 'HH:mm:ss");
        holder.date.setText(simpleDateFormat.format(date));
    }

    private void managePhoto(final MyViewHolder holder, final PrivateMessageDataBase data, final int position) {
        String localAddress = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
        int filesize = Integer.parseInt(data.getSize());


        // if getpercent ==0   we shod download
        // if getpercent >0     we should show progress
        // if get percent ==100 download completed

        if (isExistComplitly(localAddress, filesize, "photo")) {

            holder.imageFile.setVisibility(View.VISIBLE);
            holder.downloadImage.setVisibility(View.GONE);
            holder.mProgressDownloadImageInitial.setVisibility(View.GONE);
            holder.mProgressDownloadImage.setVisibility(View.GONE);
            //ax toye hafeze dar bakhshe axha hast
            FileManager fileManager = new FileManager();
            String s = "file://" + fileManager.makeFile("photo").toString() + "/" + data.getContent();
            if (s.contains(".gif")) {
                Glide.with(context)
                        .load(s)
                        .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .centerCrop()
                        .into(holder.imageFile);
            } else {
                Glide.with(context)
                        .load(s)
                        .asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .centerCrop()
                        .into(holder.imageFile);
            }

            final String finalS = s;
            holder.imageFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isActivated) {
                        Intent intent = new Intent(context, ImageViewer.class);
                        intent.putExtra("path", finalS);
                        PreferencesData.saveInt(context, "position", position);

                        context.startActivity(intent);
                    } else {
                        EventBus bus = EventBus.getDefault();
                        MessengerBus messengerBus = new MessengerBus("clicked", position);
                        bus.post(messengerBus);
                    }
                }
            });

        } else if (isExistPartialy(localAddress, filesize, "photo")) {

            // bakhshi az an download shode
            if (data.getPercent() == 0) {
                holder.imageFile.setVisibility(View.VISIBLE);
                holder.downloadImage.setVisibility(View.GONE);
                holder.mProgressDownloadImageInitial.setVisibility(View.GONE);
                holder.mProgressDownloadImage.setVisibility(View.VISIBLE);
            } else {
                holder.imageFile.setVisibility(View.VISIBLE);
                holder.downloadImage.setVisibility(View.GONE);
                holder.mProgressDownloadImageInitial.setVisibility(View.GONE);
                holder.mProgressDownloadImage.setVisibility(View.VISIBLE);

                holder.mProgressDownloadImage.setIndeterminate(false);
                holder.mProgressDownloadImage.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.light_green), PorterDuff.Mode.SRC_IN);
                holder.mProgressDownloadImage.setProgress(data.getPercent());
            }

        } else {
            // file kolan nist


            holder.imageFile.setVisibility(View.VISIBLE);
            holder.downloadImage.setVisibility(View.GONE);
            holder.mProgressDownloadImageInitial.setVisibility(View.VISIBLE);
            holder.mProgressDownloadImage.setVisibility(View.GONE);
            Intent intent = new Intent(context, DownloadService.class);
            String s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();
            final FileManager fileManager = new FileManager();
            String sssa = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
            intent.putExtra("url", s);
            intent.putExtra("mid", data.getMid());
            intent.putExtra("fileAdd", sssa);
            intent.putExtra("type", "photo");
            intent.putExtra("receiver", new DownloadReceiver(new Handler()));
            context.startService(intent);


        }


        // ax az samte user digar amade
        holder.imageLayout.setVisibility(View.VISIBLE);
        holder.imageFile.setVisibility(View.VISIBLE);
        holder.mProgressUploadImageInitial.setVisibility(View.GONE);
        holder.mProgressUploadImage.setVisibility(View.GONE);
        final FileManager fileManager = new FileManager();
        int last = data.getContent().lastIndexOf(".");
        String s = "";
        if (fileManager.isExist(data.getContent(), "photo")) {


        } else {

            holder.imageFile.setVisibility(View.INVISIBLE);
            holder.mProgressDownloadImageInitial.setVisibility(View.VISIBLE);
            holder.fileSizeImage.setVisibility(View.GONE);
            holder.downloadImage.setVisibility(View.GONE);

            s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();
            String sssa = "file://" + fileManager.makeFile("photo").toString() + "/" + data.getContent();

            Uri downloadUri = Uri.parse(s);
            Uri destinationUri = Uri.parse(sssa);
            DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                    .setRetryPolicy(new DefaultRetryPolicy())

                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                    .setDownloadListener(new DownloadStatusListener() {
                        @Override
                        public void onDownloadComplete(int id) {
                            notifyItemChanged(position);
                        }

                        @Override

                        public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();

                            holder.mProgressDownloadImage.setVisibility(View.GONE);
                            holder.mProgressDownloadImageInitial.setVisibility(View.GONE);
                            holder.cancelDownloadImage.setVisibility(View.GONE);

                            holder.downloadImage.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {

                            holder.mProgressDownloadImageInitial.setVisibility(View.GONE);
                            holder.mProgressDownloadImage.setVisibility(View.VISIBLE);
                            holder.cancelDownloadImage.setVisibility(View.GONE);
                            holder.mProgressDownloadImage.setIndeterminate(false);
                            holder.mProgressDownloadImage.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.light_green), PorterDuff.Mode.SRC_IN);
                            holder.mProgressDownloadImage.setProgress(progress);
                        }
                    });

            downloadManager.add(downloadRequest);


        }


    }

    private void manageMyPhoto(final MyViewHolder holder, final PrivateMessageDataBase data, final String mid, final int position) {
        String localAddress = data.getContent();
        int filesize = Integer.parseInt(data.getSize());
        if (!data.getForwarded().equals("")) {
            try {
                JSONObject object = new JSONObject(data.getForwarded());
                filesize = object.getInt("size");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

        }
        if (isExistComplitly(localAddress, filesize, "photo")) {
            if (data.getPercent() == 100) {
                showImage(holder, data, position);

            } else {
                showImageUpdating(holder, data.getPercent());
                // file ghataan dar hale ersale
            }
        } else if (isExistPartialy(localAddress, filesize, "photo")) {
            showImageUpdating(holder, data.getPercent());
            // file dar hale downloade
        } else {
            DatabaseHandler db = new DatabaseHandler(context);
            db.updatePercent(data.getMid(), 0);
            startDownloadImage(holder, data);


            // file bayad download she
        }
        // ax az samte user digar amade
    }

    private void manageFile(final MyViewHolder holder, final PrivateMessageDataBase data, final String mid, final int position) {


        String localAddress = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
        int filesize = Integer.parseInt(data.getSize());


        if (isExistComplitly(localAddress, filesize, "file")) {
            holder.documentLayout.setVisibility(View.VISIBLE);
            holder.documentImage.setVisibility(View.VISIBLE);
            holder.mProgressDownloadVideo.setVisibility(View.GONE);
            holder.mProgressDownloadDocument.setVisibility(View.GONE);
            holder.mProgressDownloadDocumentInitial.setVisibility(View.GONE);
            holder.retryDownloadDocument.setVisibility(View.GONE);
            holder.documentTitle.setVisibility(View.VISIBLE);
            holder.fileSizeDocument.setVisibility(View.VISIBLE);


            String size = calculateSize(filesize);
            holder.fileSizeDocument.setText(size);

            String name = data.getContent();

            int lastindexslash = name.lastIndexOf("/") + 1;
            name = name.substring(lastindexslash);


            if (!data.getFileName().equals("")) {
                name = data.getFileName();
            }
            holder.documentTitle.setText(name);
            holder.documentImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (!isActivated) {
                        final FileManager fileManager = new FileManager();


                        String sssa = "file://" + fileManager.makeFile("file").toString() + "/" + data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
                        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/saffron/Documents");

                        File pdfFile = new File(dir, data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1));//File path
                        if (pdfFile.exists()) //Checking for the file is exist or not
                        {
                            Uri path = Uri.fromFile(pdfFile);

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            // Check what kind of file you are trying to open, by comparing the url with extensions.
                            // When the if condition is matched, plugin sets the correct intent (mime) type,
                            // so Android knew what application to use to open the file
                            if (sssa.toString().contains(".doc") || sssa.toString().contains(".docx")) {
                                // Word document
                                intent.setDataAndType(path, "application/msword");
                            } else if (sssa.toString().contains(".pdf")) {
                                // PDF file
                                intent.setDataAndType(path, "application/pdf");
                            } else if (sssa.toString().contains(".ppt") || sssa.toString().contains(".pptx")) {
                                // Powerpoint file
                                intent.setDataAndType(path, "application/vnd.ms-powerpoint");
                            } else if (path.toString().contains(".xls") || path.toString().contains(".xlsx")) {
                                // Excel file
                                intent.setDataAndType(path, "application/vnd.ms-excel");
                            } else if (path.toString().contains(".zip") || path.toString().contains(".rar")) {
                                // WAV audio file
                                intent.setDataAndType(path, "application/x-wav");
                            } else if (path.toString().contains(".rtf")) {
                                // RTF file
                                intent.setDataAndType(path, "application/rtf");
                            } else if (path.toString().contains(".wav") || path.toString().contains(".mp3")) {
                                // WAV audio file
                                intent.setDataAndType(path, "audio/x-wav");
                            } else if (path.toString().contains(".gif")) {
                                // GIF file
                                intent.setDataAndType(path, "image/gif");
                            } else if (path.toString().contains(".jpg") || path.toString().contains(".jpeg") || path.toString().contains(".png")) {
                                // JPG file
                                intent.setDataAndType(path, "image/jpeg");
                            } else if (path.toString().contains(".txt")) {
                                // Text file
                                intent.setDataAndType(path, "text/plain");
                            } else if (path.toString().contains(".3gp") || path.toString().contains(".mpg") || path.toString().contains(".mpeg") || path.toString().contains(".mpe") || path.toString().contains(".mp4") || path.toString().contains(".avi")) {
                                // Video files
                                intent.setDataAndType(path, "video/*");
                            } else {
                                intent.setDataAndType(path, "*/*");
                            }

                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            context.startActivity(intent);


                        } else {


                            Toast.makeText(context, "The file not exists! ", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        EventBus bus = EventBus.getDefault();
                        MessengerBus messengerBus = new MessengerBus("clicked", position);
                        bus.post(messengerBus);
                    }

                }
            });


        } else if (isExistPartialy(localAddress, filesize, "file")) {
            if (data.getPercent() == 0) {
                holder.documentLayout.setVisibility(View.VISIBLE);
                holder.documentImage.setVisibility(View.GONE);
                holder.retryDownloadDocument.setVisibility(View.GONE);
                holder.mProgressUploadDocumentInitial.setVisibility(View.VISIBLE);
                holder.mProgressDownloadDocument.setVisibility(View.GONE);


                holder.documentTitle.setVisibility(View.VISIBLE);
                holder.fileSizeDocument.setVisibility(View.VISIBLE);
                String size = calculateSize(filesize);
                holder.fileSizeDocument.setText(size + "");

                String name = data.getContent();
                int lastindexslash = name.lastIndexOf("/") + 1;
                name = name.substring(lastindexslash);

                if (!data.getFileName().equals("")) {
                    name = data.getFileName();
                }
                holder.documentTitle.setText(name);


            } else {
                holder.documentLayout.setVisibility(View.VISIBLE);
                holder.documentImage.setVisibility(View.GONE);
                holder.retryDownloadDocument.setVisibility(View.GONE);
                holder.mProgressUploadDocumentInitial.setVisibility(View.GONE);
                holder.mProgressDownloadDocument.setVisibility(View.VISIBLE);
                holder.mProgressDownloadDocument.setIndeterminate(false);
                holder.mProgressDownloadDocument.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.light_green), PorterDuff.Mode.SRC_IN);
                holder.mProgressDownloadDocument.setProgress(data.getPercent());
                holder.documentTitle.setVisibility(View.VISIBLE);
                holder.fileSizeDocument.setVisibility(View.VISIBLE);
                String size = calculateSize(filesize);

                holder.fileSizeDocument.setText(size + "");

                String name = data.getContent();
                int lastindexslash = name.lastIndexOf("/") + 1;
                name = name.substring(lastindexslash);
                if (!data.getFileName().equals("")) {
                    name = data.getFileName();
                }
                holder.documentTitle.setText(name);
            }

        } else {


            // not exsist at all
            holder.documentLayout.setVisibility(View.VISIBLE);
            holder.documentImage.setVisibility(View.GONE);
            holder.mProgressDownloadVideo.setVisibility(View.GONE);
            holder.mProgressDownloadDocument.setVisibility(View.GONE);
            holder.mProgressDownloadDocumentInitial.setVisibility(View.GONE);
            holder.retryDownloadDocument.setVisibility(View.VISIBLE);
            holder.retryDownloadDocument.setVisibility(View.VISIBLE);
            holder.documentTitle.setVisibility(View.VISIBLE);
            holder.fileSizeDocument.setVisibility(View.VISIBLE);
            String size = calculateSize(filesize);

            holder.fileSizeDocument.setText(size);

            String name = data.getContent();
            int lastindexslash = name.lastIndexOf("/") + 1;
            name = name.substring(lastindexslash);
            if (!data.getFileName().equals("")) {
                name = data.getFileName();
            }
            holder.documentTitle.setText(name);

            holder.retryDownloadDocumentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!isActivated) {


                        DatabaseHandler db = new DatabaseHandler(context);
                        db.updatePercent(data.getMid(), 0);

                        String s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();
                        final FileManager fileManager = new FileManager();
                        String sssa = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);

                        // download statted
                        Intent intent = new Intent(context, DownloadService.class);
                        intent.putExtra("url", s);
                        intent.putExtra("mid", mid);
                        intent.putExtra("fileAdd", sssa);
                        intent.putExtra("type", "file");
                        intent.putExtra("receiver", new DownloadReceiver(new Handler()));
                        context.startService(intent);
                        UpdateUi(data.getMid());
                    } else {
                        EventBus bus = EventBus.getDefault();
                        MessengerBus messengerBus = new MessengerBus("clicked", position);
                        bus.post(messengerBus);
                    }
                }
            });
        }


    }

    private void manageMyFile(final MyViewHolder holder, final PrivateMessageDataBase data, final String mid, int position) {


        String localAddress = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
        int filesize = Integer.parseInt(data.getSize());
        if (isExistComplitly(localAddress, filesize, "file")) {
            if (data.getPercent() == 100) {
                showFile(holder, data, position);

            } else {
                showFileUpdating(holder, data.getPercent());
                // file ghataan dar hale ersale
            }
        } else if (isExistPartialy(localAddress, filesize, "file")) {
            showFileUpdating(holder, data.getPercent());
            // file dar hale downloade
        } else {
            if (data.getPercent() == 100) {

                startDownloadFile(holder, data, position);

            } else {
                startWhitoutDownloadFile(holder);
            }
            // file bayad download she
        }


    }


    private void manageVide(final MyViewHolder holder, final PrivateMessageDataBase data, final String mid, final int position) {


        String localAddress = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
        int filesize = Integer.parseInt(data.getSize());

        String addressWithOutPostFix = data.getContent().substring(0, data.getContent().lastIndexOf("."));
        int index = addressWithOutPostFix.lastIndexOf("/");
        addressWithOutPostFix = addressWithOutPostFix.substring(index + 1);
        String tumbnailAddress = InfoOfServer.MultiMediaIpServer + "/uploads/" + addressWithOutPostFix + "_1.png";
        showTumbnail(tumbnailAddress, holder.videoThumbnailFile);
        // if getpercent ==0   we shod download
        // if getpercent >0     we should show progress
        // if get percent ==100 download completed

        if (isExistComplitly(localAddress, filesize, "video")) {


            // download kamele va ma miaim va docmeye play mizarim
            holder.videoLayout.setVisibility(View.VISIBLE);
            holder.videoThumbnailFile.setVisibility(View.VISIBLE);
            holder.downloadVideo.setVisibility(View.GONE);
            holder.fileSizeVideo.setVisibility(View.GONE);
            holder.mProgressDownloadVideoInitial.setVisibility(View.GONE);
            holder.mProgressDownloadVideo.setVisibility(View.GONE);
            holder.playBtnVideo.setVisibility(View.VISIBLE);

            /*Glide.with(context).load(InfoOfServer.IpServer + "/uploads/" + data.getContent()).into(holder.videoThumbnailFile);
*/
            holder.playBtnVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isActivated) {
                        Intent intent = new Intent(context, VideoPlayer.class);
                        intent.putExtra("path", data.getContent());
                        context.startActivity(intent);
                    } else {
                        EventBus bus = EventBus.getDefault();
                        MessengerBus messengerBus = new MessengerBus("clicked", position);
                        bus.post(messengerBus);
                    }
                }
            });
        } else if (isExistPartialy(localAddress, filesize, "video")) {

            // bakhshi az an download shode
            if (data.getPercent() == 0) {
                holder.videoLayout.setVisibility(View.VISIBLE);
                holder.videoThumbnailFile.setVisibility(View.VISIBLE);
                holder.downloadVideo.setVisibility(View.GONE);
                holder.fileSizeVideo.setVisibility(View.GONE);
                holder.playBtnVideo.setVisibility(View.GONE);
                holder.mProgressDownloadVideoInitial.setVisibility(View.VISIBLE);
                holder.mProgressDownloadVideo.setVisibility(View.GONE);
            } else {
                holder.videoLayout.setVisibility(View.VISIBLE);
                holder.videoThumbnailFile.setVisibility(View.VISIBLE);
                holder.downloadVideo.setVisibility(View.GONE);
                holder.fileSizeVideo.setVisibility(View.GONE);
                holder.mProgressDownloadVideoInitial.setVisibility(View.GONE);
                holder.mProgressDownloadVideo.setVisibility(View.VISIBLE);
                holder.playBtnVideo.setVisibility(View.GONE);
                holder.mProgressDownloadVideo.setVisibility(View.VISIBLE);
                holder.mProgressDownloadVideo.setIndeterminate(false);
                holder.mProgressDownloadVideo.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.light_green), PorterDuff.Mode.SRC_IN);
                holder.mProgressDownloadVideo.setProgress(data.getPercent());
            }

        } else {
            // file kolan nist
            holder.videoLayout.setVisibility(View.VISIBLE);
            holder.videoThumbnailFile.setVisibility(View.VISIBLE);
            holder.downloadVideo.setVisibility(View.VISIBLE);
            holder.fileSizeVideo.setVisibility(View.VISIBLE);
            holder.mProgressDownloadVideoInitial.setVisibility(View.GONE);
            holder.mProgressDownloadVideo.setVisibility(View.GONE);
            holder.playBtnVideo.setVisibility(View.GONE);

            String size = calculateSize(filesize);
            holder.fileSizeVideo.setText(size);
            holder.downloadVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isActivated) {
                        holder.mProgressDownloadVideoInitial.setVisibility(View.VISIBLE);
                        holder.mProgressDownloadVideo.setVisibility(View.GONE);
                        holder.downloadVideo.setVisibility(View.GONE);

                        String s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();
                        final FileManager fileManager = new FileManager();
                        String sssa = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);


                        // download statted
                        Intent intent = new Intent(context, DownloadService.class);
                        intent.putExtra("url", s);
                        intent.putExtra("mid", mid);
                        intent.putExtra("fileAdd", sssa);
                        intent.putExtra("type", "video");
                        intent.putExtra("receiver", new DownloadReceiver(new Handler()));
                        context.startService(intent);

                    } else {
                        EventBus bus = EventBus.getDefault();
                        MessengerBus messengerBus = new MessengerBus("clicked", position);
                        bus.post(messengerBus);
                    }

                }
            });
        }


    }

    private void manageMyVide(final MyViewHolder holder, final PrivateMessageDataBase data, final String mid, int position) {

        final FileManager fileManager = new FileManager();
        String localAddress = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
        int filesize = Integer.parseInt(data.getSize());


        String addressWithOutPostFix = data.getContent().substring(0, data.getContent().lastIndexOf("."));
        int index = addressWithOutPostFix.lastIndexOf("/");
        addressWithOutPostFix = addressWithOutPostFix.substring(index + 1);
        String tumbnailAddress = InfoOfServer.MultiMediaIpServer + "/uploads/" + addressWithOutPostFix + "_1.png";
        showTumbnail(tumbnailAddress, holder.videoThumbnailFile);

        if (isExistComplitly(localAddress, filesize, "video")) {
            if (data.getPercent() == 100) {
                showVideo(holder, data, position);

            } else {
                showVideoUpdating(holder, data.getPercent());
                // file ghataan dar hale ersale
            }
        } else if (isExistPartialy(localAddress, filesize, "video")) {
            showVideoUpdating(holder, data.getPercent());
            // file dar hale downloade
        } else {
            if (data.getPercent() == 100) {

                startDownloadVideo(holder, data, position);

            } else {
                startWhitoutDownloadVideo(holder);
            }
            // file bayad download she
        }


    }


    private void manageAudio(final MyViewHolder holder, final PrivateMessageDataBase data, final String mid, final int position) {

        final String localAddress = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
        int filesize = Integer.parseInt(data.getSize());


        final FileManager fileManager = new FileManager();


        if (isExistComplitly(localAddress, filesize, "audio")) {

            holder.playBtnAudio.setVisibility(View.VISIBLE);
            holder.retryDownloadAudioButton.setVisibility(View.GONE);
            holder.audioLayout.setVisibility(View.VISIBLE);
            holder.mProgressDownloadAudio.setVisibility(View.GONE);
            holder.mProgressDownloadAudioInitial.setVisibility(View.GONE);


            if (data.getMid().equals(playingMid)) {
                holder.pauseBtnAudio.setVisibility(View.VISIBLE);
                holder.playBtnAudio.setVisibility(View.GONE);
                updatePlayingView(holder);
            } else {
                holder.pauseBtnAudio.setVisibility(View.GONE);
                holder.playBtnAudio.setVisibility(View.VISIBLE);
            }


            holder.pauseBtnAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    releaseMediaPlayer();
                    holder.pauseBtnAudio.setVisibility(View.GONE);
                    holder.playBtnAudio.setVisibility(View.VISIBLE);

                }
            });


            holder.playBtnAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isActivated) {
                        String path = "file://" + fileManager.makeFile("audio").toString() + "/" + localAddress;
                        startMediaPlayer(mid,path, holder);
                    } else {
                        EventBus bus = EventBus.getDefault();
                        MessengerBus messengerBus = new MessengerBus("clicked", position);
                        bus.post(messengerBus);
                    }
                }
            });
        } else if (isExistPartialy(localAddress, filesize, "audio")) {
            if (data.getPercent() == 0) {
                holder.playBtnAudio.setVisibility(View.GONE);
                holder.retryDownloadAudioButton.setVisibility(View.GONE);
                holder.audioLayout.setVisibility(View.VISIBLE);
                holder.mProgressDownloadAudio.setVisibility(View.GONE);
                holder.mProgressDownloadAudioInitial.setVisibility(View.VISIBLE);


            } else {
                holder.playBtnAudio.setVisibility(View.GONE);
                holder.retryDownloadAudioButton.setVisibility(View.GONE);
                holder.audioLayout.setVisibility(View.VISIBLE);
                holder.mProgressDownloadAudio.setVisibility(View.VISIBLE);
                holder.mProgressDownloadAudioInitial.setVisibility(View.GONE);
                holder.mProgressDownloadAudio.setIndeterminate(false);
                holder.mProgressDownloadAudio.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.light_green), PorterDuff.Mode.SRC_IN);
                holder.mProgressDownloadAudio.setProgress(data.getPercent());
            }

        } else {
            //file not exist
            holder.playBtnAudio.setVisibility(View.GONE);
            holder.retryDownloadAudioButton.setVisibility(View.VISIBLE);
            holder.audioLayout.setVisibility(View.VISIBLE);
            holder.mProgressDownloadAudio.setVisibility(View.GONE);
            holder.mProgressDownloadAudioInitial.setVisibility(View.GONE);
            holder.retryDownloadAudioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!isActivated) {
                        String s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();

                        String sssa = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
                        Intent intent = new Intent(context, DownloadService.class);
                        intent.putExtra("url", s);
                        intent.putExtra("mid", mid);
                        intent.putExtra("fileAdd", sssa);
                        intent.putExtra("type", "audio");
                        intent.putExtra("receiver", new DownloadReceiver(new Handler()));
                        context.startService(intent);
                    } else {
                        EventBus bus = EventBus.getDefault();
                        MessengerBus messengerBus = new MessengerBus("clicked", position);
                        bus.post(messengerBus);
                    }
                }
            });

        }


    }

    private void manageMyAudio(final MyViewHolder holder, final PrivateMessageDataBase data, final String mid, final int position) {
        final FileManager fileManager = new FileManager();
        String localAddress = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
        int filesize = Integer.parseInt(data.getSize());
        if (isExistComplitly(localAddress, filesize, "audio")) {
            if (data.getPercent() == 100) {
                showAudio(holder, data, position);
            } else {
                showAudioUpdating(holder, data.getPercent());
                // file ghataan dar hale ersale
            }
        } else if (isExistPartialy(localAddress, filesize, "audio")) {
            showAudioUpdating(holder, data.getPercent());
            // file dar hale downloade
        } else {
            if (data.getPercent() == 100) {
                startDownloadAudio(holder, data, position);
            } else {
                startWhitoutDownloadAudio(holder);
            }
            // file bayad download she
        }


    }


    public void showAudio(final MyViewHolder holder, final PrivateMessageDataBase data, final int position) {
        holder.playBtnAudio.setVisibility(View.VISIBLE);
        holder.retryDownloadAudioButton.setVisibility(View.GONE);
        holder.audioLayout.setVisibility(View.VISIBLE);
        holder.retryDownloadAudioButton.setVisibility(View.GONE);
        holder.mProgressDownloadAudio.setVisibility(View.GONE);
        holder.mProgressDownloadAudioInitial.setVisibility(View.GONE);


        if (data.getMid().equals(playingMid)) {
            holder.pauseBtnAudio.setVisibility(View.VISIBLE);
            holder.playBtnAudio.setVisibility(View.GONE);
            updatePlayingView(holder);

        } else {
            holder.pauseBtnAudio.setVisibility(View.GONE);
            holder.playBtnAudio.setVisibility(View.VISIBLE);
        }
        holder.pauseBtnAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                releaseMediaPlayer();
                holder.pauseBtnAudio.setVisibility(View.GONE);
                holder.playBtnAudio.setVisibility(View.VISIBLE);

            }
        });

        holder.playBtnAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isActivated) {
                    FileManager fileManager = new FileManager();
                    String localAddress = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
                    String path = "file://" + fileManager.makeFile("audio").toString() + "/" + localAddress;
                    startMediaPlayer(data.getMid(),path ,holder );
                } else {
                    EventBus bus = EventBus.getDefault();
                    MessengerBus messengerBus = new MessengerBus("clicked", position);
                    bus.post(messengerBus);
                }
            }
        });
    }



    public void releaseMediaPlayer() {
        if(mediaPlayer[0]!=null) {
            mediaPlayer[0].release();
        }
        mediaPlayer[0] = null;
    }

    private void startMediaPlayer(String mid, String path, final MyViewHolder holder) {
        playingMid = mid;
        if (mediaPlayer[0] != null) {

            mediaPlayer[0].stop();

        }
        mediaPlayer[0] = new MediaPlayer();
        try {
            mediaPlayer[0].setDataSource(path);
            mediaPlayer[0].prepare();
            mediaPlayer[0].start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer[0].setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                holder.playBtnAudio.setVisibility(View.VISIBLE);
                holder.pauseBtnAudio.setVisibility(View.GONE);
                playingMid = "";
                holder.audioCurrentDurationAudio.setText("00:00/");
                holder.audioSeekBar.setProgress(0);
                releaseMediaPlayer();
            }
        });

        startTimer( holder);
        updatePlayingView(holder);

    }

    private void startTimer(final MyViewHolder holder) {
        mytimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        holder.audioSeekBar.setProgress(mediaPlayer[0].getCurrentPosition());
                        holder.audioCurrentDurationAudio.setVisibility(View.VISIBLE);
                        int current = mediaPlayer[0].getCurrentPosition();
                        String time = String.format("%02d:%02d",
                                TimeUnit.MILLISECONDS.toMinutes(current),
                                TimeUnit.MILLISECONDS.toSeconds(current) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(current))
                        );
                        holder.audioCurrentDurationAudio.setText(time + "/");
                    }
                });

            }
        }, 0, 1000);
    }

    private void updatePlayingView(MyViewHolder holder) {
        holder.audioTotalDurationAudio.setVisibility(View.VISIBLE);
        int duration = mediaPlayer[0].getDuration();

        String time = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
        );
        holder.audioTotalDurationAudio.setText(time);
        holder.audioSeekBar.setMax(mediaPlayer[0].getDuration());
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (list.get(position) != null) {
            final PrivateMessageDataBase data = list.get(position);
            int type = 1;
            if (data.getSender().equals(userName)) {
                type = 1;
            } else {
                type = 2;
            }


            // Just as an example, return 0 or 2 depending on position
            // Note that unlike in ListView adapters, types don't have to be contiguous
            return type;
        } else {
            return 0;
        }
    }

    public void UpdateUi(String mid) {
        int index = IndexOfObject(mid);
        DatabaseHandler db = new DatabaseHandler(context);
        PrivateMessageDataBase pmd = db.GetSpecialPMessage(mid);

        list.set(index, pmd);

        notifyItemChanged(index, pmd);
    }

    public void UpdateList(PrivateMessageDataBase pmdb) {
        list.add(pmdb);
    }

    public int IndexOfObject(String mid) {

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) != null) {
                if (list.get(i).getMid().equals(mid)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public void removeItem(int position) {
        this.list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount() - position);
    }

    public void AddTOList(PrivateMessageDataBase item) {
        list.add(item);
    }

    private void openFile(File pdfFile, String sssa) {
        if (pdfFile.exists()) //Checking for the file is exist or not
        {
            Uri path = Uri.fromFile(pdfFile);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (sssa.toString().contains(".doc") || sssa.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(path, "application/msword");
            } else if (sssa.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(path, "application/pdf");
            } else if (sssa.toString().contains(".ppt") || sssa.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(path, "application/vnd.ms-powerpoint");
            } else if (path.toString().contains(".xls") || path.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(path, "application/vnd.ms-excel");
            } else if (path.toString().contains(".zip") || path.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(path, "application/x-wav");
            } else if (path.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(path, "application/rtf");
            } else if (path.toString().contains(".wav") || path.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(path, "audio/x-wav");
            } else if (path.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(path, "image/gif");
            } else if (path.toString().contains(".jpg") || path.toString().contains(".jpeg") || path.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(path, "image/jpeg");
            } else if (path.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(path, "text/plain");
            } else if (path.toString().contains(".3gp") || path.toString().contains(".mpg") || path.toString().contains(".mpeg") || path.toString().contains(".mpe") || path.toString().contains(".mp4") || path.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(path, "video/*");
            } else {
                intent.setDataAndType(path, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);


        } else {

            Toast.makeText(context, "The file not exists! ", Toast.LENGTH_SHORT).show();

        }

    }

    private class DownloadReceiver extends ResultReceiver {
        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == DownloadService.UPDATE_PROGRESS) {


                int progress = resultData.getInt("progress");
                String mid = resultData.getString("mid");


                int index = IndexOfObject(mid);

                if (index != -1) {

                    PrivateMessageDataBase data = list.get(index);
                    if (progress != data.getPercent()) {
                        DatabaseHandler dbh = new DatabaseHandler(context);
                        dbh.updatePercent(mid, progress);
                        UpdateUi(mid);
                    }
                }

            }
        }
    }


    private boolean isExistComplitly(String localAddress, int filesize, String type) {
        final FileManager fileManager = new FileManager();


        if (fileManager.isExist(localAddress, type)) {
            if (fileManager.fileSize(localAddress, type) >= filesize) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean isExistPartialy(String localAddress, int filesize, String type) {
        final FileManager fileManager = new FileManager();


        if (fileManager.isExist(localAddress, type)) {
            if (fileManager.fileSize(localAddress, type) == filesize) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public void showFile(final MyViewHolder holder, final PrivateMessageDataBase data, final int position) {
        holder.documentLayout.setVisibility(View.VISIBLE);
        holder.documentImage.setVisibility(View.VISIBLE);
        holder.retryDownloadDocument.setVisibility(View.GONE);
        holder.mProgressDownloadDocument.setVisibility(View.GONE);
        holder.mProgressDownloadDocumentInitial.setVisibility(View.GONE);
        holder.documentTitle.setVisibility(View.VISIBLE);
        holder.fileSizeDocument.setVisibility(View.VISIBLE);

        int filesize = Integer.parseInt(data.getSize());

        String size = calculateSize(filesize);
        holder.fileSizeDocument.setText(size);

        String name = data.getContent();
        int lastindexslash = name.lastIndexOf("/") + 1;
        name = name.substring(lastindexslash);

        if (!data.getFileName().equals("")) {
            name = data.getFileName();
        }
        holder.documentTitle.setText(name);
        holder.documentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isActivated) {
                    FileManager fileManager = new FileManager();
                    String sssa = "file://" + fileManager.makeFile("file").toString() + "/" + data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
                    File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/saffron/Documents");

                    File pdfFile = new File(dir, data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1));//File path

                    openFile(pdfFile, sssa);
                } else {
                    EventBus bus = EventBus.getDefault();
                    MessengerBus messengerBus = new MessengerBus("clicked", position);
                    bus.post(messengerBus);
                }
            }
        });
    }

    public void showVideo(final MyViewHolder holder, final PrivateMessageDataBase data, final int position) {
        holder.videoLayout.setVisibility(View.VISIBLE);
        holder.videoThumbnailFile.setVisibility(View.VISIBLE);
        holder.downloadVideo.setVisibility(View.GONE);
        holder.fileSizeVideo.setVisibility(View.GONE);
        holder.mProgressDownloadVideoInitial.setVisibility(View.GONE);
        holder.mProgressDownloadVideo.setVisibility(View.GONE);
        holder.playBtnVideo.setVisibility(View.VISIBLE);

        holder.playBtnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isActivated) {
                    Intent intent = new Intent(context, VideoPlayer.class);
                    intent.putExtra("path", data.getContent());

                    context.startActivity(intent);
                } else {
                    EventBus bus = EventBus.getDefault();
                    MessengerBus messengerBus = new MessengerBus("clicked", position);
                    bus.post(messengerBus);
                }
            }
        });
    }


    private void showImage(MyViewHolder holder, PrivateMessageDataBase data, final int position) {
        holder.imageLayout.setVisibility(View.VISIBLE);
        holder.imageFile.setVisibility(View.VISIBLE);
        holder.downloadImage.setVisibility(View.GONE);
        holder.mProgressDownloadImageInitial.setVisibility(View.GONE);
        holder.mProgressDownloadImage.setVisibility(View.GONE);
        holder.mProgressUploadImageInitial.setVisibility(View.GONE);
        holder.mProgressUploadImage.setVisibility(View.GONE);
        //ax toye hafeze dar bakhshe axha hast

        FileManager fileManager = new FileManager();
        // this is tumbnail address
        String s = "file://" + fileManager.makeFile("photo").toString() + "/" + data.getContent();

        if (data.getContent().contains(".gif")) {
            Glide.with(context)
                    .load(s)
                    .asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .into(holder.imageFile);

        } else {
            Glide.with(context)
                    .load(s)
                    .asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .into(holder.imageFile);

        }
        final String finalS = s;
        holder.imageFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isActivated) {
                    Intent intent = new Intent(context, ImageViewer.class);
                    intent.putExtra("path", finalS);

                    context.startActivity(intent);
                } else {
                    EventBus bus = EventBus.getDefault();
                    MessengerBus messengerBus = new MessengerBus("clicked", position);
                    bus.post(messengerBus);
                }

            }
        });
    }

    private void showImageUpdating(MyViewHolder holder, int percent) {
        holder.imageLayout.setVisibility(View.VISIBLE);
        holder.imageFile.setVisibility(View.INVISIBLE);
        holder.downloadImage.setVisibility(View.GONE);
        holder.mProgressDownloadImageInitial.setVisibility(View.GONE);
        holder.mProgressDownloadImage.setVisibility(View.VISIBLE);
        //ax toye hafeze dar bakhshe axha hast
        holder.mProgressDownloadImage.setIndeterminate(false);
        holder.mProgressDownloadImage.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.light_green), PorterDuff.Mode.SRC_IN);
        holder.mProgressDownloadImage.setProgress(percent);
    }

    private void showVideoUpdating(MyViewHolder holder, int percent) {
        holder.videoLayout.setVisibility(View.VISIBLE);
        holder.videoThumbnailFile.setVisibility(View.VISIBLE);
        holder.downloadVideo.setVisibility(View.GONE);
        holder.fileSizeVideo.setVisibility(View.GONE);
        holder.mProgressDownloadVideoInitial.setVisibility(View.GONE);
        holder.mProgressDownloadVideo.setVisibility(View.GONE);
        holder.playBtnVideo.setVisibility(View.GONE);
        if (percent == 0) {
            holder.mProgressDownloadVideoInitial.setVisibility(View.VISIBLE);
            holder.mProgressDownloadVideo.setVisibility(View.GONE);

        } else {
            holder.mProgressDownloadVideoInitial.setVisibility(View.GONE);
            holder.mProgressDownloadVideo.setVisibility(View.VISIBLE);
        }

        holder.mProgressDownloadVideo.setIndeterminate(false);
        holder.mProgressDownloadVideo.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.light_green), PorterDuff.Mode.SRC_IN);
        holder.mProgressDownloadVideo.setProgress(percent);
    }

    private void showAudioUpdating(MyViewHolder holder, int percent) {
        holder.playBtnAudio.setVisibility(View.GONE);
        holder.audioLayout.setVisibility(View.VISIBLE);
        holder.retryDownloadAudioButton.setVisibility(View.GONE);
        holder.mProgressDownloadAudioInitial.setVisibility(View.GONE);
        holder.mProgressDownloadAudio.setVisibility(View.GONE);
        if (percent == 0) {
            holder.mProgressDownloadAudioInitial.setVisibility(View.VISIBLE);
            holder.mProgressDownloadAudio.setVisibility(View.GONE);

        } else {
            holder.mProgressDownloadAudioInitial.setVisibility(View.GONE);
            holder.mProgressDownloadAudio.setVisibility(View.VISIBLE);
            holder.mProgressDownloadAudio.setIndeterminate(false);
            holder.mProgressDownloadAudio.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.light_green), PorterDuff.Mode.SRC_IN);


            holder.mProgressDownloadAudio.setProgress(percent);
        }

    }

    private void showFileUpdating(MyViewHolder holder, int percent) {
        holder.documentLayout.setVisibility(View.VISIBLE);
        holder.documentImage.setVisibility(View.INVISIBLE);
        holder.retryDownloadDocument.setVisibility(View.GONE);

        if (percent == 0) {
            holder.mProgressDownloadDocumentInitial.setVisibility(View.VISIBLE);
            holder.mProgressDownloadDocument.setVisibility(View.GONE);


        } else {
            holder.mProgressDownloadDocumentInitial.setVisibility(View.GONE);
            holder.mProgressDownloadDocument.setVisibility(View.VISIBLE);
            holder.mProgressDownloadDocument.setIndeterminate(false);
            holder.mProgressDownloadDocument.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.colorGrayDark), PorterDuff.Mode.SRC_IN);
            holder.mProgressDownloadDocument.setProgress(percent);

        }
        holder.documentTitle.setVisibility(View.GONE);
        holder.fileSizeDocument.setVisibility(View.GONE);

    }

    private void startDownloadImage(final MyViewHolder holder, PrivateMessageDataBase data) {
        holder.imageLayout.setVisibility(View.VISIBLE);
        holder.downloadImage.setVisibility(View.GONE);
        holder.mProgressDownloadImageInitial.setVisibility(View.VISIBLE);
        holder.mProgressDownloadImage.setVisibility(View.GONE);

        holder.imageFile.setVisibility(View.INVISIBLE);
        holder.mProgressDownloadImageInitial.setVisibility(View.VISIBLE);
        holder.fileSizeImage.setVisibility(View.GONE);
        holder.downloadImage.setVisibility(View.GONE);

        String s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();
        String sssa = data.getContent();


        // download statted
        Intent intent = new Intent(context, DownloadService.class);
        intent.putExtra("url", s);
        intent.putExtra("mid", data.getMid());
        intent.putExtra("fileAdd", sssa);
        intent.putExtra("type", "photo");
        intent.putExtra("receiver", new DownloadReceiver(new Handler()));
        context.startService(intent);

    }

    private void startDownloadFile(final MyViewHolder holder, final PrivateMessageDataBase data, final int position) {
        holder.documentLayout.setVisibility(View.VISIBLE);
        holder.retryDownloadDocument.setVisibility(View.VISIBLE);
        holder.mProgressDownloadDocument.setVisibility(View.GONE);
        holder.mProgressUploadDocumentInitial.setVisibility(View.GONE);
        holder.documentImage.setVisibility(View.GONE);
        int filesize = Integer.parseInt(data.getSize());

        String sizeStr = calculateSize(filesize);
        holder.fileSizeVideo.setText(sizeStr);

        holder.retryDownloadDocument.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                if (!isActivated) {
                                                                    DatabaseHandler db = new DatabaseHandler(context);
                                                                    db.updatePercent(data.getMid(), 0);

                                                                    final String s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();
                                                                    final String sssa = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
                                                                    // download statted
                                                                    Intent intent = new Intent(context, DownloadService.class);
                                                                    intent.putExtra("url", s);
                                                                    intent.putExtra("mid", data.getMid());
                                                                    intent.putExtra("fileAdd", sssa);
                                                                    intent.putExtra("type", "file");
                                                                    intent.putExtra("receiver", new DownloadReceiver(new Handler()));
                                                                    context.startService(intent);
                                                                    UpdateUi(data.getMid());
                                                                } else {
                                                                    EventBus bus = EventBus.getDefault();
                                                                    MessengerBus messengerBus = new MessengerBus("clicked", position);
                                                                    bus.post(messengerBus);
                                                                }
                                                            }
                                                        }
        );
    }

    private String calculateSize(int filesize) {
        String strSize = "";
        if (filesize >= 1024) {
            filesize = filesize / 1024;
            if (filesize >= 1024) {
                strSize = (((filesize) / 1024)) + " MB";
            } else {
                strSize = filesize + " KB";
            }
        } else {
            strSize = filesize + " B";
        }
        return strSize;
    }

    private void startDownloadVideo(final MyViewHolder holder, final PrivateMessageDataBase data, final int position) {


        holder.videoLayout.setVisibility(View.VISIBLE);
        holder.videoThumbnailFile.setVisibility(View.VISIBLE);
        holder.downloadVideo.setVisibility(View.VISIBLE);
        holder.fileSizeVideo.setVisibility(View.VISIBLE);
        holder.mProgressDownloadVideoInitial.setVisibility(View.GONE);
        holder.mProgressDownloadVideo.setVisibility(View.GONE);
        holder.playBtnVideo.setVisibility(View.GONE);
        int filesize = Integer.parseInt(data.getSize());

        String size = calculateSize(filesize);
        holder.fileSizeVideo.setText(size);
        final String s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();
        final String sssa = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
        holder.downloadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isActivated) {
                    DatabaseHandler db = new DatabaseHandler(context);
                    db.updatePercent(data.getMid(), 0);


                    // download statted
                    Intent intent = new Intent(context, DownloadService.class);
                    intent.putExtra("url", s);
                    intent.putExtra("mid", data.getMid());
                    intent.putExtra("fileAdd", sssa);
                    intent.putExtra("type", "video");
                    intent.putExtra("receiver", new DownloadReceiver(new Handler()));
                    context.startService(intent);
                    UpdateUi(data.getMid());

                } else {
                    EventBus bus = EventBus.getDefault();
                    MessengerBus messengerBus = new MessengerBus("clicked", position);
                    bus.post(messengerBus);
                }

            }
        });


    }

    private void startDownloadAudio(final MyViewHolder holder, final PrivateMessageDataBase data, final int position) {
        holder.playBtnAudio.setVisibility(View.GONE);
        holder.retryDownloadAudioButton.setVisibility(View.VISIBLE);
        holder.audioLayout.setVisibility(View.VISIBLE);
        holder.mProgressDownloadAudio.setVisibility(View.GONE);
        holder.mProgressDownloadAudioInitial.setVisibility(View.GONE);

        holder.retryDownloadAudioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isActivated) {


                    DatabaseHandler db = new DatabaseHandler(context);
                    db.updatePercent(data.getMid(), 0);

                    final String s = InfoOfServer.MultiMediaIpServer + "/uploads/" + data.getContent();
                    final String sssa = data.getMid() + "." + data.getContent().substring(data.getContent().lastIndexOf(".") + 1);
                    // download statted
                    Intent intent = new Intent(context, DownloadService.class);
                    intent.putExtra("url", s);
                    intent.putExtra("mid", data.getMid());
                    intent.putExtra("fileAdd", sssa);
                    intent.putExtra("type", "audio");
                    intent.putExtra("receiver", new DownloadReceiver(new Handler()));
                    context.startService(intent);
                    UpdateUi(data.getMid());
                } else {
                    EventBus bus = EventBus.getDefault();
                    MessengerBus messengerBus = new MessengerBus("clicked", position);
                    bus.post(messengerBus);
                }


            }
        });


    }

    private void startWhitoutDownload(final MyViewHolder holder) {
        holder.imageLayout.setVisibility(View.VISIBLE);
        holder.downloadImage.setVisibility(View.GONE);
        holder.mProgressDownloadImageInitial.setVisibility(View.VISIBLE);
        holder.mProgressDownloadImage.setVisibility(View.GONE);

        holder.imageFile.setVisibility(View.INVISIBLE);
        holder.mProgressDownloadImageInitial.setVisibility(View.VISIBLE);
        holder.fileSizeImage.setVisibility(View.GONE);
        holder.downloadImage.setVisibility(View.GONE);


    }

    private void startWhitoutDownloadVideo(final MyViewHolder holder) {
        holder.videoLayout.setVisibility(View.VISIBLE);
        holder.videoThumbnailFile.setVisibility(View.VISIBLE);
        holder.downloadVideo.setVisibility(View.GONE);
        holder.fileSizeVideo.setVisibility(View.GONE);
        holder.mProgressDownloadVideoInitial.setVisibility(View.VISIBLE);
        holder.mProgressDownloadVideo.setVisibility(View.GONE);
        holder.playBtnVideo.setVisibility(View.GONE);


    }

    private void startWhitoutDownloadAudio(final MyViewHolder holder) {


        holder.playBtnAudio.setVisibility(View.GONE);
        holder.audioLayout.setVisibility(View.VISIBLE);
        holder.retryDownloadAudioButton.setVisibility(View.GONE);
        holder.mProgressDownloadAudioInitial.setVisibility(View.GONE);
        holder.mProgressDownloadAudio.setVisibility(View.GONE);

        holder.mProgressDownloadAudioInitial.setVisibility(View.VISIBLE);
        holder.mProgressDownloadAudio.setVisibility(View.GONE);


    }

    private void startWhitoutDownloadFile(final MyViewHolder holder) {
        holder.documentLayout.setVisibility(View.VISIBLE);
        holder.documentImage.setVisibility(View.GONE);
        holder.mProgressDownloadDocumentInitial.setVisibility(View.VISIBLE);
        holder.mProgressDownloadDocument.setVisibility(View.GONE);
        holder.retryDownloadDocument.setVisibility(View.GONE);

        holder.documentTitle.setVisibility(View.GONE);
        holder.fileSizeDocument.setVisibility(View.GONE);

    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
            if (!isActivated)
                isActivated = true;
        }
        UpdateUi(list.get(pos).getMid());
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public PrivateMessageDataBase getItem(int position) {
        return list.get(position);
    }


    private void showTumbnail(String address, ImageView img) {
        Glide.with(context).load(address).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop().into(img);

    }

    private void getForwarded(String mid) {
        JSONObject data = new JSONObject();
        JSONObject packet = new JSONObject();
        try {
            data.put("mid", mid);
            packet.put("data", data);
            packet.put("tc", System.currentTimeMillis() / 1000);
            packet.put("signature", PreferencesData.getString(context, "signature"));
            SaffronEmitter emitter = new SaffronEmitter();
            emitter.EmitForwarded(data);
           /* Security sec = new Security(context);
            sec.Pack(packet, "forwarded");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void textLayoutManager(MyViewHolder holder, PrivateMessageDataBase data, int position) {
        holder.base.setVisibility(View.VISIBLE);
        holder.sticker.setVisibility(View.GONE);
        holder.message.setVisibility(View.VISIBLE);
        holder.audioLayout.setVisibility(View.GONE);
        holder.documentLayout.setVisibility(View.GONE);
        holder.imageLayout.setVisibility(View.GONE);
        holder.videoLayout.setVisibility(View.GONE);
        String plainText = Html.fromHtml(data.getContent()).toString();

        String text = plainText.replace("@", "");
        String emoji = emojiParser.demojizedText(text);

        holder.message.setText(emoji);
    }

    public void fileLayoutManager(MyViewHolder holder, PrivateMessageDataBase data, int position) {
        holder.base.setVisibility(View.VISIBLE);
        holder.sticker.setVisibility(View.GONE);
        holder.audioLayout.setVisibility(View.GONE);
        holder.documentLayout.setVisibility(View.VISIBLE);
        holder.imageLayout.setVisibility(View.GONE);
        holder.videoLayout.setVisibility(View.GONE);
        holder.message.setVisibility(View.INVISIBLE);

        if (data.getSender().equals(userName)) {
            manageMyFile(holder, data, data.getMid(), position);
        } else {
            manageFile(holder, data, data.getMid(), position);
        }
    }

    public void voiceLayoutManager(MyViewHolder holder, PrivateMessageDataBase data, int position) {
        holder.base.setVisibility(View.VISIBLE);
        holder.sticker.setVisibility(View.GONE);
        holder.audioLayout.setVisibility(View.VISIBLE);
        holder.documentLayout.setVisibility(View.GONE);
        holder.imageLayout.setVisibility(View.GONE);
        holder.videoLayout.setVisibility(View.GONE);
        holder.message.setVisibility(View.INVISIBLE);
        if (data.getSender().equals(userName)) {
            manageMyAudio(holder, data, data.getMid(), position);
        } else {
            manageAudio(holder, data, data.getMid(), position);
        }
    }

    public void videoLayoutManager(MyViewHolder holder, PrivateMessageDataBase data, int position) {
        holder.base.setVisibility(View.VISIBLE);
        holder.sticker.setVisibility(View.GONE);
        holder.audioLayout.setVisibility(View.GONE);
        holder.documentLayout.setVisibility(View.GONE);
        holder.imageLayout.setVisibility(View.GONE);
        holder.videoLayout.setVisibility(View.VISIBLE);
        holder.message.setVisibility(View.INVISIBLE);

        if (data.getSender().equals(userName)) {
            manageMyVide(holder, data, data.getMid(), position);
        } else {
            manageVide(holder, data, data.getMid(), position);
        }
    }

    public void photoLayoutManager(MyViewHolder holder, PrivateMessageDataBase data, int position) {
        holder.base.setVisibility(View.VISIBLE);
        holder.sticker.setVisibility(View.GONE);
        holder.message.setVisibility(View.INVISIBLE);
        holder.audioLayout.setVisibility(View.GONE);
        holder.documentLayout.setVisibility(View.GONE);
        holder.imageLayout.setVisibility(View.VISIBLE);
        holder.videoLayout.setVisibility(View.GONE);


        //check mikonim aya ax male karbare yani bayad upload beshe ya as samte servere bayad download beshe

        if (data.getSender().equals(userName)) {
            manageMyPhoto(holder, data, data.getMid(), position);

        } else {
            managePhoto(holder, data, position);

        }
    }

    public void stickerLayoutManagment(MyViewHolder holder, PrivateMessageDataBase data) {
        String s = InfoOfServer.MultiMediaIpServer + "/stickers/" + data.getContent();
        holder.base.setVisibility(View.GONE);
        holder.sticker.setVisibility(View.VISIBLE);
        Glide.with(context).load(s).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.sticker)
        ;
    }



}



