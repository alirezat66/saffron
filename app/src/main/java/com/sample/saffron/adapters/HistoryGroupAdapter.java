package com.sample.saffron.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hugomatilla.audioplayerview.AudioPlayerView;
import com.sample.saffron.R;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.entity.groupData.HistoryResponseData;
import com.sample.saffron.helper.EmojiTextView;
import com.sample.saffron.helper.PersianCalendar;
import com.sample.saffron.helper.emojiParser;
import com.sample.saffron.typo.InfoOfServer;
import com.squareup.picasso.Picasso;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import uk.co.jakelee.vidsta.VidstaPlayer;

/**
 * Created by alireza on 3/30/2017.
 */

public class HistoryGroupAdapter extends BaseAdapter {


    public List<HistoryResponseData> listItems;
    Activity context;
    View parentView;
    AdapterView.OnItemClickListener onItemClickListener;
    public boolean flagisupdate = false;
    public int positionItem;
    public int itemId;

    public HistoryGroupAdapter(Activity context/*, AdapterView.OnItemClickListener onItemClickListener*/, List<HistoryResponseData> list) {
        this.listItems = list;
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }


    public void add(HistoryResponseData hd) {
        listItems.add(hd);
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public HistoryResponseData getItem(int position) {
        return listItems.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        public String id;
        public TextView txtMessage,txtTime,txtFrom;
        public ImageView img_history;
        public RelativeLayout card;
        public AudioPlayerView audioPlayerView;
        public VidstaPlayer vidstaPlayer;

        /*
        ImageView img;
*/

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HistoryGroupAdapter.ViewHolder holder;
        HistoryResponseData data = listItems.get(position);

        if (convertView == null || !((ViewHolder)convertView.getTag()).id.equals(this.listItems.get(position).getMid()) ) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


          if(data.getFrom().equals(PreferencesData.getString(context, "userName"))){
           //   convertView = vi.inflate(R.layout.item_message, null);

          }else {
              convertView = vi.inflate(R.layout.item_message_two_group, null);

          }
            holder = new HistoryGroupAdapter.ViewHolder();
            holder.txtMessage      = (TextView)convertView.findViewById(R.id.txt_message);
            holder.txtTime = (TextView)convertView.findViewById(R.id.txt_time);
            holder.txtFrom = (TextView)convertView.findViewById(R.id.txt_from);
            holder.img_history  = (ImageView)convertView.findViewById(R.id.image_sticker);
            holder.card = (RelativeLayout) convertView.findViewById(R.id.card);
            holder.id = listItems.get(position).getMid();
            holder.audioPlayerView= (AudioPlayerView) convertView.findViewById(R.id.audio);
            holder.vidstaPlayer= (VidstaPlayer) convertView.findViewById(R.id.player);
            if(data.getMtype().equals("sticker")||data.getMtype().equals("photo")){
                holder.txtMessage.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
                holder.card.setVisibility(View.GONE);
                holder.audioPlayerView.setVisibility(View.GONE);
                holder.vidstaPlayer.setVisibility(View.GONE);
            }else if(data.getMtype().equals("voice"))
            {
                holder.txtMessage.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
                holder.card.setVisibility(View.GONE);
                holder.img_history.setVisibility(View.GONE);
                holder.vidstaPlayer.setVisibility(View.GONE);
            }
            else if(data.getMtype().equals("video")){

                holder.txtMessage.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
                holder.card.setVisibility(View.GONE);
                holder.img_history.setVisibility(View.GONE);
                holder.audioPlayerView.setVisibility(View.GONE);
            }else {
                holder.img_history.setVisibility(View.GONE);
                holder.audioPlayerView.setVisibility(View.GONE);
                holder.vidstaPlayer.setVisibility(View.GONE);
            }
            if(data.getMtype().equals("photo")||data.getMtype().equals("sticker")){
                holder.img_history.setVisibility(View.VISIBLE);
            }
            if(data.getMtype().equals("voice")){
                holder.audioPlayerView.setVisibility(View.VISIBLE);
            }else if(data.getMtype().equals("video")){
                holder.vidstaPlayer.setVisibility(View.VISIBLE);
            }
            convertView.setTag(holder);
        } else {
            holder = (HistoryGroupAdapter.ViewHolder) convertView.getTag();
            if(data.getMtype().equals("sticker")||data.getMtype().equals("photo")){
                holder.txtMessage.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
                holder.card.setVisibility(View.GONE);
                holder.audioPlayerView.setVisibility(View.GONE);
                holder.vidstaPlayer.setVisibility(View.GONE);
            }else if(data.getMtype().equals("voice"))
            {
                holder.txtMessage.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
                holder.card.setVisibility(View.GONE);
                holder.img_history.setVisibility(View.GONE);
                holder.vidstaPlayer.setVisibility(View.GONE);
            }
            else if(data.getMtype().equals("video")){

                holder.txtMessage.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
                holder.card.setVisibility(View.GONE);
                holder.img_history.setVisibility(View.GONE);
                holder.audioPlayerView.setVisibility(View.GONE);
            }else {
                holder.img_history.setVisibility(View.GONE);
                holder.audioPlayerView.setVisibility(View.GONE);
                holder.vidstaPlayer.setVisibility(View.GONE);
            }
            if(data.getMtype().equals("photo")||data.getMtype().equals("sticker")){
                holder.img_history.setVisibility(View.VISIBLE);
            }
            if(data.getMtype().equals("voice")){
                holder.audioPlayerView.setVisibility(View.VISIBLE);
            }else if(data.getMtype().equals("video")){
                holder.vidstaPlayer.setVisibility(View.VISIBLE);
            }
        }



        String text = data.getContent().replace("@","");
        String emoji = emojiParser.demojizedText(text);
        holder.txtMessage.setText(emoji);
        holder.txtFrom.setText(data.getFrom());
        String res = "";

        PersianCalendar p = new PersianCalendar(Long.parseLong(data.getTimestamp()));

        long millis =p.getTimeInMillis();
        Timestamp timestamp = new Timestamp(millis+1990);
        Date date = new Date(timestamp.getTime());

        // S is the millisecond
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy' 'HH:mm:ss:");
        holder.txtTime.setText(simpleDateFormat.format(date));
        if(data.getMtype().equals("sticker")){
            String ipStr  = InfoOfServer.IpServer;
            String s = ipStr+"/stickers/"+data.getContent();
            Picasso.with(context)
                    .load(s)
                    .placeholder(R.drawable.ic_logo)
                    .error(R.drawable.ic_failure)
                    .into(holder.img_history);
        }else if(data.getMtype().equals("photo")) {
            String ipStr  =InfoOfServer.IpServer;
            String s = ipStr+"/uploads/"+data.getContent();
            Picasso.with(context)
                    .load(s)
                    .placeholder(R.drawable.ic_logo)
                    .error(R.drawable.ic_failure)
                    .into(holder.img_history);
        }else if(data.getMtype().equals("voice")){
            String ipStr  =InfoOfServer.IpServer;
            String s = ipStr+"/uploads/"+data.getContent();
            holder.audioPlayerView.withUrl(s);
        }else if(data.getMtype().equals("video")){
            String ipStr  =InfoOfServer.IpServer;
            String s = ipStr+"/uploads/"+data.getContent();
            try {
                holder.vidstaPlayer.setVideoSource(s);

            }catch (Exception e){
                e.printStackTrace();
            }
            holder.vidstaPlayer.setAutoLoop(false);
            holder.vidstaPlayer.setAutoPlay(false);
        }


        return convertView;

    }
}
