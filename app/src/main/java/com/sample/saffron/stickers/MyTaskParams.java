package com.sample.saffron.stickers;

import android.graphics.Bitmap;
import android.os.Environment;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class MyTaskParams {

    ImageView iv;
    String url;
    String cat;
    String name;
    Bitmap bitmap;

    MyTaskParams(ImageView iv, String url,String cat,String name) {

        this.iv=iv;
        this.url=url;
        this.cat = cat;
        this.name=name;
    }

    public String getCat() {
        return cat;
    }

    public String getName() {
        return name;
    }

    void setImage()
    {

        saveBitmap(bitmap,getRoot());
        iv.setImageBitmap(bitmap);

    }


    void saveBitmap(Bitmap bmp,String filename)
    {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    boolean isDownlaod()
    {
        File f= new File(getRoot());
        if(f.exists())
            return true;
        else
            return false;
    }


    String getRoot()
    {
        String root = Environment.getExternalStorageDirectory().toString()+ "/saffron/.stickers/";
        File r=new File(root);
        if(!r.exists())
            r.mkdirs();

        int idLastDash =url.lastIndexOf("/");
        return root +url.substring(idLastDash);
    }
}
