package com.sample.saffron.stickers;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sample.saffron.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DialogStiker extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ArrayList<Categorys> list = new ArrayList<>();

    public DialogStiker() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_stiker, container, false);

        list.add(new Categorys("http://ibrand.rabinaco.ir/api/get_design.php?cat=3", "http://94.182.215.114/stickers//drindrin/815936823_13046_14513829976121778273.png"));
        list.add(new Categorys("http://ibrand.rabinaco.ir/api/get_design.php?cat=2", "http://94.182.215.114/stickers/koobide/koobide1.png"));
        list.add(new Categorys("http://ibrand.rabinaco.ir/api/get_design.php?cat=2", "http://94.182.215.114/stickers/manblue/mb1.png"));


        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        return v;
    }


    private void setupTabIcons() {
        for (int i = 0; i < list.size(); i++) {

            ImageView tabOne = new ImageView(getActivity());
            tabOne = (ImageView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            Picasso.with(getActivity()).load(list.get(i).firdtpic).into(tabOne);
            tabLayout.getTabAt(i).setCustomView(tabOne);
        }


    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        for (int i = 0; i < list.size(); i++) {
            adapter.addFragment(new DesignFragment(i+""),i+ "");
        }

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return null;
        }
    }


}
