package com.sample.saffron.stickers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.sample.saffron.R;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.typo.InfoOfServer;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import de.greenrobot.event.EventBus;


public class DesignAdapter extends RecyclerView.Adapter<DesignAdapter.MyViewHolder> {

    private List<Design> list;
    Context _context;
    RelativeLayout rv;
    ImageView i;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.imageDesign);

        }
    }


    public DesignAdapter(List<Design> list, Context context) {
        this.list = list;
        this._context = context;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.design_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Design designObj = list.get(position);


        MyTaskParams obj = new MyTaskParams(holder.image, InfoOfServer.StickerUrl+designObj.getCategory()+"/"+ designObj.getPic(),designObj.getCategory(),designObj.getPic());
        if (obj.isDownlaod()) {

            File f = new File(obj.getRoot());
            Picasso.with(_context).load(f).into(holder.image);
            itemOnClick(obj);
        }
        else
            new GetImage().execute(obj);



    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    private class GetImage extends AsyncTask<MyTaskParams, Void, MyTaskParams> {


        @Override
        protected MyTaskParams doInBackground(MyTaskParams... obj) {

            String imageURL = obj[0].url;

            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
                obj[0].bitmap = bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return obj[0];
        }

        @Override
        protected void onPostExecute(final MyTaskParams result) {
            result.setImage();
            itemOnClick(result);
        }
    }

   void  itemOnClick(final MyTaskParams obj){
       obj.iv.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               EventBus bus = EventBus.getDefault();
               MessengerBus mess = new MessengerBus("stickerChose",obj.getCat()+"/"+obj.getName(),"");
               bus.post(mess);
         //      Toast.makeText(_context,""+obj.getCat()+"/"+obj.getName(),Toast.LENGTH_LONG).show();
           }
       });
   }

}