package com.sample.saffron.stickers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sample.saffron.R;

import java.util.ArrayList;


public class DesignFragment extends Fragment {
    String url;


    private RecyclerView recyclerView;
    RelativeLayout box;
    ImageView img;
    private DesignAdapter mAdapter;
//    LinearLayout progressWindow;



    public DesignFragment() {
        // Required empty public constructor
    }
    @SuppressLint("ValidFragment")
    public DesignFragment(String url) {
       this.url=url;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_design, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
//        progressWindow = (LinearLayout) v.findViewById(R.id.progressWindow);

//        progressWindow.setVisibility(View.VISIBLE);
        getDesigns(url);
        return v;
    }


    void getDesigns(String url) {
        if(url.equals("0")){
            ArrayList<Design> listCategorys = new ArrayList<Design>();
            listCategorys.add(new Design("drindrin","421809784_12948_5705873685908358276.png"));
            listCategorys.add(new Design("drindrin","421813902_12540_5499322969324487668.png"));
            listCategorys.add(new Design("drindrin","421815968_12501_17317415885150039779.png"));
            listCategorys.add(new Design("drindrin","421820460_14187_6002163764181230967.png"));
            listCategorys.add(new Design("drindrin","421825521_6260_4086829096162421796.png"));
            listCategorys.add(new Design("drindrin","421830698_12822_16542878895798537797.png"));
            listCategorys.add(new Design("drindrin","421832686_6671_12218099667828714118.png"));
            listCategorys.add(new Design("drindrin","421837400_13305_6939833623343388480.png"));
            listCategorys.add(new Design("drindrin","421839996_6082_1488233220512783957.png"));
            listCategorys.add(new Design("drindrin","421902024_14261_7965297302099435559.png"));
            listCategorys.add(new Design("drindrin","421907337_12109_16013577729896948031.png"));
            listCategorys.add(new Design("drindrin","421912910_5936_5165765038761404403.png"));
            listCategorys.add(new Design("drindrin","422115026_879_8228923410525847991.png"));
            listCategorys.add(new Design("drindrin","422310284_1494_1443950649958564883.png"));
            listCategorys.add(new Design("drindrin","422315443_474_459483112587234271.png"));
            listCategorys.add(new Design("drindrin","422318098_1824_9450385174982249311.png"));
            listCategorys.add(new Design("drindrin","422326676_2448_5361561820139021937.png"));
            listCategorys.add(new Design("drindrin","422329946_537_9397036610817551594.png"));
            listCategorys.add(new Design("drindrin","814703952_15086_9595912131875109079.png"));
            listCategorys.add(new Design("drindrin","814721901_13030_9091292034281829167.png"));
            listCategorys.add(new Design("drindrin","814722112_18652_5771359328767694362.png"));
            listCategorys.add(new Design("drindrin","814802432_11690_6897184742336513417.png"));
            listCategorys.add(new Design("drindrin","814804108_2141_13529749948345100284.png"));
            listCategorys.add(new Design("drindrin","814804168_1213_5678661795568427122.png"));
            listCategorys.add(new Design("drindrin","814807482_14363_16644795383940747873.png"));
            listCategorys.add(new Design("drindrin","814814490_13574_4195751596015593055.png"));
            listCategorys.add(new Design("drindrin","814820112_13940_7307635124294930047.png"));
            listCategorys.add(new Design("drindrin","814828727_12627_16195006870914626447.png"));
            listCategorys.add(new Design("drindrin","815902261_1236_10358402547108752512.png"));
            listCategorys.add(new Design("drindrin","815904706_12048_10028334173418218382.png"));
            listCategorys.add(new Design("drindrin","815904824_664_14608550161596391763.png"));
            listCategorys.add(new Design("drindrin","815905992_13627_12420161200815713139.png"));
            listCategorys.add(new Design("drindrin","815909420_12682_17372897256219541814.png"));
            listCategorys.add(new Design("drindrin","815910680_14322_16536758036182092529.png"));
            listCategorys.add(new Design("drindrin","815910882_12693_1117375618896887134.png"));
            listCategorys.add(new Design("drindrin","815914060_690_9597059103493060985.png"));
            listCategorys.add(new Design("drindrin","815917318_714_18291219540674529827.png"));
            listCategorys.add(new Design("drindrin","815918706_14344_9123197884769630456.png"));
            listCategorys.add(new Design("drindrin","815923096_799_16037006298880708845.png"));
            listCategorys.add(new Design("drindrin","815924891_13880_17427768838522869146.png"));
            listCategorys.add(new Design("drindrin","815926445_11254_9048265549249217703.png"));
            listCategorys.add(new Design("drindrin","815928523_13000_4183521454235634809.png"));
            listCategorys.add(new Design("drindrin","815932864_13700_16926508791138468897.png"));
            listCategorys.add(new Design("drindrin","815934225_13125_15727657460297379317.png"));
            listCategorys.add(new Design("drindrin","815934588_11602_11659328526768630053.png"));
            listCategorys.add(new Design("drindrin","815936823_13046_14513829976121778273.png"));
            listCategorys.add(new Design("drindrin","816205477_12205_16187077566281757868.png"));
            listCategorys.add(new Design("drindrin","816205643_700_15038390922799829584.png"));
            listCategorys.add(new Design("drindrin","816209361_14197_11394888346143729512.png"));
            listCategorys.add(new Design("drindrin","816209785_11899_3144863730688798882.png"));
            listCategorys.add(new Design("drindrin","816211910_13261_7388546412052464822.png"));
            listCategorys.add(new Design("drindrin","816212681_889_6906878643064971269.png"));
            listCategorys.add(new Design("drindrin","816216127_766_7497856971366477538.png"));
            listCategorys.add(new Design("drindrin","816220427_11231_2092471236016358965.png"));
            listCategorys.add(new Design("drindrin","816220938_11752_4194545651997638441.png"));
            listCategorys.add(new Design("drindrin","816221377_14012_3721197660212665858.png"));
            listCategorys.add(new Design("drindrin","816223757_12688_3729540703161106743.png"));
            listCategorys.add(new Design("drindrin","816225800_10603_6666201137926653486.png"));



            adaptRecycle(listCategorys);
        }else if(url.equals("1")){
            ArrayList<Design> listCategorys2 = new ArrayList<Design>();

            listCategorys2.add(new Design("koobide","koobide1.png"));
            listCategorys2.add(new Design("koobide","koobide2.png"));
            listCategorys2.add(new Design("koobide","koobide3.png"));
            listCategorys2.add(new Design("koobide","koobide4.png"));
            listCategorys2.add(new Design("koobide","koobide5.png"));
            listCategorys2.add(new Design("koobide","koobide6.png"));
            listCategorys2.add(new Design("koobide","koobide7.png"));
            listCategorys2.add(new Design("koobide","koobide8.png"));
            listCategorys2.add(new Design("koobide","koobide9.png"));
            listCategorys2.add(new Design("koobide","koobide10.png"));
            listCategorys2.add(new Design("koobide","koobide11.png"));

            adaptRecycle(listCategorys2);
        }else if(url.equals("2")){
            ArrayList<Design> listCategorys2 = new ArrayList<Design>();

            listCategorys2.add(new Design("manblue","mb1.png"));
            listCategorys2.add(new Design("manblue","mb2.png"));
            listCategorys2.add(new Design("manblue","mb3.png"));
            listCategorys2.add(new Design("manblue","mb4.png"));
            listCategorys2.add(new Design("manblue","mb5.png"));
            listCategorys2.add(new Design("manblue","mb6.png"));
            listCategorys2.add(new Design("manblue","mb7.png"));
            listCategorys2.add(new Design("manblue","mb8.png"));
            listCategorys2.add(new Design("manblue","mb9.png"));
            listCategorys2.add(new Design("manblue","mb10.png"));
            listCategorys2.add(new Design("manblue","mb11.png"));
            listCategorys2.add(new Design("manblue","mb12.png"));
            listCategorys2.add(new Design("manblue","mb13.png"));
            listCategorys2.add(new Design("manblue","mb14.png"));
            listCategorys2.add(new Design("manblue","mb15.png"));
            listCategorys2.add(new Design("manblue","mb16.png"));
            listCategorys2.add(new Design("manblue","mb17.png"));
            listCategorys2.add(new Design("manblue","mb18.png"));
            listCategorys2.add(new Design("manblue","mb19.png"));

            adaptRecycle(listCategorys2);
        }




    }




    void adaptRecycle(ArrayList<Design> listCategorys) {






        mAdapter = new DesignAdapter(listCategorys, getActivity());
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
}
