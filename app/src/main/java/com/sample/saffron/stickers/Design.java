package com.sample.saffron.stickers;


import static android.R.attr.category;
import static android.R.attr.id;

public class Design {

//    public int id;
    public String title;
//    public String xfields;
    public String pic;
    public String category;
//    public String category;


    public Design() {
    }

    public Design( String category,String pic) {

        this.category = category;
        this.pic = pic;
    }

    public String getPic() {
        return pic;
    }

    public String getCategory() {
        return category;
    }
}
