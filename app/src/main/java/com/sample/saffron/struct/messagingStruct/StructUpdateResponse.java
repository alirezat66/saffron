package com.sample.saffron.struct.messagingStruct;

public class StructUpdateResponse {
    public String mid;
    public String type;
    public String mtype;
    public String from;
    public String to;
    public String size;
    public String timestamp;
    public String content;
    public String replyto;
    public String forwarded;
}
