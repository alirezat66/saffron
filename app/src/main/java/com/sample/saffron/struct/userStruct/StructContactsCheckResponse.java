package com.sample.saffron.struct.userStruct;

public class StructContactsCheckResponse {
    public String phone;
    public String username;
    public String firstname;
    public String lastname;
    public String imageuri;
}
