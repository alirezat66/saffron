package com.sample.saffron.filter;

import android.content.res.TypedArray;
import android.widget.Filter;

import com.sample.saffron.R;
import com.sample.saffron.activities.SignupActivity;
import com.sample.saffron.models.Country;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
@SuppressWarnings("ResourceType")
public class CountryListFilter extends Filter {

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();

        String[] countriesArray = SignupActivity.ctx.getResources().getStringArray(R.array.countries);

        List<String> countriesStaticList = new ArrayList<>(Arrays.asList(countriesArray));

        List<Country> countriesList = new ArrayList<>();

        String dataString;
        int holderint;
        TypedArray item;

        for (int i = 0; i < countriesStaticList.size(); i++) {
            Country tempCountry = new Country();
            dataString = "data" + (new DecimalFormat("000").format(i));
            holderint = SignupActivity.ctx.getResources().getIdentifier(dataString, "array",
                    SignupActivity.ctx.getPackageName());
            item = SignupActivity.ctx.getResources().obtainTypedArray(holderint);

            tempCountry.setName(item.getText(1).toString());
            tempCountry.setPrefix(item.getText(0).toString());
            tempCountry.setFlag(item.getDrawable(3));

            countriesList.add(tempCountry);
        }

        if (constraint == null || constraint.length() == 0) {
            results.values = countriesList;
            results.count = countriesList.size();
        } else {

            List<Country> tempCountriesList = new ArrayList<>();

            for (Country c : countriesList) {
                if (c.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                    tempCountriesList.add(c);
                }
            }

            results.values = tempCountriesList;
            results.count = tempCountriesList.size();
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

    }
}
