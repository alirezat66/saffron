package com.sample.saffron.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.saffron.R;
import com.sample.saffron.entity.userData.ChallengeRequestData;
import com.sample.saffron.entity.userData.ChallengeResponseData;
import com.sample.saffron.entity.userData.SignUpRequestData;
import com.sample.saffron.entity.userData.SignUpResponseData;
import com.sample.saffron.entity.userData.SmsResponseData;
import com.sample.saffron.entity.userData.UpdateProfileInfoRequestData;
import com.sample.saffron.entity.userData.UpdateProfileInfoResponseData;
import com.sample.saffron.models.Country;
import com.sample.saffron.struct.userStruct.StructUpdateProfileInfoRequest;
import com.sample.saffron.struct.userStruct.StructUpdateProfileInfoResponse;
import com.sample.saffron.typo.InfoOfServer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SignupActivity extends AppCompatActivity {

    public static Context ctx;
    private int requestCodeFroActivity = 1;
    private DataUpdateReceiver dataUpdateReceiver;
    private TextView countryNameTextView, countryCodeTextView, errorTextView;
    private ImageView countryFlagImageView;
    private EditText mobileNumberEditText;
    public static ProgressDialog progressDialog;


    private boolean isSMSResReady = false;
    private boolean isSignUpResReady = false;
    private boolean isChallengeSent = false;

    private String CHALLENGE, SALT, USERNAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        setContentView(R.layout.activity_signup);

        ctx = this;

        errorTextView = (TextView) findViewById(R.id.errorTextView);

        countryCodeTextView = (TextView) findViewById(R.id.selectedCountryCode);
        countryCodeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        countryNameTextView = (TextView) findViewById(R.id.selectedCountryName);
        countryNameTextView.setFocusable(true);
        countryNameTextView.requestFocus();
        countryNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        countryFlagImageView = (ImageView) findViewById(R.id.selectedCountryFlag);
        countryFlagImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if (savedInstanceState == null) {
            setCountryData("Iran");
        }

        mobileNumberEditText = (EditText) findViewById(R.id.mobileNumber);

        Button btn_signUp = (Button) findViewById(R.id.signUpButton);
        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidNumber(mobileNumberEditText.getText().toString())) {
                    Log.d(Constants.LOGTAG, "SignUP button clicked, And phone number is valid");
                    SaffronEmitter emitter = new SaffronEmitter();
                    SignUpRequestData data = new SignUpRequestData();
                    data.phone = countryCodeTextView.getText().toString() + mobileNumberEditText.getText().toString();
                    data.timestamp = String.valueOf(System.currentTimeMillis() / 1000L);
                    emitter.EmitSignUpRequest(data);

                    progressDialog = ProgressDialog.show(ctx, "Please Wait", "You are going to be signed up");
                }

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == requestCodeFroActivity) {
            if (resultCode == Activity.RESULT_OK) {
                String countryName = data.getStringExtra("result");
                setCountryData(countryName);

            }
        }
    }

    private void setCountryData(String desiredCountryName) {
        String[] countriesArray = SignupActivity.ctx.getResources().getStringArray(R.array.countries);

        List<String> countriesStaticList = new ArrayList<>(Arrays.asList(countriesArray));

        List<Country> countriesList = new ArrayList<>();

        String dataString;
        int holderint;
        TypedArray item;

        for (int i = 0; i < countriesStaticList.size(); i++) {
            Country tempCountry = new Country();
            dataString = "data" + (new DecimalFormat("000").format(i));
            holderint = SignupActivity.ctx.getResources().getIdentifier(dataString, "array",
                    SignupActivity.ctx.getPackageName());
            item = SignupActivity.ctx.getResources().obtainTypedArray(holderint);


            countriesList.add(tempCountry);
        }

        Country tempCountry = new Country();
        for (Country c : countriesList) {
            if (c.getName().toLowerCase().startsWith(desiredCountryName.toLowerCase())) {
                tempCountry = c;
            }
        }

        countryNameTextView.setText(tempCountry.getName());
        countryCodeTextView.setText(tempCountry.getPrefix());
        countryFlagImageView.setImageDrawable(tempCountry.getFlag());
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(Constants.LOGTAG, "RRRRRRRRRRRRRRRRRRRRRRRRRRR");
            String myip = InfoOfServer.IpServer+":3000";

            Log.d(Constants.LOGTAG, "DataUpdateReceiver : " + myip);
            Log.d(Constants.LOGTAG, intent.getAction());
            if (intent.getAction().equals("EventSignUpRes")) {
                SignUpResponseData signupResData = (SignUpResponseData) intent.getExtras().getSerializable("key");
                Log.d(Constants.LOGTAG, "EventSignUpRes :" + signupResData.answer);


                switch (signupResData.answer) {
                    case Constants.SIGNUPRES_WAIT_FOR_CHALLENGE: {
                        SALT = signupResData.salt;
                        ctx.getSharedPreferences(Constants.PREFERENCES_SAFFRON, MODE_PRIVATE).edit().putString(Constants.PREFERENCES_SALT, SALT).commit();

                        isSignUpResReady = true;

                        Log.d(Constants.LOGTAG, "isSMSResReady = " + String.valueOf(isSMSResReady));

                        if (isSMSResReady && (!isChallengeSent)) {
                            // Send challenge request when SignUpRes is ready
                            isChallengeSent = true;
                            SaffronEmitter emitter = new SaffronEmitter();
                            ChallengeRequestData challengeRequestData = new ChallengeRequestData();
                            challengeRequestData.phone = countryCodeTextView.getText().toString() + mobileNumberEditText.getText().toString();
                            challengeRequestData.hchallenge = Helper.sha256(ctx.getSharedPreferences(Constants.PREFERENCES_SAFFRON, MODE_PRIVATE).getString(Constants.PREFERENCES_CHALLENGE, ""));
                            emitter.EmitChallengeRequest(challengeRequestData);
                        }

                    }
                    case Constants.SIGNUPRES_PHONE_ALREADY_EXISTS: {
                    }
                    case Constants.SIGNUPRES_REJECT: {

                    }
                    default: {

                    }

                }

            }
            if (intent.getAction().equals("EventSmsRes")) {
                SmsResponseData smsResponseData = (SmsResponseData) intent.getExtras().getSerializable("key");
                Log.d(Constants.LOGTAG, "SMS Response From server : " + smsResponseData.challenge);
                CHALLENGE = smsResponseData.challenge;

                Log.d(Constants.LOGTAG, "EventSMSRes " + CHALLENGE);

                ctx.getSharedPreferences(Constants.PREFERENCES_SAFFRON, MODE_PRIVATE).edit().putString(Constants.PREFERENCES_CHALLENGE, CHALLENGE).commit();


                Log.d(Constants.LOGTAG, "isSignUpResReady = " + String.valueOf(isSignUpResReady));

                isSMSResReady = true;

                if (isSignUpResReady && (!isChallengeSent)) {
                    // Send challenge request when SignUpRes is ready
                    isChallengeSent = true;
                    SaffronEmitter emitter = new SaffronEmitter();
                    ChallengeRequestData challengeRequestData = new ChallengeRequestData();
                    challengeRequestData.phone = countryCodeTextView.getText().toString() + mobileNumberEditText.getText().toString();
                    challengeRequestData.hchallenge = Helper.sha256(ctx.getSharedPreferences(Constants.PREFERENCES_SAFFRON, MODE_PRIVATE).getString(Constants.PREFERENCES_CHALLENGE, ""));
                    emitter.EmitChallengeRequest(challengeRequestData);
                }
            }
            if (intent.getAction().equals("EventChallengeRes")) {
                ChallengeResponseData challengeResponseData = (ChallengeResponseData) intent.getExtras().getSerializable("key");
                Log.d(Constants.LOGTAG, "Challenge Response From server : " + challengeResponseData.answer);

                switch (challengeResponseData.answer) {

                    // Store userdata in preferences
                    // Redirect to userProfile activity
                    case Constants.CHALLENGERES_OK: {
                        //USERNAME = challengeResponseData.username;
// Send update profile event
                        SaffronEmitter saffronEmitter = new SaffronEmitter();
                        UpdateProfileInfoRequestData updateProfileInfoRequestData = new UpdateProfileInfoRequestData();


                        Log.d(Constants.LOGTAG, "CCCCCCCCCCCCCCCCCCCCCCCCCCCC");
                        Log.d(Constants.LOGTAG, "Send update profile event ");

                        progressDialog.dismiss();

                        ArrayList<StructUpdateProfileInfoRequest> netTasksUpdateProfileInfoReq = new ArrayList<>();
                        StructUpdateProfileInfoRequest task = new StructUpdateProfileInfoRequest();
                        task.utype = Constants.UPDATEPROFILE_USERNAME;
                        task.value = "@" + countryCodeTextView.getText().toString() + mobileNumberEditText.getText().toString();
                        netTasksUpdateProfileInfoReq.add(task);

                        updateProfileInfoRequestData.updates = netTasksUpdateProfileInfoReq;
                        saffronEmitter.EmitUpdateProfileInfoRequest(updateProfileInfoRequestData);

                        progressDialog = ProgressDialog.show(ctx, "Please Wait", "Updating profile information");


                    }
                    case Constants.CHALLENGERES_WRONG: {

                    }
                    case Constants.CHALLENGERES_REJECT: {

                    }
                    case Constants.CHALLENGERES_PHONE_LOCKED: {

                    }
                    default: {

                    }
                }

            }


            if (intent.getAction().equals("EventUpdateProfileInfoRes")) {

                UpdateProfileInfoResponseData updateProfileInfoResponseData = (UpdateProfileInfoResponseData) intent.getExtras().getSerializable("key");

                Log.d(Constants.LOGTAG, "EventUpdateProfileInfoRes  ");


                progressDialog.dismiss();

                for (StructUpdateProfileInfoResponse task : updateProfileInfoResponseData.answers) {

                    Log.d(Constants.LOGTAG, "VVVVVVVVVVVVVVVVVVVVVVVV");
                    Log.d(Constants.LOGTAG, task.utype);


                    if (task.utype.equalsIgnoreCase(Constants.UPDATEPROFILE_USERNAME.toLowerCase())) {
                        progressDialog = new ProgressDialog(ctx);

                        progressDialog.setMessage("You are registered and your username is :" + task.result);
                        progressDialog.setCancelable(false);
                        progressDialog.setButton(DialogInterface.BUTTON_POSITIVE, "View your profile", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                // Goto Profile Activity

                                ctx.startActivity(new Intent(ctx,
                                        MyMainActivity.class));
                                finish();
                            }
                        });

                        progressDialog.show();
                    }
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dataUpdateReceiver == null) {
            dataUpdateReceiver = new DataUpdateReceiver();
            Log.d(Constants.LOGTAG, "dataUpdateReceiver is initialized");
        } else {
            Log.d(Constants.LOGTAG, "dataUpdateReceiver has been initialized");
        }
        IntentFilter intentFilter = new IntentFilter("EventSignUpRes");
        registerReceiver(dataUpdateReceiver, intentFilter);

        IntentFilter intentFilter2 = new IntentFilter("EventSmsRes");
        registerReceiver(dataUpdateReceiver, intentFilter2);

        IntentFilter intentFilter3 = new IntentFilter("EventChallengeRes");
        registerReceiver(dataUpdateReceiver, intentFilter3);

        IntentFilter intentFilter4 = new IntentFilter("EventUpdateProfileInfoRes");
        registerReceiver(dataUpdateReceiver, intentFilter4);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dataUpdateReceiver != null) unregisterReceiver(dataUpdateReceiver);
    }


    private boolean isValidNumber(String mobileNumber) {
        errorTextView.setText("");
        if (mobileNumber.isEmpty()) {
            errorTextView.setText("Please enter the phone number");
            return false;
        } else if (!mobileNumber.matches("[0-9]+")) {
            errorTextView.setText("Please enter numbers only");
            return false;
        } else if (mobileNumber.length() < 6) {
            errorTextView.setText("Phone number must contain at least 6 numbers");
            return false;
        } else {
            return true;
        }
    }

}
