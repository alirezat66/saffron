package com.sample.saffron.activities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;

import com.sample.saffron.Database.CounterDb;
import com.sample.saffron.Database.DatabaseHandler;
import com.sample.saffron.Database.PrivateMessageDataBase;
import com.sample.saffron.R;
import com.sample.saffron.client.LoginProcess;
import com.sample.saffron.client.Security;
import com.sample.saffron.client.UnPack;
import com.sample.saffron.entity.groupData.AddGroupUserResponseData;
import com.sample.saffron.entity.groupData.CreateGroupResponseData;
import com.sample.saffron.entity.groupData.GroupActivitiesResponseData;
import com.sample.saffron.entity.groupData.GroupInfoResponseData;
import com.sample.saffron.entity.groupData.GroupInfoUpdatedData;
import com.sample.saffron.entity.groupData.HistoryResponseData;
import com.sample.saffron.entity.groupData.LeaveGroupResponseData;
import com.sample.saffron.entity.groupData.RemoveGroupUserResponseData;
import com.sample.saffron.entity.groupData.UpdateGroupImageResponseData;
import com.sample.saffron.entity.groupData.UpdateGroupNameResponseData;
import com.sample.saffron.entity.groupData.UserAddedData;
import com.sample.saffron.entity.groupData.UserLeftData;
import com.sample.saffron.entity.groupData.UserRemovedData;
import com.sample.saffron.entity.groupData.YouAddedData;
import com.sample.saffron.entity.groupData.YouRemovedData;
import com.sample.saffron.entity.messageData.BinaryMessageReceivedData;
import com.sample.saffron.entity.messageData.DataChunkData;
import com.sample.saffron.entity.messageData.DeleteMessagesResponseData;
import com.sample.saffron.entity.messageData.EventsResponseData;
import com.sample.saffron.entity.messageData.GroupSendingData;
import com.sample.saffron.entity.messageData.GroupStopSendingData;
import com.sample.saffron.entity.messageData.GroupStopTypingData;
import com.sample.saffron.entity.messageData.GroupTypingData;
import com.sample.saffron.entity.messageData.PrivateSendingData;
import com.sample.saffron.entity.messageData.UpdateResponseData;
import com.sample.saffron.entity.securityData.GroupsSessionKeyResponseData;
import com.sample.saffron.entity.securityData.ServerPkResponseData;
import com.sample.saffron.entity.securityData.TimeSyncResponseData;
import com.sample.saffron.entity.userData.AccessResponseData;
import com.sample.saffron.entity.userData.ChallengeResponseData;
import com.sample.saffron.entity.userData.ContactDeletedData;
import com.sample.saffron.entity.userData.ContactJoinedData;
import com.sample.saffron.entity.userData.ContactOfflineData;
import com.sample.saffron.entity.userData.ContactOnlineData;
import com.sample.saffron.entity.userData.ContactsCheckResponseData;
import com.sample.saffron.entity.userData.ContactsStateResponseData;
import com.sample.saffron.entity.userData.ConversationResponseData;
import com.sample.saffron.entity.userData.InviteContactResponseData;
import com.sample.saffron.entity.userData.ProfileInfoResponseData;
import com.sample.saffron.entity.userData.SignOffResponseData;
import com.sample.saffron.entity.userData.SignOutResponseData;
import com.sample.saffron.entity.userData.SignUpResponseData;
import com.sample.saffron.entity.userData.SmsResponseData;
import com.sample.saffron.entity.userData.UpdateProfileInfoResponseData;
import com.sample.saffron.helper.*;
import com.sample.saffron.struct.groupStruct.StructGroupActivitiesResponse;
import com.sample.saffron.struct.groupStruct.StructGroupInfoResponse;
import com.sample.saffron.struct.messagingStruct.StructDeleteMessagesResponse;
import com.sample.saffron.struct.messagingStruct.StructEventsResponse;
import com.sample.saffron.struct.messagingStruct.StructUpdateResponse;
import com.sample.saffron.struct.securityStruct.StructGroupsSessionKeyResponse;
import com.sample.saffron.struct.userStruct.StructContactsCheckResponse;
import com.sample.saffron.struct.userStruct.StructContactsStateResponse;
import com.sample.saffron.struct.userStruct.StructUpdateProfileInfoResponse;
import com.sample.saffron.typo.InfoOfServer;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import de.greenrobot.event.EventBus;

public class SaffronService extends Service {

    private ArrayList<StructDeleteMessagesResponse> netTasksDeleteMessagesRes = new ArrayList<StructDeleteMessagesResponse>();
    private ArrayList<StructGroupsSessionKeyResponse> netTasksGroupsSessionKeyRes = new ArrayList<StructGroupsSessionKeyResponse>();
    private ArrayList<StructGroupInfoResponse> netTasksGroupInfoRes = new ArrayList<StructGroupInfoResponse>();
    private ArrayList<StructGroupActivitiesResponse> netTasksGroupActivitiesRes = new ArrayList<StructGroupActivitiesResponse>();
    private ArrayList<StructContactsStateResponse> netTasksContactsStateRes = new ArrayList<StructContactsStateResponse>();
    private ArrayList<StructUpdateResponse> netTasksUpdateRes = new ArrayList<StructUpdateResponse>();
    private ArrayList<StructEventsResponse> netTasksEventsRes = new ArrayList<StructEventsResponse>();
    private ArrayList<StructUpdateProfileInfoResponse> netTasksUpdateProfileRes = new ArrayList<StructUpdateProfileInfoResponse>();
    private ArrayList<StructContactsCheckResponse> netTasksContactsCheckRes = new ArrayList<StructContactsCheckResponse>();

    public static boolean isSign = false;
    private NotificationManager mNM;

    private Socket mSocket;

    {
        try {

            SSLContext mySSLContext = SSLContext.getInstance("SSL");
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }

                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            }};

            String myip = InfoOfServer.IpServer + ":3000";


            mySSLContext.init(null, trustAllCerts, null);


            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            IO.setDefaultSSLContext(mySSLContext);
            IO.Options opts = new IO.Options();

            opts = new IO.Options();
            opts.sslContext = mySSLContext;
            opts.secure = true;
            mSocket = IO.socket(myip, opts);


            mSocket.connect();

            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    PreferencesData.saveBoolean(AppController.getCurrentContext(), "LoginFromUser", false);
                    LoginProcess process = new LoginProcess();

                    if(PreferencesData.getString(AppController.getCurrentContext(),"phone","").equals("")){
                        EventBus bus = EventBus.getDefault();
                        MessengerBus messengerBus = new MessengerBus("challengeNotOk","");
                        bus.post(messengerBus);
                    }else {
                        process.Sync();

                    }
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {


                }

            });


        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    public void checker() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.LOGTAG, "Service onStartCommand called");

        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        //phase1
        mSocket.on("error-message", onErrorMessage);
        mSocket.on("web-access-res", onAccessRes);
        mSocket.on("sign-up-res", onSignUpRes);
        mSocket.on("sms", onSms);
        mSocket.on("web-challenge-res", onChallengeRes);
        mSocket.on("web-sign-in-res", onSigning);
        mSocket.on("sign-off-res", onSignoff);
        mSocket.on("sign-out-res", onSignout);
        mSocket.on("contacts-check-res", onContactsCheckRes);
        mSocket.on("contacts-state-res", onContactsStateRes);
        mSocket.on("update-profile-info-res", onUpdateProfileInfoRes);
        mSocket.on("history-res", onHistoryRes);

        mSocket.on("profile-info-res", onProfileInfoRes);
        mSocket.on("invite-contact-res", onInviteContactRes);
        mSocket.on("contact-joined", onContactJoined);
        mSocket.on("contact-online", onContactOnline);
        mSocket.on("contact-offline", onContactOffline);
        mSocket.on("contact-deleted", onContactDeleted);
        mSocket.on("member-res", onMemberResp);

        mSocket.on("conversations-res", onConversationRes);
        //phase2
        mSocket.on("create-group-res", onCreateGroupRes);
        mSocket.on("leave-group-res", onLeaveGroupRes);
        mSocket.on("add-group-user-res", onAddGroupUserRes);
        mSocket.on("remove-group-user-res", onRemoveGroupUserRes);
        mSocket.on("update-group-name-res", onUpdateGroupNameRes);
        mSocket.on("update-group-image-res", onUpdateGroupImageRes);
        mSocket.on("user-added", onUserAdded);
        mSocket.on("user-removed", onUserRemoved);
        mSocket.on("user-left", onUserLeft);
        mSocket.on("group-info-updated", onGroupInfoUpdated);
        mSocket.on("group-activities-res", onGroupActivitiesRes);
        mSocket.on("group-info-res", onGroupInfoRes);
        mSocket.on("you-added", onYouAdded);
        mSocket.on("you-removed", onYouRemoved);

        //phase3
        mSocket.on("time-sync-res", onTimeSyncRes);
        mSocket.on("server-pk-res", onServerPkRes);
        mSocket.on("groups-session-key-res", onGroupsSessionKeyRes);
        mSocket.on("user-status-res", onStatusRes);
        //phase4
        mSocket.on("private-message", onPrivateMessage);
        mSocket.on("group-message", onGroupMessage);
        mSocket.on("private-typing", onPrivateTyping);
        mSocket.on("private-stop-typing", onPrivateStopTyping);
        mSocket.on("group-typing", onGroupTyping);
        mSocket.on("group-stop-typing", onGroupStopTyping);
        mSocket.on("sent", onSent);
        mSocket.on("delivered", onDelivered);
        mSocket.on("seen-private", onSeen);
        mSocket.on("binary-message-res", onBinaryMessageRes);
        mSocket.on("data-chunk-ack", onDataChunkAck);
        mSocket.on("binary-message-received", onBinaryMessageReceived);
        mSocket.on("private-sending-data", onPrivateSendingData);
        mSocket.on("private-stop-sending-data", onPrivateStopSendingData);
        mSocket.on("group-sending-data", onGroupSendingData);
        mSocket.on("group-stop-sending-data", onGroupStopSendingData);
        mSocket.on("update-res", onUpdate);
        mSocket.on("events-res", onEventsRes);
        mSocket.on("delete-messages-res", onDeleteMessagesRes);
        mSocket.on("forwardedmessages-res", onForwardedRes);


        return Service.START_FLAG_REDELIVERY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Emitter.Listener onErrorMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String message;
                try {
                    message = data.getString("answer");
                    showNotification(message, "");
                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onSigning = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            JSONObject data = (JSONObject) args[0];
            if (data != null) {
                LoginProcess loginProcess = new LoginProcess();
                loginProcess.OnSignInFinish(data);
                 /*   SignInResponseData signinres = new SignInResponseData();
                 */
              /*      JSONArray packet = data.getJSONArray("packet");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("packet", packet);
                  *//*  answer = data.getString("answer");
                    sk = data.getString("sk");
                    iv = data.getString("iv");
                    signinres.answer = answer;
                    signinres.sk = sk;
                    signinres.iv = iv;
*//*
                    Security s = new Security(AppController.getCurrentContext());
                    s.UnpackSign(jsonObject);
                   *//* Intent intent = new Intent("EventSignInRes");
                    intent.putExtra("key", signinres);
                    sendBroadcast(intent);*//*

                    *//*if(answer.equals("OK"))
                        isSign = true;
*/
            } else {
                EventBus bus = EventBus.getDefault();
                MessengerBus mb = new MessengerBus("notSignIn", "");
                bus.post(mb);
            }


        }
    };

    private Emitter.Listener onSignUpRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String answer;
                String salt;
                try {
                    SignUpResponseData signupres = new SignUpResponseData();
                    answer = data.getString("answer");
                    salt = data.getString("salt");
                    signupres.answer = answer;
                    signupres.salt = salt;

                    Intent intent = new Intent("EventSignUpRes");
                    intent.putExtra("key", signupres);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onMemberResp = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];

                try {


                    JSONArray conversationsArray = data.getJSONArray("messages");

                  /*  profile.target = data.getString("target");
                    profile.fname = data.getString("firstname");
                    profile.lname = data.getString("lastname");
                    profile.imgUrl = data.getString("imageuri");
                    profile.phone =     data.getString("phone");
                    profile.lastonline = data.getString("lastonline");

*/
/*
                    EventBus eventBus = EventBus.getDefault();
                    MessengerBus message = new MessengerBus("messagesapp",1,conversationsArray);
                    eventBus.post(message);
*/
                    EventBus eventBus = EventBus.getDefault();
                    MessengerBus message = new MessengerBus("members",1,conversationsArray);
                    eventBus.post(message);
                } catch (JSONException e) {
                    return;
                }


            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };
    private Emitter.Listener onHistoryRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                UnPack.sendEvent(data,"history");

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };
    private Emitter.Listener onSms = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String challenge;
                try {
                    SmsResponseData smsdata = new SmsResponseData();
                    challenge = data.getString("challenge");
                    PreferencesData.saveString(AppController.getCurrentContext(),"chal",challenge);
                    smsdata.challenge = challenge;

                    /*
                    MessengerBus event = new MessengerBus("challengeRes",challenge);
                    EventBus bus = EventBus.getDefault();
                    bus.post(event);
*/
                        LoginProcess process = new LoginProcess();
                        process.OnChallengeRes(challenge);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onChallengeRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String answer;
                String username;
                String salt;
                try {
                    ChallengeResponseData challengeresdata = new ChallengeResponseData();
                    answer = data.getString("answer");
                    username = data.getString("username");
                    PreferencesData.saveString(AppController.getCurrentContext(),"userName",username);
                    salt = data.getString("salt");
                    challengeresdata.answer = answer;
                    challengeresdata.username = username;
                    challengeresdata.salt = salt;
                    PreferencesData.saveString(AppController.getCurrentContext(),"salt",salt);


                 //   boolean isLoginFromUser= PreferencesData.getBoolean(AppController.getCurrentContext(),"LoginFromUser",true);
                        LoginProcess process = new LoginProcess();
                        process.OnEventChallengeRes(answer);

                    /*;
*/


/*                    MessengerBus event = new MessengerBus("EventChallengeRes",answer);
                    EventBus bus = EventBus.getDefault();
                    bus.post(event);*/

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };
    private Emitter.Listener onAccessRes =new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String answer;
                try {
                    AccessResponseData accessResponseData = new AccessResponseData();
                    answer = data.getString("answer");
                    com.sample.saffron.helper.PreferencesData.saveBoolean(getApplicationContext(),"hasPasscode",data.getBoolean("hasPasscode"));
                    accessResponseData.answer = answer;

                /*    Intent intent = new Intent("`");
                    intent.putExtra("key", accessResponseData);
                    sendBroadcast(intent);*/

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };
    private Emitter.Listener onSignout = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String answer;
                try {
                    SignOutResponseData signoutres = new SignOutResponseData();
                    answer = data.getString("answer");
                    signoutres.answer = answer;

                    Intent intent = new Intent("EventSignOutRes");
                    intent.putExtra("key", signoutres);
                    sendBroadcast(intent);
                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onSignoff = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String answer;
                try {
                    SignOffResponseData signoffres = new SignOffResponseData();
                    answer = data.getString("answer");
                    signoffres.answer = answer;

                    Intent intent = new Intent("EventSignOffRes");
                    intent.putExtra("key", signoffres);
                    sendBroadcast(intent);
                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onContactsCheckRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            netTasksContactsCheckRes.clear();
            ContactsCheckResponseData contactcheckres = new ContactsCheckResponseData();
            String jsonStr = args[0].toString();
            JSONArray answers = null;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    answers = jsonObj.getJSONArray("contacts");
                    for (int i = 0; i < answers.length(); i++) {
                        JSONObject c = answers.getJSONObject(i);
                        //String username = c.getString("username");
                        String imageuri = c.getString("imageuri");
                        String lastname = c.getString("lastname");
                        String firstname = c.getString("firstname");
                        String phone = c.getString("phone");

                        StructContactsCheckResponse task = new StructContactsCheckResponse();
                        //task.username = username;
                        task.imageuri = imageuri;
                        task.lastname =lastname;
                        task.firstname =firstname;
                        task.phone = phone;

                        netTasksContactsCheckRes.add(task);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                contactcheckres.contacts = netTasksContactsCheckRes;
                Intent intent = new Intent("EventContactsCheckRes");
                intent.putExtra("key", contactcheckres);
                sendBroadcast(intent);

            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
        }
    };

    private Emitter.Listener onContactsStateRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            //showNotification(args[0].toString(),"hh");
            netTasksContactsStateRes.clear();
            ContactsStateResponseData contactstateres = new ContactsStateResponseData();

            String jsonStr = args[0].toString();
            try {
                JSONObject jObj = new JSONObject(jsonStr);
                String answer = jObj.getString("answer");
                contactstateres.answer = answer;

                JSONArray jArr = jObj.getJSONArray("contacts");
                for (int i=0; i < jArr.length(); i++) {
                    JSONObject obj = jArr.getJSONObject(i);
                    String username = obj.getString("username");
                    String state = obj.getString("state");
                    String lastonline = obj.getString("lastonline");

                    StructContactsStateResponse task = new StructContactsStateResponse();
                    task.username = username;
                    task.state =state;
                    task.lastonline = lastonline;
                    netTasksContactsStateRes.add(task);
                }

                contactstateres.contacts = netTasksContactsStateRes;
                Intent intent = new Intent("EventContactsStateRes");
                intent.putExtra("key", contactstateres);
                sendBroadcast(intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
    private Emitter.Listener onProfileInfoRes=new Emitter.Listener(){

        @Override
        public void call(final Object... args) {



            JSONObject data = (JSONObject) args[0];
            EventBus bus = EventBus.getDefault();
            MessengerBus messenger;
            if(PreferencesData.getString(AppController.getCurrentContext(),"profileRequest").equals("profile")) {

                 messenger= new MessengerBus("onProfileResProfile", "", data.toString());
            }else {
                messenger= new MessengerBus("onProfileResHome", "", data.toString());

            }
            bus.post(messenger);

        }
    };
    private Emitter.Listener onUpdateProfileInfoRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            netTasksUpdateProfileRes.clear();
            UpdateProfileInfoResponseData updateprofileres = new UpdateProfileInfoResponseData();
            String jsonStr = args[0].toString();
            JSONArray answers = null;
            if (jsonStr != null) {
              //  try {
              /*      JSONObject jsonObj = new JSONObject(jsonStr);
                    answers = jsonObj.getJSONArray("answers");
                    for (int i = 0; i < answers.length(); i++) {
                        JSONObject c = answers.getJSONObject(i);
                        String utype = c.getString("utype");
                        String result = c.getString("result");

                        StructUpdateProfileInfoResponse task = new StructUpdateProfileInfoResponse();
                        task.utype = utype;
                        task.result =result;
                        netTasksUpdateProfileRes.add(task);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                updateprofileres.answers = netTasksUpdateProfileRes;*/
                EventBus eventBus =EventBus.getDefault();
                MessengerBus bus = new MessengerBus("updateCompleted","",jsonStr);
                eventBus.post(bus);
              /*  Intent intent = new Intent("EventUpdateProfileInfoRes");
                intent.putExtra("key", updateprofileres);
                sendBroadcast(intent);
*/
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
        }
    };

    private Emitter.Listener onInviteContactRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String answer;
                try {
                    InviteContactResponseData invitecontactres = new InviteContactResponseData();
                    answer = data.getString("answer");
                    invitecontactres.answer = answer;

                    Intent intent = new Intent("EventInviteContactRes");
                    intent.putExtra("key", invitecontactres);
                    sendBroadcast(intent);
                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onContactJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            showNotification(args[0].toString(), "");
            try {
                JSONObject data = (JSONObject) args[0];
                String username;
                String phone;
                String firstname;
                String lastname;
                String imageurl;
                String timestamp;
                try {
                    ContactJoinedData contactjoineddata = new ContactJoinedData();
                    username = data.getString("username");
                    phone = data.getString("phone");
                    firstname = data.getString("firstname");
                    lastname = data.getString("lastname");
                    imageurl = data.getString("imageurl");
                    timestamp = data.getString("timestamp");
                    contactjoineddata.username = username;
                    contactjoineddata.phone = phone;
                    contactjoineddata.firstname = firstname;
                    contactjoineddata.lastname = lastname;
                    contactjoineddata.imageurl = imageurl;
                    contactjoineddata.timestamp = timestamp;

                    Intent intent = new Intent("EventContactJoined");
                    intent.putExtra("key", contactjoineddata);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onContactOnline = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("events","sent recieved");
            try {
                JSONObject data = (JSONObject) args[0];
                String mid = data.getString("username");




                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("onlinecontact",mid);
                eventBus.post(message);
            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };private Emitter.Listener onStatusRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("events","sent recieved");
            try {
                JSONObject data = (JSONObject) args[0];
                UnPack.sendEvent(data,"onStatusRes");

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onContactOffline = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String mid = data.getString("username");

                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("offlinecontact",mid);
                eventBus.post(message);
            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };
    private Emitter.Listener onConversationRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                    JSONObject data = (JSONObject) args[0];
                    EventBus bus = EventBus.getDefault();
                    MessengerBus  message = new MessengerBus("conversations",data.toString());
                    bus.post(message);

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };
    private Emitter.Listener onForwardedRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                    JSONObject data = (JSONObject) args[0];
                    UnPack.sendEvent(data,"forwarded");
            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onContactDeleted = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String username;
                try {
                    ContactDeletedData contactdeleted = new ContactDeletedData();
                    username = data.getString("username");
                    contactdeleted.username = username;
                    Intent intent = new Intent("EventContactDeleted");
                    intent.putExtra("key", contactdeleted);
                    sendBroadcast(intent);
                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onCreateGroupRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
                    try {
                        JSONObject data = (JSONObject) args[0];
                        String reqid;
                        String answer;
                        String groupid;
                        String imageuri;
                        String key;
                        String iv;
                        try {
                            CreateGroupResponseData creatgroupres = new CreateGroupResponseData();
                            reqid = data.getString("reqid");
                            answer = data.getString("answer");
                            groupid = data.getString("groupid");
                            imageuri = data.getString("imageuri");
                            key = data.getString("key");
                            iv = data.getString("iv");
                            creatgroupres.reqid = reqid;
                            creatgroupres.answer = answer;
                            creatgroupres.groupid = groupid;
                            creatgroupres.imageurl = imageuri;
                            creatgroupres.key = key;
                            creatgroupres.iv = iv;

                            Intent intent = new Intent("EventCreateGroupRes");
                            intent.putExtra("key", creatgroupres);
                            sendBroadcast(intent);

                        } catch (JSONException e) {
                            return;
                        }

                    } catch (final Exception ex) {
                        Log.i("---", "Exception in thread");
                    }
        }
    };

    private Emitter.Listener onLeaveGroupRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String groupid;
                String answer;
                try {
                    LeaveGroupResponseData leavegroupres = new LeaveGroupResponseData();
                    groupid = data.getString("groupid");
                    answer = data.getString("answer");
                    leavegroupres.groupid = groupid;
                    leavegroupres.answer = answer;

                    Intent intent = new Intent("EventLeaveGroupRes");
                    intent.putExtra("key", leavegroupres);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onAddGroupUserRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String targetuser;
                String groupid;
                String answer;
                try {
                    AddGroupUserResponseData addgroupuser = new AddGroupUserResponseData();
                    targetuser = data.getString("targetuser");
                    groupid = data.getString("groupid");
                    answer = data.getString("answer");
                    addgroupuser.targetuser = targetuser;
                    addgroupuser.groupid = groupid;
                    addgroupuser.answer = answer;

                    Intent intent = new Intent("EventAddGroupUserRes");
                    intent.putExtra("key", addgroupuser);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onRemoveGroupUserRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String targetuser;
                String groupid;
                String answer;
                try {
                    RemoveGroupUserResponseData removegroupuser = new RemoveGroupUserResponseData();
                    targetuser = data.getString("targetuser");
                    groupid = data.getString("groupid");
                    answer = data.getString("answer");
                    removegroupuser.targetuser = targetuser;
                    removegroupuser.groupid = groupid;
                    removegroupuser.answer = answer;

                    Intent intent = new Intent("EventRemoveGroupUserRes");
                    intent.putExtra("key", removegroupuser);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onUpdateGroupNameRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String groupid;
                String answer;
                try {
                    UpdateGroupNameResponseData updategroupname = new UpdateGroupNameResponseData();
                    groupid = data.getString("groupid");
                    answer = data.getString("answer");
                    updategroupname.groupid = groupid;
                    updategroupname.answer = answer;

                    Intent intent = new Intent("EventGroupNameRes");
                    intent.putExtra("key", updategroupname);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onUpdateGroupImageRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String groupid;
                //String utype;
                String answer;
                try {
                    UpdateGroupImageResponseData updategroupimage = new UpdateGroupImageResponseData();
                    groupid = data.getString("groupid");
                    //utype = data.getString("utype");
                    answer = data.getString("answer");
                    updategroupimage.groupid = groupid;
                    //updategroupimage.utype = utype;
                    updategroupimage.answer = answer;

                    Intent intent = new Intent("EventGroupImageRes");
                    intent.putExtra("key", updategroupimage);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onUserAdded = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            /*
            try {
                JSONObject data = (JSONObject) args[0];
                String username;
                String groupid;
                String by;
                String timestamp;
                try {
                    username = data.getString("username");
                    groupid = data.getString("groupid");
                    by = data.getString("by");
                    timestamp = data.getString("timestamp");
                } catch (JSONException e) {
                    return;
                }

                if(by.equals(myuser)==false && username.equals(myuser)){
                    JSONObject sendGroupInfoReq = new JSONObject();
                    try {
                        sendGroupInfoReq.put("groupid", groupid);
                        mSocket.emit("group-info-req", sendGroupInfoReq);
                    } catch (JSONException c) {
                    }
                }


            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
            */
            try {
                JSONObject data = (JSONObject) args[0];
                String username;
                String groupid;
                String by;
                String timestamp;
                try {
                    UserAddedData useradded = new UserAddedData();
                    username = data.getString("username");
                    groupid = data.getString("groupid");
                    by = data.getString("by");
                    timestamp = data.getString("timestamp");
                    useradded.username = username;
                    useradded.groupid = groupid;
                    useradded.by = by;
                    useradded.timestamp = timestamp;

                    Intent intent = new Intent("EventUserAdded");
                    intent.putExtra("key", useradded);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onUserRemoved = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String username;
                String groupid;
                String by;
                String timestamp;
                try {
                    UserRemovedData userremoved = new UserRemovedData();
                    username = data.getString("username");
                    groupid = data.getString("groupid");
                    by = data.getString("by");
                    timestamp = data.getString("timestamp");
                    userremoved.username = username;
                    userremoved.groupid = groupid;
                    userremoved.by = by;
                    userremoved.timestamp = timestamp;

                    Intent intent = new Intent("EventUserRemoved");
                    intent.putExtra("key", userremoved);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String username;
                String groupid;
                String timestamp;
                try {
                    UserLeftData userleft = new UserLeftData();
                    username = data.getString("username");
                    groupid = data.getString("groupid");
                    timestamp = data.getString("timestamp");
                    userleft.username = username;
                    userleft.groupid = groupid;
                    userleft.timestamp = timestamp;

                    Intent intent = new Intent("EventUserLeft");
                    intent.putExtra("key", userleft);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onGroupInfoUpdated = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String username;
                String groupid;
                String timestamp;
                String utype;
                String newvalue;
                try {
                    GroupInfoUpdatedData groupinfoupdated = new GroupInfoUpdatedData();
                    username = data.getString("username");
                    groupid = data.getString("groupid");
                    timestamp = data.getString("timestamp");
                    utype = data.getString("utype");
                    newvalue = data.getString("newvalue");
                    groupinfoupdated.username = username;
                    groupinfoupdated.groupid = groupid;
                    groupinfoupdated.timestamp = timestamp;
                    groupinfoupdated.utype = utype;
                    groupinfoupdated.newvalue = newvalue;

                    Intent intent = new Intent("EventGroupInfoUpdated");
                    intent.putExtra("key", groupinfoupdated);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onGroupActivitiesRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            netTasksGroupActivitiesRes.clear();
            GroupActivitiesResponseData groupactivities = new GroupActivitiesResponseData();
            String jsonStr = args[0].toString();
            JSONArray activities = null;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    activities = jsonObj.getJSONArray("activities");
                    for (int i = 0; i < activities.length(); i++) {
                        JSONObject c = activities.getJSONObject(i);
                        String groupid = c.getString("groupid");
                        String atype = c.getString("atype");
                        String actoruser = c.getString("actoruser");
                        String affecteduser = c.getString("affecteduser");
                        String timestamp = c.getString("timestamp");

                        StructGroupActivitiesResponse task = new StructGroupActivitiesResponse();
                        task.groupid = groupid;
                        task.atype =atype;
                        task.actoruser =actoruser;
                        task.affecteduser =affecteduser;
                        task.timestamp =timestamp;
                        netTasksGroupActivitiesRes.add(task);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                groupactivities.activities = netTasksGroupActivitiesRes;
                Intent intent = new Intent("EventGroupActivitiesRes");
                intent.putExtra("key", groupactivities);
                sendBroadcast(intent);

            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
        }
    };

    private Emitter.Listener onYouAdded = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String groupid;
                String by;
                String timestamp;
                String key;
                String iv;
                try {
                    YouAddedData youadded = new YouAddedData();
                    groupid = data.getString("groupid");
                    by = data.getString("by");
                    timestamp = data.getString("timestamp");
                    key = data.getString("key");
                    iv = data.getString("iv");
                    youadded.groupid = groupid;
                    youadded.by = by;
                    youadded.timestamp = timestamp;
                    youadded.key = key;
                    youadded.iv = iv;

                    Intent intent = new Intent("EventYouAdded");
                    intent.putExtra("key", youadded);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onYouRemoved = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String groupid;
                String by;
                String timestamp;
                try {
                    YouRemovedData youremoved = new YouRemovedData();
                    groupid = data.getString("groupid");
                    by = data.getString("by");
                    timestamp = data.getString("timestamp");
                    youremoved.groupid = groupid;
                    youremoved.by = by;
                    youremoved.timestamp = timestamp;

                    Intent intent = new Intent("EventYouRemoved");
                    intent.putExtra("key", youremoved);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onTimeSyncRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String offset;
                try {
                    TimeSyncResponseData timesyncres = new TimeSyncResponseData();
                    offset = data.getString("offset");
                    timesyncres.offset = offset;
                    LoginProcess process = new LoginProcess();
                    process.OnSyncFinish(offset);
                   /* MessengerBus msg = new MessengerBus("syncfin",offset);
                    EventBus bus = EventBus.getDefault();
                    bus.post(msg);
*/
                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onServerPkRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String answer;
                String pk;
                String expiration;
                try {
                    ServerPkResponseData serverpkres = new ServerPkResponseData();
                    answer = data.getString("answer");
                    pk = data.getString("pk");
                    expiration = data.getString("expiration");
                    serverpkres.answer = answer;
                    serverpkres.pk = pk;
                    serverpkres.expiration = expiration;

                    Intent intent = new Intent("EventServerPkRes");
                    intent.putExtra("key", serverpkres);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onGroupsSessionKeyRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            netTasksGroupsSessionKeyRes.clear();
            GroupsSessionKeyResponseData groupsessionkey = new GroupsSessionKeyResponseData();
            String jsonStr = args[0].toString();
            JSONArray answers = null;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    answers = jsonObj.getJSONArray("keys");
                    for (int i = 0; i < answers.length(); i++) {
                        JSONObject c = answers.getJSONObject(i);
                        String groupid = c.getString("groupid");
                        String key = c.getString("key");
                        String iv = c.getString("iv");

                        StructGroupsSessionKeyResponse task = new StructGroupsSessionKeyResponse();
                        task.groupid = groupid;
                        task.key =key;
                        task.iv =iv;
                        netTasksGroupsSessionKeyRes.add(task);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                groupsessionkey.keys = netTasksGroupsSessionKeyRes;
                Intent intent = new Intent("EventGroupsSessionKeyRes");
                intent.putExtra("key", groupsessionkey);
                sendBroadcast(intent);

            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
        }
    };

    private Emitter.Listener onPrivateMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {



            JSONObject jsonobj = (JSONObject) args[0];

            HistoryResponseData data = new HistoryResponseData();
            DatabaseHandler db = new DatabaseHandler(AppController.getCurrentContext());
            try {
                data.content = jsonobj.getString("content");
                data.mid = jsonobj.getString("mid");
                data.content = jsonobj.getString("content");
                data.timestamp = jsonobj.getString("timestamp");
                data.to = jsonobj.getString("to");
                data.replyto = jsonobj.getString("replyto");
                data.forwarded = jsonobj.getString("forwarded");
                data.from = jsonobj.getString("from");
                String size = jsonobj.getString("size");
                String fileName = jsonobj.getString("orgFileName");


                int sizeint = 0;
                try{
                    sizeint=Integer.parseInt(size);
                }catch (NumberFormatException ex){
                    ex.printStackTrace();
                }

                data.mtype = jsonobj.getString("mtype");
                if(data.mtype.equals("text")){
                    data.lastevent = "delivered";

                }
                else {
                    data.lastevent="delivered";
                }




                PrivateMessageDataBase pm = new PrivateMessageDataBase(data.mid,data.mtype,data.from,data.to,Long.parseLong(data.timestamp),data.content,size,data.lastevent,data.replyto,data.forwarded,100,((sizeint/100000)+1));
                pm.setFileName(fileName);
                if(!db.isExist(data.mid)){
                    db.InsertState(pm,AppController.getCurrentContext());
                }

                JSONObject packet = new JSONObject();
                JSONObject data2  = new JSONObject();
                data2.put("mid",data.getMid());
                data2.put("from",data.getTo());
                data2.put("to",data.getFrom());
                packet.put("data",data2);
                packet.put("tc",System.currentTimeMillis()/1000);
                packet.put("signature", PreferencesData.getString(AppController.getCurrentContext(),"signature"));

                SaffronEmitter emitter =new SaffronEmitter();
                emitter.EmitReceived(data2);


                ConversationResponseData crd= db.getConversation(data.getFrom());
                if(crd==null){
                    PreferencesData.saveBoolean(AppController.getCurrentContext(),"IsNeedToUpdate",true);
                }else {
                    int unSeen= Integer.parseInt(crd.getUnseen());
                    db.updateConversation(crd.getTarget(),data.getContent(),unSeen+1);
                }
                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("privateupdated",data.toString(),"pmessagerec");
                eventBus.post(message);





            }catch (JSONException ex){
                ex.printStackTrace();
            }








        }
    };

    private Emitter.Listener onGroupMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            EventBus eventBus = EventBus.getDefault();
            MessengerBus message = new MessengerBus("getmessagegroup",1,null);
            eventBus.post(message);
            /*try {
                JSONObject data = (JSONObject) args[0];
                String mid;
                String mtype;
                String from;
                String groupid;
                String size;
                String timestamp;
                String content;
                try {
                    GroupMessageData groupmessage = new GroupMessageData();
                    mid = data.getString("mid");
                    mtype = data.getString("mtype");
                    from = data.getString("from");
                    groupid = data.getString("groupid");
                    size = data.getString("size");
                    timestamp = data.getString("timestamp");
                    content = data.getString("content");
                    groupmessage.mid = mid;
                    groupmessage.mtype = mtype;
                    groupmessage.from = from;
                    groupmessage.groupid = groupid;
                    groupmessage.size = size;
                    groupmessage.timestamp = timestamp;
                    groupmessage.content = content;

                    Intent intent = new Intent("EventGroupMessage");
                    intent.putExtra("key", groupmessage);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }*/
        }
    };

    private Emitter.Listener onPrivateTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {




            JSONObject data = (JSONObject) args[0];
            UnPack.sendEvent(data,"onprivatetyping");
          /*  Security s = new Security( AppController.getCurrentContext());
            s.Unpack(data,"onprivatetyping");*/
           /* try {
                JSONObject data = (JSONObject) args[0];
                String from;
                String to;
                try {
                    PrivateTypingData privatetypingdata = new PrivateTypingData();
                    from = data.getString("from");
                    to = data.getString("to");
                    privatetypingdata.from = from;
                    privatetypingdata.to = to;

                    Intent intent = new Intent("EventPrivateTyping");
                    intent.putExtra("key", privatetypingdata);
                    sendBroadcast(intent);


                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }*/
        }
    };

    private Emitter.Listener onPrivateStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];
            UnPack.sendEvent(data,"onprivatetypingstop");
            /*Security s = new Security( AppController.getCurrentContext());
            s.Unpack(data,"onprivatetypingstop");*/

        }
    };

    private Emitter.Listener onGroupTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String from;
                String groupid;
                try {
                    GroupTypingData grouptypingdata = new GroupTypingData();
                    from = data.getString("from");
                    groupid = data.getString("groupid");
                    grouptypingdata.from = from;
                    grouptypingdata.groupid = groupid;

                    Intent intent = new Intent("EventGroupTyping");
                    intent.putExtra("key", grouptypingdata);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onGroupStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String from;
                String groupid;
                try {
                    GroupStopTypingData groupstoptypingdata = new GroupStopTypingData();
                    from = data.getString("from");
                    groupid = data.getString("groupid");
                    groupstoptypingdata.from = from;
                    groupstoptypingdata.groupid = groupid;

                    Intent intent = new Intent("EventGroupStopTyping");
                    intent.putExtra("key", groupstoptypingdata);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onSent = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            Log.e("events","sent recieved");
            try {
                JSONObject data = (JSONObject) args[0];
                try {
                    String mid = data.getString("mid");
                    DatabaseHandler dbhandler = new DatabaseHandler(AppController.getCurrentContext());

                    PrivateMessageDataBase pmd =  dbhandler.GetSpecialPMessage(mid);
                    dbhandler.updatePercent(mid,100);
                    dbhandler.updateState(mid,"sent");
                    EventBus eventBus = EventBus.getDefault();
                    MessengerBus message = new MessengerBus("privateupdate",mid);
                    eventBus.post(message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
              /*  Security s = new Security( AppController.getCurrentContext());
                s.Unpack(data,"onsent");*/
            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }


        }
    };


    private Emitter.Listener onSeen = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.e("events","seen recieved");
            JSONObject data = (JSONObject) args[0];
            try {
                String from = data.getString("from");
                String to = data.getString("to");
                DatabaseHandler dbhandler = new DatabaseHandler(AppController.getCurrentContext());
                ArrayList<PrivateMessageDataBase> unseens = dbhandler.AlllPrivateUnseen(from,to);
                ArrayList<String>mids = new ArrayList<>();
                for(int i = 0  ; i<unseens.size();i++){
                    mids.add(unseens.get(i).getMid());
                }

                dbhandler.updateSeen(from,to);

                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("privateupdate","seen",mids);
                eventBus.post(message);


            } catch (JSONException e) {
                e.printStackTrace();
            }
         /*   Security s = new Security( AppController.getCurrentContext());
            s.Unpack(data,"onseen");*/
        }
    };

    private Emitter.Listener onBinaryMessageRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];

            EventBus eventBus = EventBus.getDefault();
            MessengerBus message = new MessengerBus("unpackfinished",data.toString(),"binaryresp");
            eventBus.post(message);
            try{
                String mid = data.getString("mid");
                DatabaseHandler db = new DatabaseHandler(AppController.getCurrentContext());
                PrivateMessageDataBase pm = db.GetSpecialPMessage(mid);
                SenderFiles sf = new SenderFiles();
                sf.SendFirst(data.toString(),pm.getMtype());
            }catch (JSONException e){
                e.printStackTrace();
            }
           /* Security s = new Security( AppController.getCurrentContext());
            s.Unpack(data,"binaryresp");
            Log.e("binary","binsty trdp");*/

        }
    };

    private Emitter.Listener onDataChunkAck = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            JSONObject data = (JSONObject) args[0];

            try {
                Log.e("myapp","binaryacc unpack");

                DataChunkData dataChunkData = new DataChunkData();
                dataChunkData.mid = data.getString("mid");
                Log.e("myapp","binaryacc mid="+dataChunkData.mid);
                DatabaseHandler db = new DatabaseHandler(AppController.getCurrentContext());
                PrivateMessageDataBase pmd = db.GetSpecialPMessage(dataChunkData.mid);
                CounterDb counterdb = db.GetSpecialCounter(dataChunkData.mid);
                int counter=0;
                if(counterdb==null){
                    counterdb = new CounterDb(dataChunkData.mid,1);
                    db.InsertCounter(counterdb);
                    counter=1;
                }else {
                    counter=counterdb.getCounter();
                    counter++;
                    db.updatecount(dataChunkData.mid,counter);
                }

                SenderFiles sf = new SenderFiles();
                sf.SendNThPart(data.toString(),counter,pmd.getMtype());
            /* *//**//*   EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("unpackfinished",data,sender);
                eventBus.post(message);*//**//**/
            }catch (JSONException e){
                e.printStackTrace();
            }

     /*       Security s = new Security( AppController.getCurrentContext());*/
           /* s.Unpack(data,"binaryacc");
            Log.d("binary","binsty acc");
            Log.e("myapp","binaryacc listener ");*/
        }
    };

    private Emitter.Listener onBinaryMessageReceived = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                Log.d("binary","binary recieved");
                JSONObject data = (JSONObject) args[0];
                String mid;
                String uri;
                try {
                    BinaryMessageReceivedData binarymessagereceived = new BinaryMessageReceivedData();
                    mid = data.getString("mid");
                    uri = data.getString("uri");

                    binarymessagereceived.mid = mid;
                    binarymessagereceived.uri = uri;

                    Intent intent = new Intent("EventBinaryMessageReceived");
                    intent.putExtra("key", binarymessagereceived);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onPrivateSendingData = new Emitter.Listener() {


        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];
            UnPack.sendEvent(data,"onprivatesending");
        /*    Security s = new Security( AppController.getCurrentContext());
            s.Unpack(data,"onprivatesending");*/
        }
    };

    private Emitter.Listener onPrivateStopSendingData = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];
            UnPack.sendEvent(data,"stopPrivateSendingData");
           /* Security s = new Security( AppController.getCurrentContext());
            s.Unpack(data,"stopPrivateSendingData");*/

        }
    };

    private Emitter.Listener onGroupSendingData = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String from;
                String groupid;
                try {
                    GroupSendingData groupsendingdata = new GroupSendingData();
                    from = data.getString("from");
                    groupid = data.getString("groupid");

                    groupsendingdata.from = from;
                    groupsendingdata.groupid = groupid;

                    Intent intent = new Intent("EventGroupSendingData");
                    intent.putExtra("key", groupsendingdata);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };

    private Emitter.Listener onGroupStopSendingData = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                String from;
                String groupid;
                try {
                    GroupStopSendingData groupstopsendingdata = new GroupStopSendingData();
                    from = data.getString("from");
                    groupid = data.getString("groupid");

                    groupstopsendingdata.from = from;
                    groupstopsendingdata.groupid = groupid;

                    Intent intent = new Intent("EventGroupStopSendingData");
                    intent.putExtra("key", groupstopsendingdata);
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    return;
                }

            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }
        }
    };


    private Emitter.Listener onDelivered = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            Log.d("events","delivered recieved");
                JSONObject data = (JSONObject) args[0];
            try {
                JSONObject jsonobj = new JSONObject(data.toString());
                String mid = jsonobj.getString("mid");
                DatabaseHandler dbhandler = new DatabaseHandler(AppController.getCurrentContext());
                dbhandler.updateState(mid,"delivered");
                PrivateMessageDataBase pmd =  dbhandler.GetSpecialPMessage(mid);

                dbhandler.updatePercent(mid,100);


                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("privateupdate",mid);
                eventBus.post(message);

            } catch (JSONException e) {
                e.printStackTrace();
            }
              /*  Security s = new Security( AppController.getCurrentContext());
                s.Unpack(data,"ondelivered");
*/


        }
    };

    private Emitter.Listener onUpdate = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            netTasksUpdateRes.clear();
            UpdateResponseData updateresponse = new UpdateResponseData();
            String jsonStr = args[0].toString();
            JSONArray answers = null;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    answers = jsonObj.getJSONArray("messages");
                    for (int i = 0; i < answers.length(); i++) {
                        JSONObject c = answers.getJSONObject(i);
                        String mid = c.getString("mid");
                        String type = c.getString("type");
                        String mtype = c.getString("mtype");
                        String from = c.getString("from");
                        String to = c.getString("to");
                        String size = c.getString("size");
                        String timestamp = c.getString("timestamp");
                        String content = c.getString("content");
                        String replyto = c.getString("replyto");
                        String forwarded = c.getString("forwarded");

                        StructUpdateResponse task = new StructUpdateResponse();
                        task.mid = mid;
                        task.type =type;
                        task.mtype =mtype;
                        task.from =from;
                        task.to =to;
                        task.size =size;
                        task.timestamp =timestamp;
                        task.content =content;
                        task.replyto =replyto;
                        task.forwarded =forwarded;
                        netTasksUpdateRes.add(task);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                updateresponse.messages = netTasksUpdateRes;
                Intent intent = new Intent("EventUpdateRes");
                intent.putExtra("key", updateresponse);
                sendBroadcast(intent);

            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
        }
    };

    private Emitter.Listener onEventsRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            netTasksEventsRes.clear();
            EventsResponseData eventsresponse = new EventsResponseData();
            String jsonStr = args[0].toString();
            JSONArray answers = null;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    answers = jsonObj.getJSONArray("events");
                    for (int i = 0; i < answers.length(); i++) {
                        JSONObject c = answers.getJSONObject(i);
                        String mid = c.getString("mid");
                        String type = c.getString("type");
                        String etype = c.getString("etype");
                        String from = c.getString("from");

                        StructEventsResponse task = new StructEventsResponse();
                        task.mid = mid;
                        task.type =type;
                        task.etype =etype;
                        task.from =from;
                        netTasksEventsRes.add(task);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                eventsresponse.events = netTasksEventsRes;
                Intent intent = new Intent("EventEventsRes");
                intent.putExtra("key", eventsresponse);
                sendBroadcast(intent);

            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
        }
    };

    private Emitter.Listener onDeleteMessagesRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            try {
                JSONObject data = (JSONObject) args[0];


                try {


                    JSONArray answers = null;
                    DatabaseHandler db = new DatabaseHandler(AppController.getCurrentContext());
                    if(data!=null){
                        answers=data.getJSONArray("messages");
                        for(int i = 0;i<answers.length();i++){
                            JSONObject c = answers.getJSONObject(i);
                            String mid = c.getString("mid");
                            db.deletePMessage(mid);
                        }


                    }

                }catch (JSONException ex){
                    ex.printStackTrace();
                }
            } catch (final Exception ex) {
                Log.i("---", "Exception in thread");
            }

        }
    };

    private Emitter.Listener onGroupInfoRes = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            netTasksGroupInfoRes.clear();
            GroupInfoResponseData groupinfores = new GroupInfoResponseData();

            try {
                String jsonStr = args[0].toString();
                JSONObject jObj = new JSONObject(jsonStr);
                String image = jObj.getString("image");
                String groupid = jObj.getString("groupid");
                String name = jObj.getString("name");
                groupinfores.groupid = groupid;
                groupinfores.name = name;
                groupinfores.image = image;


                String MEMBERS = jObj.getJSONArray("members").toString();

                JSONArray json = new JSONArray(MEMBERS);
                for(int i=0;i<json.length();i++){
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject e = json.getJSONObject(i);
                    StructGroupInfoResponse task = new StructGroupInfoResponse();
                    task.lastOnline = e.getString("lastOnline");
                    task.username = e.getString("username");
                    netTasksGroupInfoRes.add(task);
                }


                groupinfores.members = netTasksGroupInfoRes;
                Intent intent = new Intent("EventGroupInfoRes");
                intent.putExtra("key", groupinfores);
                sendBroadcast(intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }


            /*
            String jsonStr = args[0].toString();
            try {
                JSONObject jObj = new JSONObject(jsonStr);
                String image = jObj.getString("image");
                String groupid = jObj.getString("groupid");
                String name = jObj.getString("name");
                groupinfores.groupid = groupid;
                groupinfores.name = name;
                groupinfores.image = image;

                JSONArray jArr = jObj.getJSONArray("members");
                for (int i=0; i < jArr.length(); i++) {
                    JSONObject obj = jArr.getJSONObject(i);
                    String lastOnline = jObj.getString("lastOnline");
                    String username = jObj.getString("username");

                    StructGroupInfoResponse task = new StructGroupInfoResponse();
                    task.lastOnline = lastOnline;
                    task.username = username;
                    netTasksGroupInfoRes.add(task);
                }

                groupinfores.members = netTasksGroupInfoRes;
                Intent intent = new Intent("EventGroupInfoRes");
                intent.putExtra("key", groupinfores);
                sendBroadcast(intent);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            */

        }
    };


    private void showNotification(String username, String msg)
    {
        msg = msg.replace(InfoOfServer.SOCKETIO_LINK_UPLOAD,"");
        String title = "Saffron: new Message from (" + username + ")";
        String text = username + ": " +
                ((msg.length() < 5) ? msg : msg.substring(0, 5) + "...");
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_logo)
                .setContentTitle(title)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentText(text);
        mBuilder.setAutoCancel(true);
        mBuilder.setContentText(username + ": " + msg);
        mNM.notify((username + msg).hashCode(), mBuilder.build());
    }


}
