package com.sample.saffron.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.CircleProgress;
import com.sample.saffron.R;
import com.sample.saffron.client.LoginProcess;
import com.sample.saffron.entity.securityData.TimeSyncRequestData;
import com.sample.saffron.entity.userData.AccessRequestData;
import com.sample.saffron.entity.userData.AccessResponseData;
import com.sample.saffron.entity.userData.SmsResponseData;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.sender.senderPackets;

import java.io.InputStream;

import de.greenrobot.event.EventBus;

/**
 * Created by alireza on 3/13/2017.
 */

public class Access_Req_Activity extends Activity implements View.OnClickListener {
    EventBus bus = EventBus.getDefault();
    Context context;
    info.hoang8f.widget.FButton btnAccReq;
    EditText edtPhone;
    CircleProgress progress_whee;
    TextView txtError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_req);
        AppController.setCurrentContext(this);
        context = this;
        DefineView();
    }

    private void DefineView() {
        btnAccReq = (info.hoang8f.widget.FButton) findViewById(R.id.access_req_btn);
        edtPhone = (EditText) findViewById(R.id.access_req_phone);
        txtError = (TextView) findViewById(R.id.errorTextView);
        btnAccReq.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        access();

    }

    private void access() {
        String a = edtPhone.getText().toString();

        if (a.equals("")) {
            Toast.makeText(this, "please fill phone fild", Toast.LENGTH_LONG).show();
        } else {


            PreferencesData.saveBoolean(context,"LoginFromUser",true);
            PreferencesData.saveString(context,"phone",edtPhone.getText().toString());
            LoginProcess process = new LoginProcess();
            process.Sync();
        }
    }


  /*  private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("EventAccessRes")) {
                AccessResponseData signInResponseData = (AccessResponseData) intent.getExtras().getSerializable("key");
                //    progressDialog.dismiss();

            } else if (intent.getAction().equals("EventSmsRes")) {
                SmsResponseData smsResponseData = (SmsResponseData) intent.getExtras().getSerializable("key");
                Log.d(Constants.LOGTAG, "SMS Response From server : " + smsResponseData.challenge);
                String challenge = smsResponseData.challenge;
                Intent intent1 = new Intent(context, Challenge_Req_Activity.class);
                intent1.putExtra("challenge", challenge);
                intent1.putExtra("phone", edtPhone.getText().toString());
                startActivity(intent1);
                finish();


            }
        }
    }

*/

    @Override
    protected void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Override
    protected void onStart() {
        bus.register(this);

        super.onStart();
    }



    public void onEvent(MessengerBus event) {
        if(event.getMessage().equals("requestPass")){
            Intent intent = new Intent(context,Access_Req_Pass.class);
            intent.putExtra("data",event.getMes());
            startActivity(intent);
            finish();
        }else if(event.getMessage().equals("challengeNotOk")){
            Intent intent = new Intent(context,Access_Req_Pass.class);
            intent.putExtra("data","");
            startActivity(intent);
            finish();
        }
    }



}
