package com.sample.saffron.activities.messageData;

import com.sample.saffron.interfacer.EventData;

public class BinaryMessageRequestData implements EventData {
    public String username;
    public String mid;
    public String type; // type = private || group
    public String filename;
    public String btype; // btype = file || photo || voice || video
    public long size;
    public String from;
    public String to;
    public String timestamp;
    public String replyto;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
