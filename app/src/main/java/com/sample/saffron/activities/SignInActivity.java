package com.sample.saffron.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sample.saffron.R;
import com.sample.saffron.client.Security;
import com.sample.saffron.entity.userData.ProfileInfoRequestData;
import com.sample.saffron.entity.userData.SignInResponseData;
import com.sample.saffron.helper.JsonToAns;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.sender.senderPackets;
import com.victor.loading.newton.NewtonCradleLoading;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

public class SignInActivity extends AppCompatActivity {


    TextView userNameEditText;
    Context ctx;
    String userName;
    String salt;
    String challenge;
    JSONObject data = new JSONObject();
    EventBus bus = EventBus.getDefault();
    private NewtonCradleLoading newtonCradleLoading;
     info.hoang8f.widget.FButton   signInButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Bundle bundle = getIntent().getExtras();
       AppController.setActivityContext(this,this);
        if(bundle!=null){
            userName=bundle.getString("username");
            salt  =bundle.getString("salt");
            challenge=bundle.getString("challenge");
        }
        ctx = this;

        makePage();
    }

    private void makePage() {
        userNameEditText = (TextView) findViewById(R.id.sign_in_activity_username);
        userNameEditText.setText("hi dear " +userName+"!"+"\n please sign in for last step.");
        newtonCradleLoading = (NewtonCradleLoading) findViewById(R.id.newton_cradle_loading);

         signInButton=(info.hoang8f.widget.FButton) findViewById(R.id.sign_in_activity_button_signUp);;
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              SignIn();

                //        saffronEmitter.EmitSignInRequest(signInRequestData);

            }
        });
    }

    public void SignIn(){
        newtonCradleLoading.setVisibility(View.VISIBLE);
        newtonCradleLoading.start();
        senderPackets sender = new senderPackets(ctx);
        data  = sender.PackSignInStepOne(userName);
     /*   Security s = new Security(SignInActivity.this);
        s.Sign(data);*/
        signInButton.setEnabled(false);
    }
    @Override
    protected void onStart() {
        bus.register(this);
        SignIn();
        super.onStart();
    }

    @Override
    protected void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }



    public void onEvent(MessengerBus event) {

        if (event.getMessage().equals("signfinished")) {

           senderPackets sp = new senderPackets(ctx);
            sp.PackSingInStepTwo(event.getMes(),challenge,salt,userName);
        }else if(event.getMessage().equals("packsignFinished") ){
         /*   JSArray array = (JSArray) event.getObj();

            senderPackets sp = new senderPackets(ctx);
            sp.SendPacketSignIn(array);*/
        }
        else if(event.getMessage().equals("signinfinish")){
            try {


                newtonCradleLoading.setVisibility(View.GONE);
                newtonCradleLoading.stop();
                signInButton.setEnabled(true);
                JSONObject jObject = new JSONObject(event.getMes());
                JSONObject data = jObject.getJSONObject("data");
                SignInResponseData responseData = JsonToAns.getSignInAsn(data);
                if(responseData.getAnswer().equals("OK")){
                    PreferencesData.saveString(ctx,"userName",userName);
                    PreferencesData.saveString(SignInActivity.this,"sk",responseData.getSk());
                    PreferencesData.saveString(SignInActivity.this,"iv",responseData.getIv());
                    PreferencesData.saveBoolean(ctx,"logedin",true);
                    Intent intent = new Intent(SignInActivity.this,MyMainActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(ctx,"oops ! something is wrong",Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
