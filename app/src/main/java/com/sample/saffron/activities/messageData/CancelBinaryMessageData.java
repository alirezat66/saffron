package com.sample.saffron.activities.messageData;

import org.json.JSONException;
import org.json.JSONObject;

import com.sample.saffron.interfacer.EventData;

public class CancelBinaryMessageData implements EventData {
    public String mid;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        JSONObject json = new JSONObject();
        try {
            json.put("mid", mid);
        } catch (JSONException e) {
        }
        result = json.toString();
        return result;
    }
}
