package com.sample.saffron.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.CircleProgress;
import com.sample.saffron.R;
import com.sample.saffron.client.LoginProcess;
import com.sample.saffron.helper.MessengerBus;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by alireza on 3/13/2017.
 */

public class Access_Req_Pass extends Activity implements View.OnClickListener {
    Context context;
    info.hoang8f.widget.FButton btnAccReq;
    EditText edtPhone;
    CircleProgress progress_whee;
    TextView txtError;
    String postData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_req);
        AppController.setCurrentContext(this);
        context = this;
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            postData = bundle.getString("data");
        }
        DefineView();
    }

    private void DefineView() {
        btnAccReq = (info.hoang8f.widget.FButton) findViewById(R.id.access_req_btn);
        edtPhone = (EditText) findViewById(R.id.access_pass);
        txtError = (TextView) findViewById(R.id.errorTextView);
        btnAccReq.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        access();

    }

    private void access() {
        String a = edtPhone.getText().toString();

        if (a.equals("")) {
            Toast.makeText(this, "please fill pass fild", Toast.LENGTH_LONG).show();
        } else {
            JSONObject data = null;
            try {
                data = new JSONObject(postData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            PreferencesData.saveString(context,"password",edtPhone.getText().toString());
            if(!postData.equals("")) {
                SaffronEmitter emitter = new SaffronEmitter();
                emitter.EmitSignInRequest(data);
            }else {
                LoginProcess process = new LoginProcess();
                process.Sync();
            }

        }
    }


  /*  private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("EventAccessRes")) {
                AccessResponseData signInResponseData = (AccessResponseData) intent.getExtras().getSerializable("key");
                //    progressDialog.dismiss();

            } else if (intent.getAction().equals("EventSmsRes")) {
                SmsResponseData smsResponseData = (SmsResponseData) intent.getExtras().getSerializable("key");
                Log.d(Constants.LOGTAG, "SMS Response From server : " + smsResponseData.challenge);
                String challenge = smsResponseData.challenge;
                Intent intent1 = new Intent(context, Challenge_Req_Activity.class);
                intent1.putExtra("challenge", challenge);
                intent1.putExtra("phone", edtPhone.getText().toString());
                startActivity(intent1);
                finish();


            }
        }
    }

*/
    @Override

    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();


      /*  IntentFilter intentFilter2 = new IntentFilter("EventAccessRes");
        registerReceiver(dataUpdateReceiver, intentFilter2);*/

    }





}
