package com.sample.saffron.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.test.mock.MockPackageManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.github.nkzawa.emitter.Emitter;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.github.rubensousa.bottomsheetbuilder.adapter.BottomSheetItemClickListener;
import com.sample.saffron.Database.DatabaseHandler;
import com.sample.saffron.Database.PrivateMessageDataBase;
import com.sample.saffron.R;
import com.sample.saffron.adapters.HistoryAdapter;
import com.sample.saffron.adapters.PrivateMessagesAdapter;
import com.sample.saffron.client.Security;
import com.sample.saffron.dialogs.ChoseDialog;
import com.sample.saffron.dialogs.ChoseForwardDialog;
import com.sample.saffron.dialogs.dialofListener;
import com.sample.saffron.entity.groupData.HistoryRequestData;
import com.sample.saffron.entity.groupData.HistoryResponseData;
import com.sample.saffron.entity.messageData.BinaryMessageRequestData;
import com.sample.saffron.entity.messageData.DataChunkData;
import com.sample.saffron.entity.messageData.PrivateMessageData;
import com.sample.saffron.helper.AppHelper;
import com.sample.saffron.helper.FileManager;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.helper.SenderFiles;
import com.sample.saffron.mySenderService;
import com.sample.saffron.typo.InfoOfServer;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.java_websocket.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.*;
import cn.gavinliu.android.lib.shapedimageview.ShapedImageView;
import de.greenrobot.event.EventBus;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

/**
 * Created by alireza on 3/30/2017.
 */

public class ActivityHistory extends AppCompatActivity implements  RecyclerView.OnItemTouchListener ,  ActionMode.Callback ,View.OnClickListener{
    private static final int REQUEST_CODE_PERMISSION = 2;
    private final int GALLERY_ACTIVITY_CODE = 100;
    private final  int GALLERY_VIDEO_ACTIVITY_CODE = 200;
    private final  int GALLERY_FILES_ACTIVITY_CODE = 300;
    private final  int GALLERY_MUSIC_ACTIVITY_CODE = 600;
    Menu myMenu;
    long delay = 1000; // 1 seconds after user stops typing
    long last_text_edit = 0;
    Handler handler = new Handler();
    private ActionMode actionMode;

    String userState= "";
    int lastvisible;
    LinearLayout root;
    SaffronEmitter emitter;
    private  long filesize;
    private  long allsize;
    public  int counter=0;
    public int chunkedsize;
    public  int pos=0;
    TextView txttyping;
    boolean Isupdate=false;
    byte[] bytesofimage=null;
    List<Integer>sizes=new ArrayList<>();
    List<String>mine=new ArrayList<>();
    List<File>files=new ArrayList<>();
    String from="";
    private GestureDetectorCompat gestureDetector;

    ChoseDialog dialogChose;
    ChoseForwardDialog forwardDialog;
    String[] mPermission = {
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };
    public static final String STATE_GRID = "state_grid";
    private BottomSheetMenuDialog mBottomSheetDialog;
    private BottomSheetBehavior mBehavior;

    FloatingActionButton fabdown;
    AppBarLayout appBarLayout;
    ProgressBar progressBar;
    Toolbar toolbar;
    RelativeLayout rl_reply;
    TextView    txtMainMessage;
    String replyMid="";
    TextView    txtMessreply;
    ImageView cancelReply;
    boolean isreply=false;
    private boolean mShowingGridDialog;

    ImageView btn_back;
    boolean issend=false;
    String target,jsonstring;
    TextView txt_target;
    RecyclerView list ;
    ImageView img_send;
    EmojiconEditText edt_message;
    String image_profile;

    String to;
    File myfile;
    Context context;
    ArrayList<PrivateMessageDataBase>pmList = new ArrayList<>();
    EventBus bus =EventBus.getDefault();
  //  PrivateMessagesAdapter adapter ;
    HistoryAdapter adapter;

    RelativeLayout sticker_parent;
    RelativeLayout dialg;

    EmojIconActions emojIcon;

    ImageView imgEmoji;

    ShapedImageView imageProfile;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        AppController.setActivityContext(this,this);
        emitter = new SaffronEmitter();
        Bundle bundle = getIntent().getExtras();
        context=this;
        PreferencesData.saveInt(context,"position",0);


        if(bundle!=null){
            target = bundle.getString("targetname");
            to = bundle.getString("to");
            from =PreferencesData.getString(context,"userName");
            image_profile = bundle.getString("image");
        }

        defineView();
        SendReqState();
        new MakePage().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new SendSeen().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void defineView() {
        progressBar= (ProgressBar) findViewById(R.id.progress_bar);
        toolbar = (Toolbar) findViewById(R.id.actionbarlayout);
        rl_reply = (RelativeLayout) findViewById(R.id.rl_reply);
        txtMainMessage = (TextView) findViewById(R.id.txtMainMessage);
        txtMessreply   = (TextView) findViewById(R.id.txtMessreply);
        cancelReply    = (ImageView) findViewById(R.id.cancelReply);
        imageProfile  = (ShapedImageView) findViewById(R.id.img_profile);
        if(image_profile.equals("")){
            Glide.with(context).load(R.drawable.default_user_64).fitCenter().into(imageProfile);
        }else {
            String s = InfoOfServer.MultiMediaIpServer+"/uploads/"+image_profile;

            Glide.with(context).load(s).centerCrop().error(R.drawable.default_user_64).into(imageProfile);
        }
        cancelReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isreply=false;
                rl_reply.setVisibility(View.GONE);
            }
        });
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        sticker_parent = (RelativeLayout) findViewById(R.id.sticker_parent);
        dialg=(RelativeLayout)findViewById(R.id.stiker);
        dialogChose = new ChoseDialog(context);
        fabdown = (FloatingActionButton) findViewById(R.id.fab);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        btn_back = (ImageView) findViewById(R.id.img_back);
        root = (LinearLayout) findViewById(R.id.root);

    /*    View view = snackbar.getView();
        LinearLayout.LayoutParams params =(LinearLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);*/

        txt_target = (TextView) findViewById(R.id.txt_target);
        txttyping= (TextView) findViewById(R.id.txt_type);
        txt_target.setText(target);
        fabdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabdown.setVisibility(View.GONE);




                DatabaseHandler db = new DatabaseHandler(context);

                pmList = db.AlllPrivate(from,to);



               /* adapter.clear();
                for(int i = 0;i<pmList.size();i++){
                    adapter.add(pmList.get(i));

                }*/

                setMyAdapter();
                GoToLastPosition();

                progressBar.setVisibility(View.GONE);

            }
        });


        img_send = (ImageView) findViewById(R.id.img_send);
        edt_message = (EmojiconEditText) findViewById(R.id.edit_message);
        imgEmoji = (ImageView) findViewById(R.id.img_sticker);

        imgEmoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                emojIcon.ShowEmojIcon();
                      }

        });
        emojIcon = new EmojIconActions(this, root, edt_message,imgEmoji);
        emojIcon.ShowEmojIcon();



        final Runnable input_finish_checker = new Runnable() {
            public void run() {
                if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
                    JSONObject packet = new JSONObject();
                    JSONObject data = new JSONObject();
                    try{

                        data.put("from", from);
                        data.put("to",to);


                        emitter.EmitPrivateStopTyping(data);
                     /*   packet.put("data", data);
                        packet.put("tc", System.currentTimeMillis() / 1000);
                        packet.put("signature", PreferencesData.getString(context, "signature"));

                        Security sec = new Security(context);
                        sec.Pack(packet, "stoptyping");*/

                    }
                    catch (JSONException ex){
                        ex.printStackTrace();
                    }

                }
            }
        };
        list = (RecyclerView) findViewById(R.id.list);
        list.setItemAnimator(new DefaultItemAnimator());
        list.getItemAnimator().setChangeDuration(0);

        list.setHasFixedSize(true);
        list.setItemViewCacheSize(10);
        list.setDrawingCacheEnabled(true);
        list.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        list.addOnItemTouchListener(this);
        gestureDetector = new GestureDetectorCompat(this, new RecyclerViewBenOnGestureListener());



        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition = (recyclerView == null || recyclerView.getChildCount() == 0) ?
                        0 : recyclerView.getChildAt(0).getTop();
                LinearLayoutManager linearLayoutManager1 = (LinearLayoutManager) recyclerView.getLayoutManager();
                int firstVisibleItem = linearLayoutManager1.findFirstVisibleItemPosition();
                lastvisible = linearLayoutManager1.findLastVisibleItemPosition();

                if(lastvisible>=adapter.getItemCount()-1){
                    fabdown.setVisibility(View.GONE);
                }
            }
        });
      edt_message.addTextChangedListener(new TextWatcher() {
                                  @Override
                                  public void beforeTextChanged (CharSequence s,int start, int count,
                                                                 int after){
                                  }
                                  @Override
                                  public void onTextChanged ( final CharSequence s, int start, int before,
                                                              int count){

                                      JSONObject packet = new JSONObject();
                                      JSONObject data = new JSONObject();
                                      try{

                                          data.put("from", from);
                                          data.put("to",to);
                                          emitter.EmitPrivateTyping(data);
                                        /*  packet.put("data", data);
                                          packet.put("tc", System.currentTimeMillis() / 1000);
                                          packet.put("signature", PreferencesData.getString(context, "signature"));

                                          Security sec = new Security(context);
                                          sec.Pack(packet, "typing");*/

                                      }
                                      catch (JSONException ex){
                                          ex.printStackTrace();
                                      }
                                      handler.removeCallbacks(input_finish_checker);
                                  }
                                  @Override
                                  public void afterTextChanged ( final Editable s){
                                      last_text_edit = System.currentTimeMillis();
                                      handler.postDelayed(input_finish_checker, delay);
                                      if(edt_message.getText().length()>0){

                                          img_send.setImageResource(R.drawable.ic_chat_send_active);
                                      }else {
                                          img_send.setImageResource(R.drawable.ic_attach_24dp);
                                      }

                                  }
                              }

        );
/*
        list.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if ( bottom < oldBottom) {
                    list.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int topRowVerticalPosition = (list == null || list.getChildCount() == 0) ?
                                    0 : list.getChildAt(0).getTop();
                            LinearLayoutManager linearLayoutManager1 = (LinearLayoutManager) list.getLayoutManager();
                            int firstVisibleItem = linearLayoutManager1.findFirstVisibleItemPosition();
                            lastvisible = linearLayoutManager1.findLastVisibleItemPosition();
                            list.smoothScrollToPosition(lastvisible+1);
                        }
                    }, 100);
                }
            }
        });*/


    /*    KeyboardVisibilityEvent.setEventListener(
                ActivityHistory.this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if(isOpen){



                            list.scrollToPosition(lastvisible);
                        }
                        // some code depending on keyboard visiblity status
                    }
                });*/



     /*   list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                pos = firstVisibleItem+visibleItemCount;


                if(firstVisibleItem+visibleItemCount>=adapter.getCount()){
                    fabdown.setVisibility(View.GONE);
                }
            }
        });*/
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                        if (edt_message.getText().toString().trim().length() > 0) {

                            JSONObject data = new JSONObject();
                            JSONObject packet = new JSONObject();


                            try {
                                data.put("mtype", "text");
                                data.put("from", PreferencesData.getString(context, "userName"));
                                data.put("to", to);

                                data.put("size", edt_message.getText().toString().length());
                                data.put("content", edt_message.getText().toString());
                                long timestamp = System.currentTimeMillis() / 1000;
                                data.put("timestamp", timestamp);

                                if(!isreply) {
                                    data.put("replyto", "");
                                }else {
                                    data.put("replyto",replyMid);
                                }
                                data.put("forwarded", replyMid);
                                String mid = Helper.sha256("text" + from + to + timestamp);

                                data.put("mid", mid);

                                packet.put("data", data);
                                packet.put("tc", System.currentTimeMillis() / 1000);
                                packet.put("signature", PreferencesData.getString(context, "signature"));

                                DatabaseHandler db = new DatabaseHandler(context);
                                PrivateMessageDataBase pm = new PrivateMessageDataBase(mid, "text", from, to, timestamp, edt_message.getText().toString()
                                        , edt_message.getText().toString().length() + "", "save", data.getString("replyto"), "",100,1);
                                pm.setFileName("");
                                if(!db.isExist(pm.getMid())) {
                                    db.InsertState(pm, context);
                                }
                                adapter.UpdateList(pm);
                                adapter.notifyItemInserted(adapter.getItemCount()-1);
                                GoToLastPosition();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            emitter.EmitPrivateMessage(data);
                           /* Security sec = new Security(context);
                            sec.Pack(packet, "privatetext");*/
                            replyMid="";
                        }else {

                            if(!dialogChose.isShowing()){




                                dialogChose.setListener(new dialofListener() {
                                    @Override
                                    public void accepted() {
                                        dialogChose.dismiss();

                                    }

                                    @Override
                                    public void rejected() {
                                        dialogChose.dismiss();

                                    }

                                    @Override
                                    public void accepted(int result) {
                                        PreferencesData.saveString(context,"replyMid",replyMid);

                                        if(result==1) {


                                            if (checkWriteExternalPermission() && checkReadExternalPermission()) {
                                                Intent gallery_Intent = new Intent(context, GalleryUtilVideo.class);

                                                startActivityForResult(gallery_Intent, GALLERY_VIDEO_ACTIVITY_CODE);

                                            } else {

                                                ActivityCompat.requestPermissions(ActivityHistory.this,
                                                        new String[]{mPermission[0], mPermission[1]}, REQUEST_CODE_PERMISSION);
                                            }
                                        }else if(result==2){
                                            if (checkWriteExternalPermission() && checkReadExternalPermission()) {
                                                Intent gallery_Intent = new Intent(context, GalleryMusicFilesUtil.class);
                                                startActivityForResult(gallery_Intent, GALLERY_MUSIC_ACTIVITY_CODE);

                                            } else {

                                                ActivityCompat.requestPermissions(ActivityHistory.this,
                                                        new String[]{mPermission[0], mPermission[1]}, REQUEST_CODE_PERMISSION);
                                            }

                                        }else if(result==3){
                                            if (checkWriteExternalPermission() && checkReadExternalPermission()) {
                                                Intent gallery_Intent = new Intent(context, GalleryUtil.class);
                                                startActivityForResult(gallery_Intent, GALLERY_ACTIVITY_CODE);

                                            } else {

                                                ActivityCompat.requestPermissions(ActivityHistory.this,
                                                        new String[]{mPermission[0], mPermission[1]}, REQUEST_CODE_PERMISSION);
                                            }
                                        }else if(result==4) {
                                            if (checkWriteExternalPermission() && checkReadExternalPermission()) {
                                                Intent gallery_Intent = new Intent(context, GalleryFilesUtil.class);
                                                startActivityForResult(gallery_Intent, GALLERY_FILES_ACTIVITY_CODE);

                                            } else {

                                                ActivityCompat.requestPermissions(ActivityHistory.this,
                                                        new String[]{mPermission[0], mPermission[1]}, REQUEST_CODE_PERMISSION);
                                            }
                                        }else {

                                            dialg.setVisibility(View.VISIBLE);
                                            sticker_parent.setVisibility(View.VISIBLE);
                                        }
                                        dialogChose.dismiss();
                                    }

                                    @Override
                                    public void accepted(String result) {
                                        dialogChose.dismiss();
                                    }

                                });
                                Window window = dialogChose.getWindow();
                                WindowManager.LayoutParams wlp = window.getAttributes();

                                wlp.gravity = Gravity.BOTTOM;
                                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                                window.setAttributes(wlp);
                                dialogChose.show();
                            }else {
                                dialogChose.dismiss();
                            }

                        }

                        edt_message.setText("");
                        isreply=false;
                        rl_reply.setVisibility(View.GONE);


                    }

        });

    }

    private void makeAction() {
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.txt_target);
        mTitleTextView.setText("My Own Title");

        ImageButton imageButton = (ImageButton) mCustomView
                .findViewById(R.id.img_back);
        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Refresh Clicked!",
                        Toast.LENGTH_LONG).show();
            }
        });

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    @Override
    public void onStart() {

        bus.register(this);
        PreferencesData.saveBoolean(context,"inhistory",true);

        super.onStart();
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String picturePath = "";
        if (requestCode == GALLERY_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                counter   = 0;
                replyMid=PreferencesData.getString(context,"replyMid","");

                picturePath = data.getStringExtra("picturePath");
                sendImage(picturePath,false,null,replyMid);
                rl_reply.setVisibility(View.GONE);
                isreply=false;
                replyMid="";
                PreferencesData.saveString(context,"replyMid","");

            }
        }else if(requestCode == GALLERY_VIDEO_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                counter=0;
                replyMid=PreferencesData.getString(context,"replyMid","");

                picturePath = data.getStringExtra("videoPath");
                sendVideo(picturePath,false,null,replyMid);
                rl_reply.setVisibility(View.GONE);
                isreply=false;
                replyMid="";
                PreferencesData.saveString(context,"replyMid","");

            }
        }else if(requestCode == GALLERY_FILES_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                picturePath = data.getStringExtra("filepath");
                if(picturePath!=null){
                    replyMid=PreferencesData.getString(context,"replyMid","");
                    sendFile(picturePath,false,null,replyMid);
                    rl_reply.setVisibility(View.GONE);
                    isreply=false;
                    replyMid="";

                    PreferencesData.saveString(context,"replyMid","");
                }else {
                    Toast.makeText(context, "this file cant send because of path address is not accesable", Toast.LENGTH_SHORT).show();
                }
            }
        }else if(requestCode == GALLERY_MUSIC_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                picturePath = data.getStringExtra("filepath");
                replyMid=PreferencesData.getString(context,"replyMid","");
                sendMusic(picturePath,false,null,replyMid);
                rl_reply.setVisibility(View.GONE);
                isreply=false;
                replyMid="";
                PreferencesData.saveString(context,"replyMid","");
            }
        }
    }

    public void sendImage(String picturePath,boolean isAgain,@Nullable PrivateMessageDataBase oldPm,String replyto){

        PreferencesData.saveBool(context,"fromOut",false);

        myfile = new File(Uri.parse(picturePath).getPath());
        SenderFiles sf = new SenderFiles();

        sf.MakeMessage(picturePath,to,oldPm,"photo",replyto);


    }
    public void sendFile(String picturePath,boolean isAgain,@Nullable PrivateMessageDataBase oldPm,String replyto){

        PreferencesData.saveBool(context,"fromOut",false);

        myfile = new File(Uri.parse(picturePath).getPath());
        SenderFiles sf = new SenderFiles();
        sf.MakeMessage(picturePath,to,oldPm,"file",replyto);


    }
  public void sendMusic(String picturePath,boolean isAgain,@Nullable PrivateMessageDataBase oldPm,String replyto){

        PreferencesData.saveBool(context,"fromOut",false);

        myfile = new File(Uri.parse(picturePath).getPath());
        SenderFiles sf = new SenderFiles();
        sf.MakeMessage(picturePath,to,oldPm,"voice",replyto);


    }
    @Override
    public void onClick(View view) {
        int position = list.getChildAdapterPosition(view);
        if (actionMode != null) {
            ToggleSelection(position);
        }
    }
    public  void sendVideo(String picturePath, boolean isAgain,@Nullable PrivateMessageDataBase oldpm,String replyto){


        PreferencesData.saveBool(context,"fromOut",false);

        myfile = new File(Uri.parse(picturePath).getPath());
        SenderFiles sf = new SenderFiles();
        sf.MakeMessage(picturePath,to,oldpm,"video",replyto);




/*

        //make message
        BinaryMessageRequestData message = new BinaryMessageRequestData();
        message.username = PreferencesData.getString(context, "username");


        message.from = PreferencesData.getString(context, "username");
        message.to = to;
        message.replyto = "";
        message.type = "private";
        message.btype = "video";
        myfile = new File(Uri.parse(picturePath).getPath());


        message.size = myfile.length();
        message.replyto = "";
        if(oldpm==null){
            message.timestamp = (System.currentTimeMillis()/1000)+ "";
            message.mid = Helper.sha256(message.btype + message.from + message.to + message.timestamp);
        }else {
            message.timestamp = oldpm.getTimestamp()+"";
            message.mid = oldpm.getMid();
        }

        String filename = picturePath.substring(picturePath.lastIndexOf("/") + 1);
        message.filename = filename;








        FileManager fileManager = new FileManager();
        String textFileName =fileManager.makeFile("video").toString() +"/"+message.getMid()+"."+picturePath.substring(picturePath.lastIndexOf(".")+1);
        File destiny = new File(textFileName);
        try {
            fileManager.copy(myfile,destiny);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject data2 = new JSONObject();
        JSONObject packet = new JSONObject();


        try {
            data2.put("from", message.getFrom());
            data2.put("to", to);

            data2.put("type", message.getType());
            data2.put("btype", message.getBtype());
            data2.put("timestamp", message.getTimestamp());
            data2.put("replyto", "");
            data2.put("size",message.getSize());
            data2.put("filename",message.getFilename());

            data2.put("mid", message.getMid());

            packet.put("data", data2);
            packet.put("tc",message.getTimestamp());
            packet.put("signature", PreferencesData.getString(context, "signature"));


            //end making packet


            // add to db
            DatabaseHandler db = new DatabaseHandler(context);
            PrivateMessageDataBase pm = new PrivateMessageDataBase(message.getMid(), "video",  message.getFrom(), to, Long.parseLong(message.getTimestamp()),picturePath, message.getSize() + "", "save", "", "",0);
// end add to db


            // update list
            if(!isAgain){
                db.InsertState(pm);
                ArrayList<PrivateMessageDataBase> all =  db.AlllPrivate(from,to);
                PrivateMessageDataBase pmd =all.get(all.size()-1);
                adapter.add(pmd);
            }

            // end update list


        } catch (JSONException e) {
            e.printStackTrace();
        }

        // send initialize file
        Security sec = new Security(context);
        sec.Pack(packet, "filesend");
*/


    }
    @Override
    public void onDestroy() {
      /*  if (mBottomSheetDialog != null) {
            mBottomSheetDialog.dismiss();
        }*/
        bus.unregister(this);
        PreferencesData.saveBoolean(context,"inhistory",false);
        super.onDestroy();
    }

    @Override
    public void onStop() {
        if(adapter!=null){
            adapter.releaseMediaPlayer();
        }
        PreferencesData.saveBoolean(context,"inhistory",false);
        bus.unregister(this);
        super.onStop();
    }


    private void makelist() {
        DatabaseHandler db = new DatabaseHandler(context);
        pmList = db.AlllPrivate(from,to);
        setMyAdapter();


        if(pmList.size()>0){
            db = new DatabaseHandler(context);

            if(pmList.get(pmList.size()-1).getMtype().contains("text")){
                db.updateConversation(to,pmList.get(pmList.size()-1).getContent(),0);

            }else {
                db.updateConversation(to,pmList.get(pmList.size()-1).getMtype(),0);

            }

            JSONObject data = new JSONObject();
            JSONObject packet = new JSONObject();
            try {
                data.put("reqid", 13);
                data.put("type", "private");
                data.put("target", to);
                data.put("offset", 0);
                data.put("count", 500);
                packet.put("data", data);
                packet.put("tc", System.currentTimeMillis() / 1000);
                packet.put("signature", PreferencesData.getString(context, "signature"));
                emitter.EmitHistoryRequest(data);
            }
            catch (JSONException e){
                e.printStackTrace();
            }

        }else {
            progressBar.setProgress(60);
            progressBar.setVisibility(View.VISIBLE);
            JSONObject data = new JSONObject();
            JSONObject packet = new JSONObject();
            try {


                data.put("reqid", 13);
                data.put("type", "private");
                data.put("target", to);
                data.put("offset", 0);
                data.put("count", 500);
                packet.put("data", data);
                packet.put("tc", System.currentTimeMillis() / 1000);
                packet.put("signature", PreferencesData.getString(context, "signature"));
                emitter.EmitHistoryRequest(data);
            }
            catch (JSONException e){
                e.printStackTrace();
            }
        }

    }



    private void setMyAdapter() {


            adapter= new HistoryAdapter(ActivityHistory.this,pmList);

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        mLayoutManager.setReverseLayout(false);
        mLayoutManager.setStackFromEnd(true);
        list.setLayoutManager(mLayoutManager);
            list.setItemAnimator(new DefaultItemAnimator());
            list.buildDrawingCache(false);
            list.setHasFixedSize(true);
            list.setItemViewCacheSize(40);
            list.setDrawingCacheEnabled(true);

            list.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO);
            list.addOnItemTouchListener(this);
            list.setAdapter(adapter);




      /*  list.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override

            public void onLayoutChange(View v, int left, int top, int right,int bottom, int oldLeft, int oldTop,int oldRight, int oldBottom)
            {

                list.scrollToPosition(lastvisible);

            }
        });*/
    }


    private void GoToLastPosition() {
        if(adapter!=null){
            if(adapter.getItemCount()>0)
                list.smoothScrollToPosition(adapter.getItemCount()-1);
        }


    }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onEvent(MessengerBus event) {

            if(event.getMessage().equals("privatedeleted")){




            }
            else if (event.getMessage().equals("onlinecontact")){
                if(event.getMes().equals(to)){
                    userState="Online";
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txttyping.setText(userState);
                        }
                    });
                }
            }
            else if (event.getMessage().equals("onStatusRes")){
                try {
                    JSONObject jo = new JSONObject(event.getMes());
                    final boolean isonline = jo.getBoolean("isonline");
                    final long lastonline = jo.getLong("lastonline");
                    String username = jo.getString("username");

                    if(isonline){
                        userState="Online";
                    }else {
                        userState="Offline";

                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(isonline) {
                                txttyping.setText(userState);
                            }else {
                                long difference = System.currentTimeMillis() - (lastonline*1000);
                                int days = (int) (difference / (1000*60*60*24));
                                int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
                                int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
                                if(days==0 && hours==0 && min<=5) {
                                    txttyping.setText("last online : recently");
                                }else if(days==0 && hours>0) {

                                    txttyping.setText("last online :"+hours+"  h:"+min+" m ago");
                                }else  if(days==0 && hours==0)
                                {
                                    txttyping.setText("last online :"+min+" min ago");

                                }
                                    else {
                                    txttyping.setText("last online :" +days+" days ago");
                                }
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
              /*  if(event.getMes().equals(to)){
                    userState="Online";
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txttyping.setText(userState);
                        }
                    });
                }*/
            }
            else if (event.getMessage().equals("offlinecontact")){
                if(event.getMes().equals(to)){
                    userState="Offline";
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txttyping.setText(userState);
                        }
                    });
                }
            }
           else if(event.getMessage().equals("clicked")){
                int idx=0;
                if(event.getView()!=null){
                    idx= list.getChildAdapterPosition(event.getView());

                }else {
                     idx = event.getPosition();
                }
                if (actionMode != null) {
                    ToggleSelection(idx);
                    return;
                }
            }
            else if(event.getMessage().equals("stickerChose")){
                sticker_parent.setVisibility(View.GONE);
                dialg.setVisibility(View.GONE);
                JSONObject data = new JSONObject();
                JSONObject packet = new JSONObject();


                try {
                    data.put("mtype", "sticker");
                    data.put("from", PreferencesData.getString(context, "userName"));
                    data.put("to", to);

                    data.put("size",1);
                    data.put("content", event.getMes());
                    long timestamp = System.currentTimeMillis() / 1000;
                    data.put("timestamp", timestamp);
                    data.put("replyto", replyMid);

                    data.put("forwarded", "");
                    String mid = Helper.sha256("sticker" + from + to + timestamp);

                    data.put("mid", mid);

                    data.put("orgFileName","");
                    packet.put("data", data);
                    packet.put("tc", System.currentTimeMillis() / 1000);
                    packet.put("signature", PreferencesData.getString(context, "signature"));

                    DatabaseHandler db = new DatabaseHandler(context);
                    PrivateMessageDataBase pm = new PrivateMessageDataBase(mid, "sticker", from, to, timestamp, event.getMes()
                            , 1 + "", "save", "", "",100,1);

                    if(!db.isExist(pm.getMid())) {
                        db.InsertState(pm, context);

                        adapter.UpdateList(pm);
                        adapter.notifyItemInserted(adapter.getItemCount()-1);
                        GoToLastPosition();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                emitter.EmitPrivateMessage(data);
             /*   Security sec = new Security(context);
                sec.Pack(packet, "privatetext");*/
            }
            if(event.getMessage().equals("newfile")){
                DatabaseHandler db = new DatabaseHandler(context);
               PrivateMessageDataBase item= db.GetSpecialPMessage(event.getMes());
                adapter.AddTOList(item);
            }
            if(event.getMessage().equals("notify")){
                int pos = Integer.parseInt(event.getMes());
                adapter.notifyItemChanged(pos);
            }
        else if (event.getMessage().equals("unpackfinished")) {
            if(event.getSender().equals("history")){

                try{
                    JSONObject jsonObject = new JSONObject(event.getMes());
                    JSONArray jsonarray = jsonObject.getJSONArray("messages");
                    for(int i  =0;  i<jsonarray.length();i++){

                        JSONObject jsonobj = jsonarray.getJSONObject(i);
                        HistoryResponseData data = new HistoryResponseData();
                        DatabaseHandler db = new DatabaseHandler(context);

                        data.content = jsonobj.getString("content");
                        data.mid = jsonobj.getString("mid");
                        data.content = jsonobj.getString("content");
                        data.timestamp = jsonobj.getString("timestamp");
                        data.to = jsonobj.getString("to");
                        data.replyto = jsonobj.getString("replyto");
                        data.forwarded = jsonobj.getString("forwarded");
                        data.from = jsonobj.getString("from");

                        String size = jsonobj.getString("size");

                        String fileName = jsonobj.getString("orgFileName");


                        int sizeint =0;
                        try{
                            sizeint = Integer.parseInt(size);
                        }catch (NumberFormatException ex){
                            ex.printStackTrace();
                        }
                        data.mtype = jsonobj.getString("mtype");
                        data.lastevent = jsonobj.getString("lastevent");

                        PrivateMessageDataBase pm = new PrivateMessageDataBase(data.mid,data.mtype,data.from,data.to,Long.parseLong(data.timestamp),data.content,size,data.lastevent,data.replyto,data.forwarded,100,((sizeint/100000)+1));
                        pm.setFileName(fileName);
                        if(!db.isExist(data.mid)){
                            db.InsertState(pm,context);
                            adapter.UpdateList(pm);
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }


                progressBar.setVisibility(View.GONE);
                GoToLastPosition();

            }else if(event.getSender().equals("forwarded")){
               Toast.makeText(context, "forwarded message send exactly", Toast.LENGTH_SHORT).show();
            }
            else if(event.getSender().equals("onprivatetyping")){

                try {
                    JSONObject obj  =new JSONObject(event.getMes());
                    String from = obj.getString("from");
                    if(from.equals(to)){
                        txttyping.setText("typing...");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else  if(event.getSender().equals("stopPrivateSendingData")){
                txttyping.setText(userState);
                //    Toast.makeText(context,event.getMes().toString(),Toast.LENGTH_LONG).show();
            }else if(event.getSender().equals("onprivatesending")){
                try {
                    JSONObject obj  =new JSONObject(event.getMes());
                    String from = obj.getString("from");
                    if(from.equals(to)){
                        txttyping.setText("sending data ...");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if(event.getSender().equals("onprivatetypingstop")){

                try {
                    JSONObject obj  =new JSONObject(event.getMes());
                    String from = obj.getString("from");
                    if(from.equals(to)){
                        txttyping.setText("");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            else if(event.getSender().equals("binaryresp"))
            {


                try {
                    JSONObject dt = new JSONObject(event.getMes());


                    DataChunkData dataChunkData = new DataChunkData();
                    chunkedsize = dt.getInt("maxchunksize");
                    dataChunkData.mid = dt.getString("mid");
                    dataChunkData.sequence = dt.getInt("initialsequence");
                    try {
                        bytesofimage = convertFileToByte(myfile);
                        sizes.clear();
                        mine = spilit(bytesofimage, chunkedsize, ((int) myfile.length() / chunkedsize) + 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dataChunkData.chunksize = sizes.get(0);
                    dataChunkData.content = mine.get(0);
                    JSONObject data = new JSONObject();
                    data.put("sequence", dataChunkData.sequence);
                    data.put("mid", dataChunkData.mid);
                    data.put("chunksize", dataChunkData.chunksize);
                    data.put("content", dataChunkData.content);

                    DatabaseHandler db = new DatabaseHandler(context);
                    PrivateMessageDataBase item = db.GetSpecialPMessage(dataChunkData.mid);
                    adapter.AddTOList(item);
                    adapter.notifyItemInserted(adapter.getItemCount());
                    GoToLastPosition();

                }catch (JSONException e) {
                    e.printStackTrace();
                }






            }

            else if(event.getSender().equals("binaryacc")){

                try{
                    JSONObject dt = new JSONObject(event.getMes());
                    DataChunkData dataChunkData = new DataChunkData();
                    dataChunkData.mid = dt.getString("mid");





                }catch (JSONException je){
                    je.printStackTrace();
                }

            }
        }else if(event.getMessage().equals("privateupdate")){
                if(event.getMes().equals("seen")){
                    ArrayList<String>mids = event.getMids();
                    for(int i = 0 ; i<mids.size();i++){
                        adapter.UpdateUi(mids.get(i));
                    }
                }
                else {
                    adapter.UpdateUi(event.getMes());
                }


        }else if(event.getMes().equals("dbup")){


        }
        else  if(event.getMessage().equals("upfile")){

          adapter.UpdateUi(event.getMes());

        }else  if(event.getMessage().equals("privateupdated")){

            new SendSeen().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);



            DatabaseHandler db =new DatabaseHandler(context);
            pmList = db.AlllPrivate(from,to);

            db = new DatabaseHandler(context);
            if(pmList.get(pmList.size()-1).getMtype().equals("text")){
                db.updateConversation(to,pmList.get(pmList.size()-1).getContent(),0);
            }else {
                db.updateConversation(to,pmList.get(pmList.size()-1).getMtype(),0);

            }


            progressBar.setVisibility(View.GONE);

            if(adapter.getItemCount()<pmList.size()){
                adapter.UpdateList(pmList.get(pmList.size()-1));
                adapter.notifyItemInserted(adapter.getItemCount()-1);
            }
            if(event.getSender().equals("pmessagerec")){
                try {
                    JSONObject object = new JSONObject(event.getMes());
                    String from = object.getString("from");
                    if(from.equals(to)){
                        fabdown.setVisibility(View.VISIBLE);

                        if(adapter.getItemCount()-3<=lastvisible){
                            fabdown.setVisibility(View.GONE);
                            GoToLastPosition();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        else if (event.getMessage().equals("messagesent")) {
            SaffronEmitter saffronEmitter = new SaffronEmitter();
            HistoryRequestData historyRequestData = new HistoryRequestData();
            historyRequestData.reqid = "13";
            historyRequestData.target = to;
            historyRequestData.type = "private";
            historyRequestData.username = PreferencesData.getString(context, "userName");
            // saffronEmitter.EmitHistoryRequest(historyRequestData);
        } else if (event.getMessage().equals("messagesapp")) {

            jsonstring = event.getJsonArray().toString();
           /* makelistWithOutAdapter();*/
        } else if (event.getMessage().equals("getmessage")) {
            SaffronEmitter saffronEmitter = new SaffronEmitter();
            HistoryRequestData historyRequestData = new HistoryRequestData();
            historyRequestData.reqid = "13";
            historyRequestData.target = to;
            historyRequestData.type = "private";
            historyRequestData.username = PreferencesData.getString(context, "userName");
            //  saffronEmitter.EmitHistoryRequest(historyRequestData);
        } else if (event.getMessage().equals("binaryresp")) {


        } else if (event.getMessage().equals("binaryacc")) {
            counter++;

            DataChunkData dataChunkData = new DataChunkData();

            dataChunkData.chunksize = sizes.get(counter)/* (int) files.get(counter).length()*/;
            dataChunkData.mid = event.getMid();
            dataChunkData.sequence = event.getInitial() + 1;
            dataChunkData.content = mine.get(counter);



            //    SaffronEmitter saffronEmitter = new SaffronEmitter();
            //         saffronEmitter.EmitDataChunk(dataChunkData);
        } else if (event.getMessage().equals("packfinish")) {
            if (event.getSender().equals("privatetext")) {
                SaffronEmitter emitter = new SaffronEmitter();
                JSONObject object = new JSONObject();
                try {
                    object.put("packet", event.getMes());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                emitter.EmitPrivateMessage(object);
            }else if(event.getSender().equals("statereq")){
                SaffronEmitter emitter = new SaffronEmitter();
                JSONObject object = new JSONObject();
                try {
                    object.put("packet", event.getMes());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                emitter.EmitReqState(object);
            }

            else if(event.getSender().equals("forwarded")){
                SaffronEmitter emitter = new SaffronEmitter();
                JSONObject object = new JSONObject();
                try{
                    object.put("packet",event.getMes());
                }catch (JSONException e){
                    e.printStackTrace();
                }
                emitter.EmitForwarded(object);
            }

            else if(event.getSender().equals("deleteMessage")){
                SaffronEmitter emitter = new SaffronEmitter();
                JSONObject object = new JSONObject();
                try{
                    object.put("packet",event.getMes());
                }catch (JSONException e){
                    e.printStackTrace();
                }
                emitter.EmitDeleteMessagesRequest(object);
            }

            else if(event.getSender().equals("datachunk")){

                SaffronEmitter emitter = new SaffronEmitter();
                JSONObject object = new JSONObject();
                try {
                    object.put("packet", event.getMes());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                emitter.EmitDataChunk(object);

            }
            else if(event.getSender().equals("history")){
                SaffronEmitter emitter = new SaffronEmitter();
                JSONObject object = new JSONObject();
                try {
                    object.put("packet",event.getMes());
                }catch (JSONException e){
                    e.printStackTrace();
                }
                emitter.EmitHistoryRequest(object);
            }else if(event.getSender().equals("filesend")){
                SaffronEmitter emitter = new SaffronEmitter();
                JSONObject object = new JSONObject();
                try {
                    object.put("packet",event.getMes());
                    emitter.EmitBinaryMessageRequest(object);
                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        }
        else if(event.getMessage().equals("initial")) {
            if(event.getMes().equals("")){
             /*   DatabaseHandler db = new DatabaseHandler(context);
                ArrayList<PrivateMessageDataBase> all = db.AlllPrivate(from, to);
                //if(list.getLastVisiblePosition())
                PrivateMessageDataBase pmd = all.get(all.size() - 1);
                adapter.add(pmd);*/
            }


        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)


    public  List<String>  spilit(byte[] bytearrayfile,int sizechunk,int parts) throws IOException {
        List<String> list = new ArrayList<>();
        int splitedsize=0;
        for(int i = 0 ;i<parts;i++){
            splitedsize+=sizechunk;
            int size = 0;
            if(bytearrayfile.length<splitedsize){
                size=bytearrayfile.length-(splitedsize-sizechunk);
            }else {
                size=sizechunk;
            }
            sizes.add(size);
            String a=    Base64.encodeBytes(bytearrayfile,splitedsize-sizechunk,size);
            list.add(a);
        }
        return list;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private boolean checkWriteExternalPermission() {

        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkReadExternalPermission() {

        String permission = "android.permission.READ_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Req Code", "" + requestCode);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length == 2 &&
                    grantResults[0] == MockPackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == MockPackageManager.PERMISSION_GRANTED) {

                Intent gallery_Intent = new Intent(context, GalleryUtil.class);
                PreferencesData.saveString(context,"replyMid",replyMid);
                replyMid="";
                startActivityForResult(gallery_Intent, GALLERY_ACTIVITY_CODE);

            }
        }
    }

    public  byte[] convertFileToByte(File file) throws IOException {
//init array with file length
        byte[] bytesArray = new byte[(int) file.length()];

        FileInputStream fis = new FileInputStream(file);
        fis.read(bytesArray); //read file into bytes[]
        fis.close();

        return bytesArray;
    }

    @Override
    protected void onResume() {
     //   UpdateUI();
        super.onResume();
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
       /* if (isGroup)
            inflater.inflate(R.menu.select_share_messages_group_menu, menu);
        else*/
        inflater.inflate(R.menu.select_share_messages_menu, menu);
        myMenu = menu;
        getSupportActionBar().hide();
        if (AppHelper.isAndroid5()) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(AppHelper.getColor(this, R.color.colorAccent));
        }
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        final int arraySize = adapter.getSelectedItems().size();

        switch (item.getItemId()){
            case (R.id.reply_content):
                rl_reply.setVisibility(View.VISIBLE);
                isreply=true;
                int currentPosition = adapter.getSelectedItems().get(0);
                PrivateMessageDataBase messagesModel = adapter.getItem(currentPosition);
                txtMainMessage.setText(messagesModel.getSender());
                if(messagesModel.getContent().length()>20){
                    txtMessreply.setText(messagesModel.getContent().substring(0,17)+" ...");
                }else {
                    txtMessreply.setText(messagesModel.getContent());

                }
                replyMid=messagesModel.getMid();
                actionMode.finish();
                getSupportActionBar().show();

                break;
            case (R.id.transfer_message):
                forwardDialog = new ChoseForwardDialog(context,ActivityHistory.this,to);
                forwardDialog.setListener(new dialofListener() {
                    @Override
                    public void accepted() {

                    }

                    @Override
                    public void rejected() {
                        forwardDialog.dismiss();
                    }

                    @Override
                    public void accepted(int result) {

                    }

                    @Override
                    public void accepted(String result) {

                        for(int x=0;x<arraySize;x++) {
                            JSONObject data = new JSONObject();
                            JSONObject forwarded = new JSONObject();
                            JSONObject packet = new JSONObject();

                            int currentPosition = adapter.getSelectedItems().get(x);
                            PrivateMessageDataBase messagesModel = adapter.getItem(currentPosition);


                            try {
                                data.put("mtype", messagesModel.getMtype());
                                data.put("from", PreferencesData.getString(context, "userName"));
                                data.put("to", result);

                                data.put("size", messagesModel.getSize());
                                long timestamp = System.currentTimeMillis() / 1000;
                                data.put("timestamp", timestamp);
                                data.put("replyto", "");


                                forwarded.put("mid",messagesModel.getMid());
                                forwarded.put("mtype",messagesModel.getMtype());
                                forwarded.put("from",messagesModel.getSender());
                                forwarded.put("size",messagesModel.getSize());
                                forwarded.put("timestamp",messagesModel.getTimestamp());
                                String path = messagesModel.getContent();
                                int index = path.lastIndexOf(".");
                                path = path.substring(index+1);
                                if(messagesModel.getMtype().equals("text")){
                                    forwarded.put("content",messagesModel.getContent());
                                    data.put("content", messagesModel.getContent());

                                }else {
                                    if(messagesModel.getMtype().equals("photo")){
                                        forwarded.put("content","th_"+messagesModel.getMid()+"."+path);
                                        data.put("content","th_"+messagesModel.getMid()+"."+path);

                                    }else{
                                        forwarded.put("content",messagesModel.getMid()+"."+path);
                                        data.put("content",messagesModel.getMid()+"."+path);
                                    }

                                }

                                data.put("forwarded",forwarded);
                                String mid = Helper.sha256(messagesModel.getMtype() + from + to + timestamp);
                                data.put("mid", mid);
                                packet.put("data", data);
                                packet.put("tc", System.currentTimeMillis() / 1000);
                                packet.put("signature", PreferencesData.getString(context, "signature"));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            emitter.EmitPrivateMessage(data);
                          /*  Security sec = new Security(context);
                            sec.Pack(packet, "privatetext");*/




                        }
                        Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
                        forwardDialog.dismiss();
                        actionMode.finish();
                        getSupportActionBar().show();

                    }

                });
                forwardDialog.show();


                break;
            case (R.id.delete_message):
                JSONArray jArry = new JSONArray();

                for (int x = 0; x < arraySize; x++) {


                    try {
                        currentPosition = adapter.getSelectedItems().get(x);
                        messagesModel = adapter.getItem(currentPosition);
                    JSONObject jObjd = new JSONObject();

                        jObjd.put("mid", messagesModel.getMid());
                        jArry.put(jObjd);
                        int position = adapter.IndexOfObject(messagesModel.getMid());
                        adapter.removeItem(position);
                    }
                     catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                JSONObject data = new JSONObject();
                JSONObject packet = new JSONObject();
                try {



                    data.put("messages", jArry);

       /*             Security sec = new Security(context);
                    sec.Pack(packet, "deleteMessage");*/
                    emitter.EmitDeleteMessagesRequest(data);

                }catch (JSONException ex){
                    ex.printStackTrace();
                }
                if(actionMode!=null) {
                    actionMode.finish();
                }
                getSupportActionBar().show();



                break;

        }

        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        this.actionMode = null;
        adapter.clearSelections();
        getSupportActionBar().show();
        if (AppHelper.isAndroid5()) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(AppHelper.getColor(this, R.color.colorPrimaryDark));
        }
    }

    private class RecyclerViewBenOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            try {
                View view = list.findChildViewUnder(e.getX(), e.getY());
                int currentPosition = list.getChildAdapterPosition(view);
                PrivateMessageDataBase messagesModel = adapter.getItem(currentPosition);
                if(actionMode!=null){
                    return;
                }
                actionMode = startActionMode(ActivityHistory.this);
                  //  ToggleSelection(currentPosition);

                super.onLongPress(e);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }
    private void ToggleSelection(int position) {

        adapter.toggleSelection(position);
        String title = String.format("%s", adapter.getSelectedItemCount());
        actionMode.setTitle(title);

        if(adapter.getSelectedItemCount()==0){
            actionMode.finish();
            getSupportActionBar().show();
        }
        if(adapter.getSelectedItemCount()!=1){
            if(myMenu.findItem(R.id.reply_content)!=null){
                myMenu.findItem(R.id.reply_content).setVisible(false);

            }
        }else if(adapter.getSelectedItemCount()==1){
            if(myMenu.findItem(R.id.reply_content)!=null){
                myMenu.findItem(R.id.reply_content).setVisible(true);
            }
        }

    }
    class MakePage extends AsyncTask<String, String, String> {

        @Override protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    makelist();
                }
            });

            return "";

        }



        @Override protected void onPostExecute(String value) {
            GoToLastPosition();

        }
    }


    class SendSeen extends AsyncTask<String, String, String> {

        @Override protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   DatabaseHandler db = new DatabaseHandler(context);
                    ArrayList<PrivateMessageDataBase>dblist = db.AlllPrivate(from,to);
                    for(int i = 0 ; i <dblist.size();i++){
                        if(dblist.get(i).getSender().equals(to)) {
                            if (!dblist.get(i).events.equals("seen")) {
                                db.updateState(dblist.get(i).getMid(), "seen");


                                try {
                                    JSONObject packet = new JSONObject();
                                    JSONObject data2 = new JSONObject();

                                    data2.put("mid", dblist.get(i).getMid());
                                    data2.put("from", dblist.get(i).getReciever());
                                    data2.put("to", dblist.get(i).getSender());
                                    packet.put("tc", System.currentTimeMillis() / 1000);
                                    packet.put("signature", PreferencesData.getString(context, "signature"));
                                    emitter.EmitSeen(data2);
                                    /*packet.put("data", data2);
                                    Security sec = new Security(context);
                                    sec.Pack(packet, "seen");
*/
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }
                    }
                }
            });

            return "";

        }



        @Override protected void onPostExecute(String value) {


        }
    }

    public void SendReqState(){
        try {
            JSONObject data = new JSONObject();
            JSONObject packet=new JSONObject();
            data.put("username", to);
            packet.put("data", data);
            packet.put("tc", System.currentTimeMillis() / 1000);
            packet.put("signature", PreferencesData.getString(context, "signature"));
            emitter.EmitReqState(data);
           /* Security sec = new Security(context);
            sec.Pack(packet, "statereq");*/
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

}
