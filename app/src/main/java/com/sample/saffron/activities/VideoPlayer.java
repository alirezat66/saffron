package com.sample.saffron.activities;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sample.saffron.R;
import com.sample.saffron.typo.InfoOfServer;

import java.io.File;

import uk.co.jakelee.vidsta.VidstaPlayer;

/**
 * Created by alireza on 6/29/2017.
 */

public class VideoPlayer extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        Bundle bundle = getIntent().getExtras();
        String path="";
        if(bundle!=null){
            path = bundle.getString("path");

        }
        VidstaPlayer player = (VidstaPlayer) findViewById(R.id.player);

        if(!path.contains("/Saffron/Videos")){
           String ipStr  = InfoOfServer.MultiMediaIpServer;

            path=    ipStr+"/uploads/"+path;
            player.setVideoSource(path);


        }else {
            player.setVideoSource(path)
            ;
            player.setAutoPlay(true);

        }



    }
}
