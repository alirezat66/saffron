package com.sample.saffron.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.materialtextfield.MaterialTextField;
import com.github.glomadrian.codeinputlib.CodeInput;
import com.sample.saffron.R;
import com.sample.saffron.entity.userData.ChallengeRequestData;
import com.sample.saffron.entity.userData.ChallengeResponseData;
import com.sample.saffron.sender.senderPackets;

import java.util.Arrays;

import info.hoang8f.widget.FButton;

/**
 * Created by alireza on 3/14/2017.
 */

public class Challenge_Req_Activity extends Activity implements View.OnClickListener{
    private Challenge_Req_Activity.DataUpdateReceiver dataUpdateReceiver;

    info.hoang8f.widget.FButton btnChaReq;
    com.github.glomadrian.codeinputlib.CodeInput codeInput;
    String challenge;
    String phone;
    boolean hasPassCode=false;
    com.github.florent37.materialtextfield.MaterialTextField laytwo;
    EditText edtPassCode ;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_req);
        Bundle bundle=getIntent().getExtras();
        context= this;
        if(bundle!=null){
            challenge=bundle.getString("challenge");
            PreferencesData.saveString(this,"challenge",challenge);
            phone=bundle.getString("phone");
            PreferencesData.saveString(this,"phone",phone);
        }
        hasPassCode=PreferencesData.getBoolean(context,"hasPasscode",false);
        ShowNotif(challenge);
        DefineView();
    }

    private void ShowNotif(String challenge) {

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =   new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_attach_24dp) // notification icon
                .setContentTitle("challenge code!") // title for notification
                .setContentText(challenge) // message for notification
                .setAutoCancel(true); // clear notification after click
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this,0,intent,Intent.FLAG_ACTIVITY_NEW_TASK);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder.setSound(uri);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText("Challenge code!"));
        long[] v = {500,1000};
        mBuilder.setVibrate(v);
        mNotificationManager.notify(0, mBuilder.build());

    }

    private void DefineView() {
        codeInput = (CodeInput) findViewById(R.id.pincode);
        edtPassCode = (EditText) findViewById(R.id.edtpasscode);
        laytwo  = (MaterialTextField) findViewById(R.id.laytwo);
        if(hasPassCode){
            laytwo.setVisibility(View.GONE);
        }
        btnChaReq= (FButton) findViewById(R.id.access_req_btn);
        btnChaReq.setOnClickListener(this);
       /* btnChaReq= (Button) findViewById(R.id.access_req_btn);
        edtChallenge= (EditText) findViewById(R.id.access_req_phone);
        edtChallenge.setText(challenge);
        txtError= (TextView) findViewById(R.id.errorTextView);
        btnChaReq.setOnClickListener(this);*/
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (dataUpdateReceiver != null) unregisterReceiver(dataUpdateReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (dataUpdateReceiver == null)
            dataUpdateReceiver = new Challenge_Req_Activity.DataUpdateReceiver();

        IntentFilter intentFilter = new IntentFilter("EventChallengeRes");
        registerReceiver(dataUpdateReceiver, intentFilter);

      /*  IntentFilter intentFilter2 = new IntentFilter("EventAccessRes");
        registerReceiver(dataUpdateReceiver, intentFilter2);*/

    }
    @Override
    public void onClick(View v) {
        challengeReq();
    }
    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("EventChallengeRes")) {
                ChallengeResponseData challengeResponseData = (ChallengeResponseData) intent.getExtras().getSerializable("key");
                Log.d(Constants.LOGTAG, "Challenge Response From server : " + challengeResponseData.answer);

                switch (challengeResponseData.answer) {

                    // Store userdata in preferences
                    // Redirect to userProfile activity
                    case Constants.CHALLENGERES_OK: {
                        //USERNAME = challengeResponseData.username;
// Send update profile event


                      Intent intent1 = new Intent(context,SignInActivity.class);
                        PreferencesData.saveString(Challenge_Req_Activity.this,"userName",challengeResponseData.username);
                        PreferencesData.saveString(Challenge_Req_Activity.this,"salt",challengeResponseData.salt);
                        intent1.putExtra("username",challengeResponseData.username);
                        intent1.putExtra("salt",challengeResponseData.salt);
                        intent1.putExtra("challenge",challenge);
                        PreferencesData.saveString(context,"passCode",edtPassCode.getText().toString());
                        startActivity(intent1);
                        finish();
                        break;

                    }
                    case Constants.CHALLENGERES_WRONG: {
                        Toast.makeText(context,"لطفا کد خود را به درستی وارد نمایید.",Toast.LENGTH_LONG).show();

                    }
                    case Constants.CHALLENGERES_REJECT: {
                        Toast.makeText(context,"لطفا کد خود را به درستی وارد نمایید.",Toast.LENGTH_LONG).show();

                    }
                    case Constants.CHALLENGERES_PHONE_LOCKED: {
                        Toast.makeText(context,"تلفن مورد نظر قفل شده است..",Toast.LENGTH_LONG).show();

                    }
                    default: {

                    }
                }

            }
        }
    }


    private void challengeReq() {


        senderPackets senderPacket =  new senderPackets(context);
        String valdCode= Arrays.toString(codeInput.getCode());
        valdCode = valdCode.replace("[","");
        valdCode = valdCode.replace("]","");
        valdCode = valdCode.replace(",","");
        valdCode = valdCode.replace(" ","");
       if (valdCode.equals(challenge)   ) {
            senderPacket.SenderChallengeRequestDate(phone, valdCode, edtPassCode.getText().toString());
        }else {
           Toast.makeText(context,"لطفا کد خود را به درستی وارد نمایید.",Toast.LENGTH_LONG).show();
       }

    }

    @Override
    protected void onStart() {
        challengeReqTwo();
        super.onStart();
    }

    private void challengeReqTwo() {


        senderPackets senderPacket =  new senderPackets(context);

            senderPacket.SenderChallengeRequestDate(phone, challenge, edtPassCode.getText().toString());


    }

}
