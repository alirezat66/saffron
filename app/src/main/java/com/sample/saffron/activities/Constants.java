package com.sample.saffron.activities;


public class Constants {
    public static final String LOGTAG = "SaffronTag";

    public static final String SIGNUPRES_WAIT_FOR_CHALLENGE = "WAIT_FOR_CHALLENGE";
    public static final String SIGNUPRES_PHONE_ALREADY_EXISTS = "PHONE_ALREADY_EXISTS";
    public static final String SIGNUPRES_REJECT  = "REJECT ";

    public static final String CHALLENGERES_OK  = "OK";
    public static final String CHALLENGERES_WRONG  = "WRONG";
    public static final String CHALLENGERES_REJECT  = "REJECT";
    public static final String CHALLENGERES_PHONE_LOCKED  = "PHONE_LOCKED";

    public static final String PREFERENCES_SAFFRON  = "PREFERENCES_SAFFRON";
    public static final String PREFERENCES_USERNAME  = "PREFERENCES_USERNAME";
    public static final String PREFERENCES_SK  = "PREFERENCES_SK";
    public static final String PREFERENCES_IV  = "PREFERENCES_IV";
    public static final String PREFERENCES_SALT  = "PREFERENCES_SALT";
    public static final String PREFERENCES_CHALLENGE  = "PREFERENCES_CHALLENGE";

    public static final String SIGNIN_OK  = "OK";
    public static final String SIGNIN_BAD_CREDENTIALS  = "BAD_CREDENTIALS";
    public static final String SIGNIN_WAIT_FOR_CHALLENGE  = "WAIT_FOR_CHALLENGE";
    public static final String SIGNIN_REJECT  = "REJECT";

    public static final String UPDATEPROFILE_USERNAME  = "username";

}
