package com.sample.saffron.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.test.mock.MockPackageManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.github.rubensousa.bottomsheetbuilder.adapter.BottomSheetItemClickListener;
import com.sample.saffron.R;
import com.sample.saffron.adapters.HistoryAdapter;
import com.sample.saffron.adapters.HistoryGroupAdapter;
import com.sample.saffron.entity.groupData.HistoryRequestData;
import com.sample.saffron.entity.groupData.HistoryResponseData;
import com.sample.saffron.entity.messageData.BinaryMessageRequestData;
import com.sample.saffron.entity.messageData.DataChunkData;
import com.sample.saffron.entity.messageData.GroupMessageData;
import com.sample.saffron.entity.messageData.PrivateMessageData;
import com.sample.saffron.helper.MessengerBus;

import org.java_websocket.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by alireza on 3/30/2017.
 */

public class ActivityHistoryGroup extends AppCompatActivity {

    private static final int REQUEST_CODE_PERMISSION = 2;
    private final int GALLERY_ACTIVITY_CODE = 100;
    private final  int GALLERY_VIDEO_ACTIVITY_CODE = 200;
    ImageView btn_back,btn_attach;
    String target,jsonstring;
    TextView txt_target;
    ListView list ;
    ImageView img_send;
    EditText edt_message;
    String to;
    File myfile;
    public  int counter=0;

    private boolean mShowingGridDialog;

    Context context;
    ArrayList<HistoryResponseData>historylist = new ArrayList<>();
    EventBus bus =EventBus.getDefault();
    HistoryGroupAdapter adapter = new HistoryGroupAdapter(ActivityHistoryGroup.this,historylist);
    byte[] bytesofimage=null;
    List<Integer> sizes=new ArrayList<>();
    List<String>mine=new ArrayList<>();
    List<File>files=new ArrayList<>();
    String[] mPermission = {
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };
    public static final String STATE_GRID = "state_grid";
    private BottomSheetMenuDialog mBottomSheetDialog;
    private BottomSheetBehavior mBehavior;

    AppBarLayout appBarLayout;

    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Bundle bundle = getIntent().getExtras();
        context=this;
        if(bundle!=null){
            target = bundle.getString("targetname");
            jsonstring = bundle.getString("jsonstring");
            to = bundle.getString("to");
        }
        defineView();
    }

    private void defineView() {
     /*   toolbar = (Toolbar) findViewById(R.id.toolbar);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);*/

        btn_back = (ImageView) findViewById(R.id.img_back);
        txt_target = (TextView) findViewById(R.id.txt_target);
        txt_target.setText(target);
    //    img_send = (ImageView) findViewById(R.id.send_btn);
     //   edt_message = (EditText) findViewById(R.id.chat_edit_text);
        list = (ListView) findViewById(R.id.list);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        makelist();

        img_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_message.getText().toString().trim().length()>0){
                    SaffronEmitter saffronEmitter = new SaffronEmitter();
                    GroupMessageData message = new GroupMessageData();
                    message.username= PreferencesData.getString(context,"username");
                    message.mid= "";
                    String s = edt_message.getText().toString();
                    message.content= s;
                    message.mtype= "text";
                    message.from= PreferencesData.getString(context,"username");
                    message.forwarded="";
                    message.groupid=to;
                    message.size=edt_message.getText().toString().length()+"";
                    message.replyto="";
                    message.timestamp =  System.currentTimeMillis()+"";
                    saffronEmitter.EmitGroupMessage(message);
                    edt_message.setText("");
                }

            }
        });

        btn_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                }
                mShowingGridDialog = true;
                mBottomSheetDialog = new BottomSheetBuilder(ActivityHistoryGroup.this)
                        .setMode(BottomSheetBuilder.MODE_GRID)
                        .setAppBarLayout(appBarLayout)
                        .setMenu(getResources().getBoolean(R.bool.tablet_landscape)
                                ? R.menu.menu_bottom_grid_sheet : R.menu.menu_bottom_grid_sheet)
                        .expandOnStart(true)
                        .setItemClickListener(new BottomSheetItemClickListener() {
                            @Override
                            public void onBottomSheetItemClick(MenuItem item) {

                                if(item.getTitle().equals("Photos")){
                                    if (checkWriteExternalPermission() && checkReadExternalPermission()) {
                                        Intent gallery_Intent = new Intent(context, GalleryUtil.class);
                                        startActivityForResult(gallery_Intent, GALLERY_ACTIVITY_CODE);

                                    } else {

                                        ActivityCompat.requestPermissions(ActivityHistoryGroup.this,
                                                new String[]{mPermission[0],mPermission[1]}, REQUEST_CODE_PERMISSION);
                                    }
                                }else if(item.getTitle().equals("Videos")){
                                    if (checkWriteExternalPermission() && checkReadExternalPermission()) {
                                        Intent gallery_Intent = new Intent(context, GalleryUtilVideo.class);
                                        startActivityForResult(gallery_Intent, GALLERY_VIDEO_ACTIVITY_CODE);

                                    } else {

                                        ActivityCompat.requestPermissions(ActivityHistoryGroup.this,
                                                new String[]{mPermission[0],mPermission[1]}, REQUEST_CODE_PERMISSION);
                                    }
                                }


                                mShowingGridDialog = false;
                            }
                        })
                        .createDialog();
                mBottomSheetDialog.show();
//                mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            }
        });
        edt_message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(edt_message.getText().length()>0){
                    btn_attach.setVisibility(View.GONE);
                    img_send.setVisibility(View.VISIBLE);
                }else {
                    btn_attach.setVisibility(View.VISIBLE);
                    img_send.setVisibility(View.INVISIBLE);
                }
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String picturePath = "";
        if (requestCode == GALLERY_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                counter=0;
                picturePath = data.getStringExtra("picturePath");

                SaffronEmitter saffronEmitter = new SaffronEmitter();
                BinaryMessageRequestData message = new BinaryMessageRequestData();
                message.username= PreferencesData.getString(context,"username");



                message.from= PreferencesData.getString(context,"username");
                message.to= to;
                message.replyto="";
                message.type="group";
                message.btype="photo";
                myfile=new File(Uri.parse(picturePath).getPath());



                message.size=myfile.length();
                message.replyto="";
                message.timestamp =  System.currentTimeMillis()+"";

                String filename=picturePath.substring(picturePath.lastIndexOf("/")+1);
                message.filename=filename;

                message.mid=  Helper.sha256(message.btype+message.from+message.to+message.timestamp);





       //        saffronEmitter.EmitBinaryMessageRequest(message);

                edt_message.setText("");


                Toast.makeText(context, picturePath, Toast.LENGTH_LONG).show();
                //perform Crop on the Image Selected from Gallery
            }
        }else if(requestCode == GALLERY_VIDEO_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                counter=0;
                picturePath = data.getStringExtra("videoPath");

                SaffronEmitter saffronEmitter = new SaffronEmitter();
                BinaryMessageRequestData message = new BinaryMessageRequestData();
                message.username = PreferencesData.getString(context, "username");


                message.from = PreferencesData.getString(context, "username");
                message.to = to;
                message.replyto = "";
                message.type = "group";
                message.btype = "video";
                myfile = new File(Uri.parse(picturePath).getPath());


                message.size = myfile.length();
                message.replyto = "";
                message.timestamp = System.currentTimeMillis() + "";

                String filename = picturePath.substring(picturePath.lastIndexOf("/") + 1);
                message.filename = filename;

                message.mid = Helper.sha256(message.btype + message.from + message.to + message.timestamp);


         //       saffronEmitter.EmitBinaryMessageRequest(message);

                edt_message.setText("");


                Toast.makeText(context, picturePath, Toast.LENGTH_LONG).show();
                //perform Crop on the Image Selected from Gallery
            }
        }
    }
    @Override
    public void onStart() {
        bus.register(this);
        PreferencesData.saveBoolean(context,"inhistory",true);
        super.onStart();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        PreferencesData.saveBoolean(context,"inhistory",false);
        super.onDestroy();
    }

    @Override
    public void onStop() {
        PreferencesData.saveBoolean(context,"inhistory",false);
        bus.unregister(this);
        super.onStop();
    }


    private void makelist() {
        try {
            JSONObject jsonObject = new JSONObject(jsonstring);
            JSONArray jsonarray = jsonObject.getJSONArray("messages");
            for(int i  =0;  i<jsonarray.length();i++){
                JSONObject jsonobj = jsonarray.getJSONObject(i);
                HistoryResponseData data = new HistoryResponseData();
                data.content = jsonobj.getString("content");
                data.mid = jsonobj.getString("mid");
                data.content = jsonobj.getString("content");
                data.timestamp = jsonobj.getString("timestamp");
            //    data.to = jsonobj.getString("to");
                data.replyto = jsonobj.getString("replyto");
                data.forwarded = jsonobj.getString("forwarded");
                data.from = jsonobj.getString("from");
                data.mtype = jsonobj.getString("mtype");
                data.lastevent = jsonobj.getString("lastevent");

                historylist.add(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
     setMyAdapter();
    }
    private void makelistWithOutAdapter() {
        historylist = new ArrayList<>();
        try {
            JSONArray jsonarray = new JSONArray(jsonstring);
            for(int i  =0;  i<jsonarray.length();i++){
                JSONObject jsonobj = jsonarray.getJSONObject(i);
                HistoryResponseData data = new HistoryResponseData();
                data.content = jsonobj.getString("content");
                data.mid = jsonobj.getString("mid");
                data.content = jsonobj.getString("content");
                data.timestamp = jsonobj.getString("timestamp");
                data.replyto = jsonobj.getString("replyto");
                data.forwarded = jsonobj.getString("forwarded");
                data.from = jsonobj.getString("from");
                data.mtype = jsonobj.getString("mtype");
                data.lastevent = jsonobj.getString("lastevent");

                historylist.add(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.add(historylist.get(historylist.size()-1));
            }
        });

    }
    private void setMyAdapter() {
        list.setAdapter(adapter);
        list.setSelection(adapter.getCount() - 1);
        list.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        list.setStackFromBottom(true);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onEvent(MessengerBus event) {
       /* if (event.getMessage().equals("messagesent")) {
            SaffronEmitter saffronEmitter = new SaffronEmitter();
            HistoryRequestData historyRequestData = new HistoryRequestData();
            historyRequestData.reqid = "13";
            historyRequestData.target = to;
            historyRequestData.type = "group";
            historyRequestData.username = PreferencesData.getString(context, "username");
            saffronEmitter.EmitHistoryRequest(historyRequestData);
        }*/  if (event.getMessage().equals("messagesapp")) {

            jsonstring = event.getJsonArray().toString();
            makelistWithOutAdapter();
        }else if(event.getMessage().equals("getmessagegroup")){
            SaffronEmitter saffronEmitter = new SaffronEmitter();
            HistoryRequestData historyRequestData = new HistoryRequestData();
            historyRequestData.reqid = "13";
            historyRequestData.target = to;
            historyRequestData.type = "group";
            historyRequestData.username = PreferencesData.getString(context, "username");
         //   saffronEmitter.EmitHistoryRequest(historyRequestData);
        }else if(event.getMessage().equals("binaryresp")){
            DataChunkData dataChunkData=new DataChunkData();
            dataChunkData.mid=event.getMid();
            dataChunkData.sequence=event.getInitial();
          /*  try {
                files = splitFile(myfile,chunkedsize);
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            try {
                bytesofimage  = convertFileToByte(myfile);

                sizes.clear();
                mine  =spilit(bytesofimage,event.getMaxchunk(),((int)myfile.length()/event.getMaxchunk())+1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            dataChunkData.chunksize= sizes.get(0);

                /*InputStream input = new FileInputStream(files.get(0));
                Base64.InputStream inputStream = new Base64.InputStream(input);

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder total = new StringBuilder();
                String line;

                while ((line = r.readLine()) != null) {
                    total.append(line);
                }*/
            dataChunkData.content=mine.get(0);
            SaffronEmitter saffronEmitter=new SaffronEmitter();
        //    saffronEmitter.EmitDataChunk(dataChunkData);

        }else if(event.getMessage().equals("binaryacc")){
            counter++;

            DataChunkData dataChunkData=new DataChunkData();

            dataChunkData.chunksize=sizes.get(counter)/* (int) files.get(counter).length()*/;
            dataChunkData.mid=event.getMid();
            dataChunkData.sequence=event.getInitial()+1;
           /* try {
                long a = filesize;
              *//*  allsize+=files.get(counter).length();
              *//*
                InputStream input = new FileInputStream(files.get(counter));
                Base64.InputStream inputStream = new Base64.InputStream(input);
                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder total = new StringBuilder();
                String line;

                while ((line = r.readLine()) != null) {
                    total.append(line);
                }
             */
            dataChunkData.content=mine.get(counter);
            SaffronEmitter saffronEmitter=new SaffronEmitter();
        //    saffronEmitter.EmitDataChunk(dataChunkData);
            /*} catch (IOException e) {
                e.printStackTrace();
            }*/



        }
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public  List<String>  spilit(byte[] bytearrayfile,int sizechunk,int parts) throws IOException {
        List<String> list = new ArrayList<>();
        int splitedsize=0;
        for(int i = 0 ;i<parts;i++){
            splitedsize+=sizechunk;
            int size = 0;
            if(bytearrayfile.length<splitedsize){
                size=bytearrayfile.length-(splitedsize-sizechunk);
            }else {
                size=sizechunk;
            }
            sizes.add(size);
            String a=    Base64.encodeBytes(bytearrayfile,splitedsize-sizechunk,size);
            list.add(a);
        }
        return list;
    }
    private boolean checkWriteExternalPermission() {

        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkReadExternalPermission() {

        String permission = "android.permission.READ_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Req Code", "" + requestCode);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length == 2 &&
                    grantResults[0] == MockPackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == MockPackageManager.PERMISSION_GRANTED) {

                Intent gallery_Intent = new Intent(context, GalleryUtil.class);
                startActivityForResult(gallery_Intent, GALLERY_ACTIVITY_CODE);

            }
        }
    }

    public  byte[] convertFileToByte(File file) throws IOException {
//init array with file length
        byte[] bytesArray = new byte[(int) file.length()];

        FileInputStream fis = new FileInputStream(file);
        fis.read(bytesArray); //read file into bytes[]
        fis.close();

        return bytesArray;
    }


}
