package com.sample.saffron.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sample.saffron.R;
import com.sample.saffron.fragments.About;

import com.sample.saffron.fragments.Home;
import com.sample.saffron.fragments.setting;
import com.sample.saffron.helper.Consts;
import com.sample.saffron.typo.InfoOfServer;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    Context context;
    boolean doubleBackToExitPressedOnce = false;
    JSONObject packet = new JSONObject();
    JSONObject data = new JSONObject();
    CoordinatorLayout mainContent;
    DrawerLayout drawerLayout;
    NavigationView navDrawer;
    ActionBarDrawerToggle drawerToggle;
    Toolbar toolBar;
    ImageView menu_imgbtn, more_imgbtn;
    TextView title_tv;
    ImageView imgProfile;
    TextView txtUser;
    TextView txtphone;
    /*Activity Values*/
    ArrayList<Fragment> listFragments = new ArrayList<>();
    String username, fname, lname, imgurl, phone;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
/*
    private ProfileActivity.DataUpdateReceiver dataUpdateReceiver;
*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);



        AppController.setActivityContext(this,this);

        username= PreferencesData.getString(this,"username","");
        phone = PreferencesData.getString(this,"phone","");
        context = this;
        AppController.setActivityContext(this, getApplicationContext());

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mainContent = (CoordinatorLayout) findViewById(R.id.mainContent);


        findViews();
        setupDrawer();
        setupDrawerContent(navDrawer);


      /*  txtuser= (TextView) findViewById(R.id.txtusername);
        txtname= (TextView) findViewById(R.id.txtuserfirt);
        txtlname= (TextView) findViewById(R.id.txtuserlast);
        txtphone= (TextView) findViewById(R.id.txtuserphone);
        img = (ImageView) findViewById(R.id.img);
        txtuser.setText(username);
        txtname.setText(fname);
        txtlname.setText(lname);
        txtphone.setText(phone);
        String s = InfoOfServer.IpServer+"/uploads/"+imgurl;
        new DownloadImage().execute(s);

        SaffronEmitter saffronEmitter = new SaffronEmitter();
        ConversationRequestData conversationRequestData = new ConversationRequestData();
        conversationRequestData.username=txtuser.getText().toString();
        saffronEmitter.EmitConversationRequest(conversationRequestData);
*/
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Consts.freeMemory();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (!navDrawer.getMenu().findItem(R.id.btn_home).isChecked()) {
                drawerLayout.closeDrawer(GravityCompat.START);
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mainContent, listFragments.get(0)).commit();
                navDrawer.getMenu().findItem(R.id.btn_home).setChecked(true);
                title_tv.setText(navDrawer.getMenu().findItem(R.id.btn_home).getTitle());
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu_imgbtn:
                Consts.hideKeyboard();
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.more_imgbtn:
                //initMenu(more_imgbtn);
                break;
        }
    }

    private void findViews() {
        toolBar = (Toolbar) findViewById(R.id.toolBar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        navDrawer = (NavigationView) findViewById(R.id.navDrawer);

        menu_imgbtn = (ImageView) findViewById(R.id.menu_imgbtn);
        more_imgbtn = (ImageView) findViewById(R.id.more_imgbtn);

        title_tv = (TextView) findViewById(R.id.title_tv);
         imgProfile = (ImageView) navDrawer.getHeaderView(0).findViewById(R.id.imgProfile);
         txtUser      = (TextView) navDrawer.getHeaderView(0).findViewById(R.id.txtUser);
         txtphone    = (TextView) navDrawer.getHeaderView(0).findViewById(R.id.txtPhone);
        txtUser.setText(username);
        txtphone.setText(phone);

        String s = InfoOfServer.IpServer+"/uploads/"+imgurl;
        new DownloadImage().execute(s);
        setOnClick();
    }

    private void setOnClick() {
        menu_imgbtn.setOnClickListener(this);
        more_imgbtn.setOnClickListener(this);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, drawerLayout, toolBar, R.string.drawer_opened, R.string.drawer‌_closed) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                Consts.hideKeyboard();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Consts.hideKeyboard();
            }
        };
    }

    private void setupDrawer() {
        /*Setting Custom ActionBar*/
        setSupportActionBar(toolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        // Find our drawer view
        drawerToggle = setupDrawerToggle();
        // Tie DrawerLayout events to the ActionBarToggle
        drawerLayout.addDrawerListener(drawerToggle);

        listFragments.add(new Home());
        listFragments.add(new setting());
        listFragments.add(new About());



        /*Load first fragment as default*/
        getSupportFragmentManager().beginTransaction().replace(R.id.mainContent, listFragments.get(0)).commit();
        title_tv.setText(navDrawer.getMenu().findItem(R.id.btn_home).getTitle());
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        switch (menuItem.getItemId()) {
            /*Test Fragment*/
            case R.id.btn_home:
                fragmentManager.beginTransaction().replace(R.id.mainContent, listFragments.get(0)).commit();
                break;
            /*Favorites Fragment*/
            case R.id.btn_setting:

                fragmentManager.beginTransaction().replace(R.id.mainContent, listFragments.get(1)).commit();


                break;
            /*FavoredQuestion Fragment*/
            case R.id.btn_about:
                fragmentManager.beginTransaction().replace(R.id.mainContent, listFragments.get(2)).commit();
                break;

            default:
                fragmentManager.beginTransaction().replace(R.id.mainContent, listFragments.get(0)).commit();
                break;
        }

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        title_tv.setText(menuItem.getTitle());
        // Close the navigation drawer
        drawerLayout.closeDrawers();
    }


    @Override
    protected void onPause() {
/*
        if (dataUpdateReceiver != null) unregisterReceiver(dataUpdateReceiver);
*/
        super.onPause();
    }

    @Override
    protected void onResume() {
        AppController.setActivityContext(this,this);

 /*       if (dataUpdateReceiver == null) dataUpdateReceiver = new ProfileActivity.DataUpdateReceiver();

                    IntentFilter intentFilter = new IntentFilter("EventConversationRes");

        registerReceiver(dataUpdateReceiver, intentFilter);*/
        super.onResume();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Profile Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }



    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            // Set progressdialog title
            // Set progressdialog message
            // Show progressdialog
        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];

            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // Set the bitmap into ImageView
            imgProfile.setImageBitmap(result);
            // Close progressdialog
        }
    }



}
