package com.sample.saffron.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.sample.saffron.R;
import com.sample.saffron.client.LoginProcess;
import com.sample.saffron.client.Security;
import com.sample.saffron.entity.securityData.TimeSyncRequestData;
import com.sample.saffron.entity.userData.SignInResponseData;
import com.sample.saffron.helper.JsonToAns;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.sender.senderPackets;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

public class SplashActivity extends AppCompatActivity {

    private int splashActivityShowTime = 3000;
    EventBus bus = EventBus.getDefault();
    Context context;
    @Override
    protected void onStart() {
        bus.register(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.setCurrentContext(this);
        context=this;
        PreferencesData.saveBool(context,"fromOut",true);

        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_splash);
        if (!isMyServiceRunning(SaffronService.class)) {
            Intent intent = new Intent(this, SaffronService.class);
            startService(intent);
        }
        ImageView logo_imageView = (ImageView) findViewById(R.id.splash_activity_logo);
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(logo_imageView, "alpha", 1f, .3f);
        fadeOut.setDuration(500);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(logo_imageView, "alpha", .3f, 1f);
        fadeIn.setDuration(1000);
        final AnimatorSet mAnimationSet = new AnimatorSet();
        mAnimationSet.play(fadeIn).after(fadeOut);
        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationSet.start();
            }
        });
        mAnimationSet.start();
        Thread splashThread = new Thread() {

            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (waited < splashActivityShowTime) {
                        sleep(100);
                        waited += 100;

                        /*Log.d(LOGTAG, "Waited = " + String.valueOf(waited));
                        Log.d(LOGTAG, "splashActivityShowTime = " + String.valueOf(splashActivityShowTime));*/
                    }
                } catch (Exception e) {
                    Log.d(Constants.LOGTAG, e.getMessage());
                } finally {


/*
                    LoginProcess process = new LoginProcess();
                    process.Sync();
*/

                    /*TimeSyncRequestData timeReq  = new TimeSyncRequestData();
                    timeReq.timestamp  = String.valueOf((System.currentTimeMillis()/1000) );
                    SaffronEmitter emitter = new SaffronEmitter();
                    emitter.EmitTimeSyncRequest(timeReq);*/


                 }
            }
        };

        splashThread.start();


    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }




    public void onEvent(MessengerBus event) {


        if(event.getMessage().equals("gotologin")){
            startActivity(new Intent(SplashActivity.this,
                    Access_Req_Activity.class));
            finish();
        }else if(event.getMessage().equals("challengeNotOk")){
            Intent intent = new Intent(SplashActivity.this,Access_Req_Activity.class);
            startActivity(intent);
            finish();
        }else if(event.getMessage().equals("loginfinished")){
            finish();
        }

       /* if(event.getMessage().equals("syncfin")){

            long offset = Long.parseLong(event.getMes());
            PreferencesData.saveLong(context,"offset",offset);

            boolean islogedin=PreferencesData.getBoolean(SplashActivity.this,"logedin",false);
            if(!islogedin) {
                startActivity(new Intent(SplashActivity.this,
                        Access_Req_Activity.class));
                finish();
            }else {
                senderPackets sp = new senderPackets(context);
                String phone = PreferencesData.getString(context,"phone","");
                sp.SenderAccessRequestData(phone);
            }

        }
        else if(event.getMessage().equals("challengeRes")){
            senderPackets senderPacket =  new senderPackets(context);
            String phone = PreferencesData.getString(context,"phone","");

            String pass=PreferencesData.getString(context,"passCode","");

            senderPacket.SenderChallengeRequestDate(phone, event.getMes(), pass);

        }else if(event.getMessage().equals("EventChallengeRes")){
            if(event.getMes().equals(Constants.CHALLENGERES_OK)){
                senderPackets sender = new senderPackets(context);
                String userName = PreferencesData.getString(context,"userName","");
                JSONObject data  = sender.PackSignInStepOne(userName);
                Security s = new Security(context);
                s.Sign(data);

            }else {
                Intent intent = new Intent(SplashActivity.this,Access_Req_Activity.class);
                startActivity(intent);
                finish();
            }
        }
        if (event.getMessage().equals("signfinished")) {

            senderPackets sp = new senderPackets(context);

            String challenge = PreferencesData.getString(context,"challenge","");
            String salt = PreferencesData.getString(context,"salt","");
            String userName = PreferencesData.getString(context,"userName","");
            sp.PackSingInStepTwo(event.getMes(),challenge,salt,userName);
        }else if(event.getMessage().equals("packsignFinished") ){
            JSArray array = (JSArray) event.getObj();
            senderPackets sp = new senderPackets(context);
            sp.SendPacketSignIn(array);
        }
        else if(event.getMessage().equals("signinfinish")){
            try {



                JSONObject jObject = new JSONObject(event.getMes());
                JSONObject data = jObject.getJSONObject("data");
                SignInResponseData responseData = JsonToAns.getSignInAsn(data);
                if(responseData.getAnswer().equals("OK")){
                    PreferencesData.saveString(context,"sk",responseData.getSk());
                    PreferencesData.saveString(context,"iv",responseData.getIv());
                    PreferencesData.saveBoolean(context,"logedin",true);
                    Intent intent = new Intent(context,ProfileActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(context,"oops ! something is wrong",Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }*/else if(event.getMessage().equals("notSignIn")){
            Intent intent = new Intent(SplashActivity.this,Access_Req_Activity.class);
            startActivity(intent);
            finish();
        }
    }


}
