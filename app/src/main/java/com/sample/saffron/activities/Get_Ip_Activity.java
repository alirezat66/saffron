package com.sample.saffron.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sample.saffron.R;

import org.json.JSONObject;


/**
 * Created by alireza on 3/13/2017.
 */

public class Get_Ip_Activity extends Activity implements View.OnClickListener
{
    Button btnAccReq;
    EditText edtPhone;
    TextView txtError;
    JSONObject data=new JSONObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_req);
        DefineView();
    }

    private void DefineView() {
        btnAccReq= (Button) findViewById(R.id.access_req_btn);
        edtPhone= (EditText) findViewById(R.id.access_req_phone);
        edtPhone.setHint("192.168.1.2");
        txtError= (TextView) findViewById(R.id.errorTextView);
        btnAccReq.setOnClickListener(this);




    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {

        super.onStop();
    }

    public int uploadFile(String sourceFileUri) {
        return 0;
    }
    @Override
    public void onClick(View v) {

        PreferencesData.saveString(Get_Ip_Activity.this,"ip","http://"+edtPhone.getText().toString());
        Intent intent=new Intent(Get_Ip_Activity.this,SplashActivity.class);
        startActivity(intent);
        finish();

    }





}
