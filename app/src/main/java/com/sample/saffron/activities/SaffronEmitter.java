package com.sample.saffron.activities;

import android.widget.Toast;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import com.sample.saffron.entity.groupData.AddGroupUserRequestData;
import com.sample.saffron.entity.groupData.CreateGroupRequestData;
import com.sample.saffron.entity.groupData.GroupActivitiesRequestData;
import com.sample.saffron.entity.groupData.GroupInfoRequestData;
import com.sample.saffron.entity.groupData.LeaveGroupRequestData;
import com.sample.saffron.entity.groupData.RemoveGroupUserRequestData;
import com.sample.saffron.entity.groupData.UpdateGroupImageRequestData;
import com.sample.saffron.entity.groupData.UpdateGroupNameRequestData;
import com.sample.saffron.entity.messageData.CancelBinaryMessageData;
import com.sample.saffron.entity.messageData.DeleteMessagesRequestData;
import com.sample.saffron.entity.messageData.GroupMessageData;
import com.sample.saffron.entity.messageData.GroupStopTypingData;
import com.sample.saffron.entity.messageData.GroupTypingData;
import com.sample.saffron.entity.messageData.PrivateStopTypingData;
import com.sample.saffron.entity.messageData.PrivateTypingData;
import com.sample.saffron.entity.messageData.UpdateRequestData;
import com.sample.saffron.entity.securityData.ServerPkRequestData;
import com.sample.saffron.entity.securityData.TimeSyncRequestData;
import com.sample.saffron.entity.userData.AccessRequestData;
import com.sample.saffron.entity.userData.ChallengeRequestData;
import com.sample.saffron.entity.userData.ContactMemberData;
import com.sample.saffron.entity.userData.ContactsCheckRequestData;
import com.sample.saffron.entity.userData.ContactsStateRequestData;
import com.sample.saffron.entity.userData.InviteContactRequestData;
import com.sample.saffron.entity.userData.ProfileInfoRequestData;
import com.sample.saffron.entity.userData.SignUpRequestData;
import com.sample.saffron.entity.userData.UpdateProfileInfoRequestData;
import com.sample.saffron.struct.messagingStruct.StructDeleteMessagesRequest;
import com.sample.saffron.struct.userStruct.StructContactsCheckRequest;
import com.sample.saffron.struct.userStruct.StructUpdateProfileInfoRequest;
import com.sample.saffron.typo.InfoOfServer;

public class SaffronEmitter {

    public Socket mSocket;
    {
        try {

            String myip = InfoOfServer.IpServer+":3000";
            mSocket = IO.socket(myip);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * function events for User Information Management System
     */
    public void EmitAcceccRequset(AccessRequestData data){
        JSONObject sendAccessReq = new JSONObject();
        try{
            sendAccessReq.put("phone",data.phone);
            sendAccessReq.put("ctype",data.ctype);
            mSocket.emit("web-access-req",sendAccessReq);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void EmitSignUpRequest(SignUpRequestData data){
        //Emit event to server
        JSONObject sendSignUpReq = new JSONObject();
        try {
            sendSignUpReq.put("phone", data.phone);
            sendSignUpReq.put("timestamp", data.timestamp);
            mSocket.emit("sign-up-req", sendSignUpReq);
        } catch (JSONException e) {
        }
    }

    public void EmitSignOffRequest(){
        //Emit event to server
        mSocket.emit("sign-off-req");
    }

    public void EmitForwarded(JSONObject data){
        JSONObject sendUsername = data;
        mSocket.emit("forwarded-messages", sendUsername);
    }
    public void EmitReqState(JSONObject data){
        JSONObject req = data;
        mSocket.emit("user-status-req", req);
    }
    public void EmitSignInRequest(JSONObject data){
        JSONObject sendUsername = data;
        mSocket.emit("web-sign-in-req", sendUsername);
    }
    public void EmitHistoryRequest(JSONObject data){
        //Emit event to server

            mSocket.emit("history-req", data);
    }
    public void EmitterMemberRequest(ContactMemberData data){

        JSONObject sendUsername = new JSONObject();
        try {

            sendUsername.put("reqid", "");
            sendUsername.put("username",data.username);
            mSocket.emit("members-android-req", sendUsername);


        } catch (JSONException e) {
        }
    }

    public void EmitSignOutRequest(){
        //Emit event to server
        mSocket.emit("sign-out-req");
    }

    public void EmitChallengeRequest(ChallengeRequestData data){
        //Emit event to server
        JSONObject sendChallenge = new JSONObject();
        try {
            sendChallenge.put("phone", data.phone);
            sendChallenge.put("hchallenge", data.hchallenge);
            sendChallenge.put("Passcode", data.passCode);
            mSocket.emit("web-challenge-req", sendChallenge);
        } catch (JSONException e) {
        }
    }
    public void EmitProfileInfoRequest(JSONObject data){

            mSocket.emit("profile-info-req", data);

    }

    public void EmitConversationRequest(JSONObject data){

            mSocket.emit("conversations-req", data);

    }
    public void EmitContactsCheckRequest(ContactsCheckRequestData data){
        //Emit event to server
        /*
        JSONObject sendContact = new JSONObject();
        try {
            sendContact.put("contacts", data.contacts);
            mSocket.emit("contacts-check-req", sendContact);
        } catch (JSONException e) {
        }
        */
        try {
            JSONArray jArry = new JSONArray();
            for (StructContactsCheckRequest task : data.contacts) {
                JSONObject jObjd = new JSONObject();
                jObjd.put("phone", task.phone);
                jObjd.put("firstname", task.firstname);
                jObjd.put("lastname", task.lastname);
                jArry.put(jObjd);
            }
            JSONObject finalobject = new JSONObject();
            finalobject.put("contacts", jArry);
            mSocket.emit("contacts-check-req", finalobject);
        } catch(JSONException ex) {}
    }

    public void EmitContactsStateRequest(ContactsStateRequestData data){
        //Emit event to server
        JSONObject sendContact = new JSONObject();
        try {
            sendContact.put("type", data.type);
            sendContact.put("groupid", data.groupid);
            mSocket.emit("contacts-state-req", sendContact);
        } catch (JSONException e) {
        }
    }

    public void EmitUpdateProfileInfoRequest(UpdateProfileInfoRequestData data){
        //Emit event to server
        /*
        try {
            JSONArray jArry = new JSONArray();
            for (StructUpdateProfileInfoRequest task : data.updates) {
                JSONObject jObjd = new JSONObject();
                jObjd.put("imageformat", task.imageformat);
                jObjd.put("value", task.value);
                jObjd.put("utype", task.utype);
                jArry.put(jObjd);
            }
            mSocket.emit("update-profile-info-req", jArry);
        } catch(JSONException ex) {}
        */
        try {
            JSONArray jArry = new JSONArray();
            for (StructUpdateProfileInfoRequest task : data.updates) {
                JSONObject jObjd = new JSONObject();
                jObjd.put("utype", task.utype);
                jObjd.put("value", task.value);
                jObjd.put("imageformat", task.imageformat);
                jArry.put(jObjd);
            }
            JSONObject finalobject = new JSONObject();
            finalobject.put("updates", jArry);
            mSocket.emit("update-profile-info-req", finalobject);
        } catch(JSONException ex) {}
    }

    public void EmitInviteContactRequest(InviteContactRequestData data){
        //Emit event to server
        JSONObject sendTargetPhone = new JSONObject();
        try {
            sendTargetPhone.put("targetphone", data.targetphone);
            mSocket.emit("invite-contact-req", sendTargetPhone);
        } catch (JSONException e) {
        }
    }

    /**
     * function events for Group Information
     */
    public void EmitCreateGroupRequest(CreateGroupRequestData data){
        //Emit event to server
        JSONObject sendCreateGroup = new JSONObject();
        try {
            sendCreateGroup.put("reqid", data.reqid);
            sendCreateGroup.put("name", data.name);
            sendCreateGroup.put("image", data.image);
            sendCreateGroup.put("imageformat", data.imageformat);
            sendCreateGroup.put("timestamp", data.timestamp);
            mSocket.emit("create-group-req", sendCreateGroup);
        } catch (JSONException c) {
        }
    }

    public void EmitLeaveGroupRequest(LeaveGroupRequestData data){
        //Emit event to server
        JSONObject sendLeaveGroup = new JSONObject();
        try {
            sendLeaveGroup.put("groupid", data.groupid);
            sendLeaveGroup.put("timestamp", data.timestamp);
            mSocket.emit("leave-group-req", sendLeaveGroup);
        } catch (JSONException c) {
        }
    }

    public void EmitAddGroupUserRequest(AddGroupUserRequestData data){
        //Emit event to server
        JSONObject sendAddGroupUser = new JSONObject();
        try {
            sendAddGroupUser.put("targetuser", data.targetuser);
            sendAddGroupUser.put("groupid", data.groupid);
            sendAddGroupUser.put("timestamp", data.timestamp);
            mSocket.emit("add-group-user-req", sendAddGroupUser);
        } catch (JSONException c) {
        }
    }

    public void EmitRemoveGroupUserRequest(RemoveGroupUserRequestData data){
        //Emit event to server
        JSONObject sendRemoveGroupUser = new JSONObject();
        try {
            sendRemoveGroupUser.put("targetuser", data.targetuser);
            sendRemoveGroupUser.put("groupid", data.groupid);
            sendRemoveGroupUser.put("timestamp", data.timestamp);
            mSocket.emit("remove-group-user-req", sendRemoveGroupUser);
        } catch (JSONException c) {
        }
    }

    public void EmitUpdateGroupNameRequest(UpdateGroupNameRequestData data){
        //Emit event to server
        JSONObject sendUpdateGroupName = new JSONObject();
        try {
            sendUpdateGroupName.put("groupid", data.groupid);
            sendUpdateGroupName.put("timestamp", data.timestamp);
            sendUpdateGroupName.put("newname", data.newname);
            mSocket.emit("update-group-name-req", sendUpdateGroupName);
        } catch (JSONException c) {
        }
    }

    public void EmitUpdateGroupImageRequest(UpdateGroupImageRequestData data){
        //Emit event to server
        JSONObject sendUpdateGroupImage = new JSONObject();
        try {
            sendUpdateGroupImage.put("groupid", data.groupid);
            sendUpdateGroupImage.put("timestamp", data.timestamp);
            sendUpdateGroupImage.put("newimage", data.newimage);
            sendUpdateGroupImage.put("imageformat", data.imageformat);
            mSocket.emit("update-group-image-req", sendUpdateGroupImage);
        } catch (JSONException c) {
        }
    }

    public void EmitGroupActivitiesRequest(GroupActivitiesRequestData data){
        //Emit event to server
        JSONObject sendSince = new JSONObject();
        try {
            sendSince.put("since", data.since);
            mSocket.emit("group-activities-req", sendSince);
        } catch (JSONException c) {
        }
    }

    public void EmitGroupInfoRequest(GroupInfoRequestData data){
        //Emit event to server
        JSONObject sendGroupInfo = new JSONObject();
        try {
            sendGroupInfo.put("groupid", data.groupid);
            mSocket.emit("group-info-req", sendGroupInfo);
        } catch (JSONException c) {
        }
    }

    /**
     * function events for Messaging Information
     */
    public void EmitPrivateMessage(JSONObject data){

        mSocket.emit("private-message", data);

    }

    public void EmitGroupMessage(GroupMessageData data){
        //Emit event to server
        JSONObject sendText = new JSONObject();
        try {
            sendText.put("mid", data.mid);
            sendText.put("mtype", data.mtype);
            sendText.put("from", data.from);
            sendText.put("groupid", data.groupid);
            sendText.put("size", data.size);
            sendText.put("timestamp", data.timestamp);
            sendText.put("content", data.content);
            sendText.put("replyto", data.replyto);
            sendText.put("forwarded", data.forwarded);
            sendText.put("username",data.username);
            mSocket.emit("android-group-message", sendText);
        } catch (JSONException e) {
        }
    }

    public void EmitPrivateTyping(JSONObject data){
        //Emit event to server


            mSocket.emit("private-typing", data);

    }

    public void EmitPrivateStopTyping(JSONObject data){
        //Emit event to server
             mSocket.emit("private-stop-typing", data);

    }

    public void EmitGroupTyping(GroupTypingData data){
        //Emit event to server
        JSONObject sendGroupTyping = new JSONObject();
        try {
            sendGroupTyping.put("from", data.from);
            sendGroupTyping.put("groupid", data.groupid);
            mSocket.emit("group-typing", sendGroupTyping);
        } catch (JSONException e) {
        }
    }

    public void EmitGroupStopTyping(GroupStopTypingData data){
        //Emit event to server
        JSONObject sendGroupStopTyping = new JSONObject();
        try {
            sendGroupStopTyping.put("from", data.from);
            sendGroupStopTyping.put("groupid", data.groupid);
            mSocket.emit("group-stop-typing", sendGroupStopTyping);
        } catch (JSONException e) {
        }
    }


    public void EmitReceived(JSONObject data){


      /*  security s = new security( AppController.getCurrentContext());
        s.Unpack(data,"ondelivered");        //Emit event to server
        JSONObject sendReceived = new JSONObject();
        try {
            sendReceived.put("mid", data.mid);
            sendReceived.put("from", data.from);
            sendReceived.put("to", data.to);
            mSocket.emit("received", sendReceived);
        } catch (JSONException e) {
        }*/

        mSocket.emit("received", data);

    }

    public void EmitSeen(JSONObject data){
        //Emit event to server
        mSocket.emit("seen-private", data);

    }

    public void EmitBinaryMessageRequest(JSONObject data){
        mSocket.emit("binary-message-req", data);

        //Emit event to server

    }

    public void EmitDataChunk(JSONObject data){
        //Emit event to server
        mSocket.emit("data-chunk", data);


    }

    public void EmitCancelBinaryMessage(CancelBinaryMessageData data){
        //Emit event to server
        JSONObject sendCancelBinaryMessage = new JSONObject();
        try {
            sendCancelBinaryMessage.put("mid", data.mid);
            mSocket.emit("cancel-binary-message", sendCancelBinaryMessage);
        } catch (JSONException c) {
        }
    }

    public void EmitUpdateRequest(UpdateRequestData data){
        //Emit event to server
        JSONObject sendTimestamp = new JSONObject();
        try {
            sendTimestamp.put("since", data.since);
            mSocket.emit("update-req", sendTimestamp);
        } catch (JSONException e) {
        }
    }

    public void EmitEventsRequest(){
        //Emit event to server
        mSocket.emit("events-req");
    }

    public void EmitEventsFlush(){
        //Emit event to server
        mSocket.emit("events-flush");
    }

    public void EmitDeleteMessagesRequest(JSONObject data ){

        mSocket.emit("delete-messages-req", data);

    }

    /**
     * function events for Security Information
     */
    public void EmitTimeSyncRequest(TimeSyncRequestData data){
        //Emit event to server
        JSONObject sendTimeSync = new JSONObject();
        try {
            sendTimeSync.put("timestamp", data.timestamp);
            mSocket.emit("time-sync-req", sendTimeSync);
        } catch (JSONException e) {
        }
    }

    public void EmitServerPkRequest(ServerPkRequestData data){
        //Emit event to server
        JSONObject sendServerPk = new JSONObject();
        try {
            sendServerPk.put("appid", data.appid);
            sendServerPk.put("timestamp", data.timestamp);
            sendServerPk.put("signature", data.signature);
            mSocket.emit("server-pk-req", sendServerPk);
        } catch (JSONException e) {
        }
    }

    public void EmitGroupsSessionKeyRequest(){
        //Emit event to server
        mSocket.emit("groups-session-key-req");
    }


}
