package com.sample.saffron.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;

import io.fabric.sdk.android.Fabric;


public class AppController extends MultiDexApplication {

    private static final String TAG = AppController.class.getSimpleName();

    public Bitmap cropped = null;
    private static Activity CurrentActivity = null;
    private static Context CurrentContext;



    @Override
    public void onCreate() {
        
        // The following line triggers the initialization of ACRA
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        Fresco.initialize(this);

        MultiDex.install(this);

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
    public static void setCurrentContext(Context mCurrentContext) {
        AppController.CurrentContext = mCurrentContext;
    }

    public static void setActivityContext(Activity activity, Context context) {
        CurrentActivity = activity;
        CurrentContext = context;
    }

    public static Activity getCurrentActivity() {
        return CurrentActivity;
    }

    public static Context getCurrentContext() {
        return CurrentContext;
    }
}