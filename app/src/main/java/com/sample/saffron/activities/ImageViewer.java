package com.sample.saffron.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sample.saffron.R;
import com.squareup.picasso.Picasso;

/**
 * Created by alireza on 6/29/2017.
 */

public class ImageViewer extends Activity {
    ImageView img  ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_player);
        img = (ImageView) findViewById(R.id.img);

        Bundle bundle = getIntent().getExtras();
        String path = "";
        if(bundle!=null){
            path = bundle.getString("path");
            path=path.replace("th_","");
        }

        Glide.with(this).load(path).override(800,1600).diskCacheStrategy(DiskCacheStrategy.ALL).crossFade().fitCenter().into(img);

     /*   Picasso.with(this)
                .load(path)
                .placeholder(R.drawable.ic_logo)
                .error(R.drawable.ic_failure)
                .into(img);*/
    }
}
