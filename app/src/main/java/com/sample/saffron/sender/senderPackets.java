package com.sample.saffron.sender;

import android.content.Context;

import com.sample.saffron.activities.AppController;
import com.sample.saffron.activities.Helper;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.activities.SaffronEmitter;
import com.sample.saffron.client.Security;
import com.sample.saffron.entity.userData.AccessRequestData;
import com.sample.saffron.entity.userData.ChallengeRequestData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alireza on 6/21/2017.
 */

public class senderPackets {
    Context context ;

    public senderPackets(Context context) {
        this.context = context;
    }

    public void SenderAccessRequestData(String phoneNumber){

        SaffronEmitter saffronEmitter = new SaffronEmitter();
        AccessRequestData accData = new AccessRequestData();
        accData.phone =phoneNumber;
        accData.ctype ="sms";
        saffronEmitter.EmitAcceccRequset(accData);
    }
    public void SenderChallengeRequestDate(String phone,String challange,String passCode){
        SaffronEmitter saffronEmitter = new SaffronEmitter();
        ChallengeRequestData challengeRequestData = new ChallengeRequestData();
        challengeRequestData.phone =phone;
        challengeRequestData.hchallenge = Helper.sha256(challange);
        challengeRequestData.passCode =passCode;
        saffronEmitter.EmitChallengeRequest(challengeRequestData);
    }

    public JSONObject PackSignInStepOne(String userName){
        JSONObject data = new JSONObject();
        String pkc = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl3V6Ne+XSKK8PmvSvl1UsMk9oSF8Tese3C1LLOZuVmRtjRHx1ckOJuBjbFyzst1vjan5124AlAQ7yqpygfiY+iaDpB63XcATOyL4BOt9FX0iBkS62KwyO00F3IrSqnTlOh7p9DhRGm7fvikRAwC0wCGPnlw12SD4PMEpREodRJW0uKPyPgmpR80ItwRJ339v4rG6Qx5nkIafLtxjZzdUZ482dxk6RBsBSdoHS0woynkXMS4/vKQ7bZW0WAJfoVImye93AmwO3yAt1vBYvwWAfOTT1YQyshnrzLe92ZUSeiWR5ZXmTqoSHxXrZz8T3SqAb9C4ThUw6MF2FWnwYuLoCwIDAQAB-----END PUBLIC KEY-----";
        long tc = System.currentTimeMillis()/1000 ;


        try {
            data.put("pkc",pkc);
            data.put("username",userName);
            data.put("tc",tc);
            data.put("password",Helper.sha256(PreferencesData.getString(AppController.getCurrentContext(),"chal")+PreferencesData.getString(AppController.getCurrentContext(),"salt")));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }
    public void  PackSingInStepTwo(String signiture,String challenge,String salt,String userName){

        JSONObject packet = new JSONObject();
        try {

            String pkc = "-----BEGIN PUBLIC KEY-----\r" +
                    "\n" +
                    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl3V6Ne+XSKK8PmvSvl1U\r" +
                    "\n" +
                    "sMk9oSF8Tese3C1LLOZuVmRtjRHx1ckOJuBjbFyzst1vjan5124AlAQ7yqpygfiY\r" +
                    "\n" +
                    "+iaDpB63XcATOyL4BOt9FX0iBkS62KwyO00F3IrSqnTlOh7p9DhRGm7fvikRAwC0\r" +
                    "\n" +
                    "wCGPnlw12SD4PMEpREodRJW0uKPyPgmpR80ItwRJ339v4rG6Qx5nkIafLtxjZzdU\r" +
                    "\n" +
                    "Z482dxk6RBsBSdoHS0woynkXMS4/vKQ7bZW0WAJfoVImye93AmwO3yAt1vBYvwWA\r" +
                    "\n" +
                    "fOTT1YQyshnrzLe92ZUSeiWR5ZXmTqoSHxXrZz8T3SqAb9C4ThUw6MF2FWnwYuLo\r" +
                    "\n" +
                    "CwIDAQAB\n" +
                    "\n" +
                    "-----END PUBLIC KEY-----\r\n";


            long tc = System.currentTimeMillis()/1000;

            JSONObject data = new JSONObject();
            String  pass2 = Helper.sha256(challenge+salt);

            data.put("password",pass2);
            data.put("pkc",pkc);

            data.put("username", userName);
            packet.put("data",data);
            packet.put("tc",tc);
            packet.put("signature",signiture);
            PreferencesData.saveString(context,"signature",signiture);
            PreferencesData.saveString(context,"password",pass2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
       /* Security se = new Security(context);
        se.PackSign(packet);*/
    }



}
