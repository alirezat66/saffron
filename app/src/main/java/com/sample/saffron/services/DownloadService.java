package com.sample.saffron.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;

import com.sample.saffron.activities.VideoPlayer;
import com.sample.saffron.helper.FileManager;
import com.sample.saffron.interfacer.DownloadCallbacks;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by alireza on 7/18/2017.
 */

public class DownloadService extends IntentService {
    public static final int UPDATE_PROGRESS = 8344;
    public DownloadService() {
        super("DownloadService");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        String urlToDownload = intent.getStringExtra("url");
        String mid = intent.getStringExtra("mid");
        String name = intent.getStringExtra("fileAdd");
        String type = intent.getStringExtra("type");
        ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
        final FileManager fileManager = new FileManager();
        try
        {
            URL url = new URL(urlToDownload);
                        /* making a directory in sdcard */

            File myDir = new File(fileManager.makeFile(type).toString()+"/"+name);

            URLConnection ucon = url.openConnection();
            InputStream inputStream = null;
            HttpURLConnection httpConn = (HttpURLConnection)ucon;
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                inputStream = httpConn.getInputStream();
            }

            FileOutputStream fos = new FileOutputStream(myDir);
            int totalSize = httpConn.getContentLength();
            int downloadedSize = 0;
            byte[] buffer = new byte[1024];
            int bufferLength = 0;
            while ( (bufferLength = inputStream.read(buffer)) >0 )
            {
                fos.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;

                final int percent =(int)(( (double)downloadedSize/(double)totalSize) *100);
                Bundle resultData = new Bundle();
                resultData.putInt("progress" ,percent);
                resultData.putString("mid" ,mid);
                receiver.send(UPDATE_PROGRESS, resultData);
                Log.i("Progress:","downloadedSize:"+downloadedSize+"totalSize:"+ totalSize) ;
            }
            fos.close();
            Log.d("test", "zakhire kamel shod..");
        }
        catch(IOException io)
        {
            Bundle resultData = new Bundle();
            resultData.putInt("progress" ,-1);
            resultData.putString("mid" ,mid);
            receiver.send(UPDATE_PROGRESS,resultData);
            io.printStackTrace();

        }
        catch(Exception e)
        {
            Bundle resultData = new Bundle();
            resultData.putInt("progress" ,-1);
            resultData.putString("mid" ,mid);
            receiver.send(UPDATE_PROGRESS, resultData);
            e.printStackTrace();
        }

        Bundle resultData = new Bundle();
        resultData.putInt("progress" ,100);
        resultData.putString("mid" ,mid);
        receiver.send(UPDATE_PROGRESS, resultData);
    }
}
