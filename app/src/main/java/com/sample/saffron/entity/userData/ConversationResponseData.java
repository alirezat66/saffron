package com.sample.saffron.entity.userData;

import com.sample.saffron.interfacer.EventData;

import org.json.JSONArray;

import java.io.Serializable;

/**
 * Created by alireza on 3/13/2017.
 */

public class ConversationResponseData implements EventData,Serializable {
    public  String target;
    public  String type;
    public  String fname;
    public  String lname;
    public  String imgUrl;
    public  String phone;
    public  String state;
    public String unseen;
    public  String lastonline;
    public String lastMessage;

    public ConversationResponseData() {
    }

    public ConversationResponseData(String target, String type, String fname, String lname, String imgUrl, String phone, String state, String unseen, String lastonline, String lastMessage) {
        this.target = target;
        this.type = type;
        this.fname = fname;
        this.lname = lname;
        this.imgUrl = imgUrl;
        this.phone = phone;
        this.state = state;
        this.unseen = unseen;
        this.lastonline = lastonline;
        this.lastMessage = lastMessage;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public String getType() {
        return type;
    }

    public String getTarget() {
        return target;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getUnseen() {
        return unseen;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getPhone() {
        return phone;
    }

    public String getState() {
        return state;
    }

    public String getLastonline() {
        return lastonline;
    }

    public JSONArray conversationArray;
    @Override
    public String toJsonString() {
        String result = "";
        //do work (create json from data)
        return result;

    }


}



