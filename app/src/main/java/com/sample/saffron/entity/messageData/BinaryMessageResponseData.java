package com.sample.saffron.entity.messageData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;

public class BinaryMessageResponseData implements EventData,Serializable {
    public String mid;
    public String answer; // answer = OK || REJECT
    public int maxchunksize;
    public int initialsequence;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
