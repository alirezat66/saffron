package com.sample.saffron.entity.userData;

import org.json.JSONException;
import org.json.JSONObject;

import com.sample.saffron.interfacer.EventData;


public class InviteContactRequestData implements EventData {
    public String username;
    public String targetphone;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            json.put("targetphone", targetphone);
        } catch (JSONException e) {
        }
        result = json.toString();
        return result;
    }
}
