package com.sample.saffron.entity.groupData;

import java.io.Serializable;
import java.util.ArrayList;

import com.sample.saffron.interfacer.EventData;
import com.sample.saffron.struct.groupStruct.StructGroupInfoResponse;

public class GroupInfoResponseData implements EventData,Serializable {
    public String groupid;
    public String name;
    public String image;
    public static ArrayList<StructGroupInfoResponse> members;
    //include members: username
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
