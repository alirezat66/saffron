package com.sample.saffron.entity.messageData;

import com.sample.saffron.interfacer.EventData;

public class BinaryMessageRequestData implements EventData {
    public String username;
    public String mid;
    public String type; // type = private || group
    public String filename;
    public String btype; // btype = file || photo || voice || video
    public long size;
    public String from;
    public String to;
    public String timestamp;
    public String replyto;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }

    public String getUsername() {
        return username;
    }

    public String getMid() {
        return mid;
    }

    public String getType() {
        return type;
    }

    public String getFilename() {
        return filename;
    }

    public String getBtype() {
        return btype;
    }

    public long getSize() {
        return size;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getReplyto() {
        return replyto;
    }
}
