package com.sample.saffron.entity.messageData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;

public class DataChunkAckData implements EventData,Serializable {
    public String mid;
    public int sequence;
    public String state; // state = OUT_OF_SEQUENCE || RECEIVED
    public int lastsequence;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
