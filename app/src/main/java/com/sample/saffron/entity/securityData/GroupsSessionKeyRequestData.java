package com.sample.saffron.entity.securityData;

import org.json.JSONException;
import org.json.JSONObject;

import com.sample.saffron.interfacer.EventData;

public class GroupsSessionKeyRequestData implements EventData {
    public String username;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
        } catch (JSONException e) {
        }
        result = json.toString();
        return result;
    }
}
