package com.sample.saffron.entity.userData;

import com.sample.saffron.interfacer.EventData;

import org.json.JSONObject;


public class SignInRequestData implements EventData {
    public String username;
    public String password;
    public String pkc="-----BEGIN PUBLIC KEY-----\n" +
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDNCoBFasoNKHc5Uxrkla+UzHll\n" +
            "oUJWkSesID6M8ERQep4e3+8sIlWqEymlQuLnJE7h2B27+kbhCCUXfwt3M0pdiM+W\n" +
            "UEv14fRBY/dNiMy0iWQ8StvGSIfq80atZ7SWxZRBSn+kiuFmaVkZtzb3h8QqSqj5\n" +
            "hJFmbuRU7pyGZDrGuwIDAQAB\n" +
            "-----END PUBLIC KEY-----\r\n";
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
