package com.sample.saffron.entity.groupData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;

public class GroupInfoUpdatedData implements EventData,Serializable {
    public String username;
    public String groupid;
    public String timestamp;
    public String utype; // utype = name || image
    public String newvalue;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
