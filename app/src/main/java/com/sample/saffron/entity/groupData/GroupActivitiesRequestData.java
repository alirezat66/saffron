package com.sample.saffron.entity.groupData;

import org.json.JSONException;
import org.json.JSONObject;

import com.sample.saffron.interfacer.EventData;

public class GroupActivitiesRequestData implements EventData {
    public String since;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        JSONObject json = new JSONObject();
        try {
            json.put("since", since);
        } catch (JSONException e) {
        }
        result = json.toString();
        return result;
    }
}
