package com.sample.saffron.entity.userData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;


public class SignUpResponseData implements EventData,Serializable {
    public String answer; // answer = WAIT_FOR_CHALLANGE || PHONE_ALREADY_EXISTS || REJECT
    public String salt;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return answer+salt;
    }
}
