package com.sample.saffron.entity.groupData;

import com.sample.saffron.interfacer.EventData;

/**
 * Created by alireza on 3/30/2017.
 */

public class HistoryRequestData implements EventData {
    public String reqid; // answer = OK || WRONG || PHONE_LOCKED
    public String type;
    public String username;
    public String target;
    public String count="1000";
    public String offset="0";


    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
