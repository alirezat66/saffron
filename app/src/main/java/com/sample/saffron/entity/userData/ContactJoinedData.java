package com.sample.saffron.entity.userData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;


public class ContactJoinedData implements EventData,Serializable {
    public String username;
    public String phone;
    public String firstname;
    public String lastname;
    public String imageurl;
    public String timestamp;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
