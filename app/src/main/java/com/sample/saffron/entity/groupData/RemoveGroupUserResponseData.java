package com.sample.saffron.entity.groupData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;

public class RemoveGroupUserResponseData implements EventData,Serializable {
    public String targetuser;
    public String groupid;
    public String answer; // answer = OK || REJECT || NOT_ALLOWED
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
