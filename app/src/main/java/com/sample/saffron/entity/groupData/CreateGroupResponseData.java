package com.sample.saffron.entity.groupData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;

public class CreateGroupResponseData implements EventData,Serializable {
    public String reqid;
    public String answer; // answer = OK || REJECT
    public String groupid;
    public String imageurl;
    public String key;
    public String iv;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
