package com.sample.saffron.entity.groupData;

import org.json.JSONException;
import org.json.JSONObject;

import com.sample.saffron.interfacer.EventData;

public class GroupInfoRequestData implements EventData {
    public String groupid;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        JSONObject json = new JSONObject();
        try {
            json.put("groupid", groupid);
        } catch (JSONException e) {
        }
        result = json.toString();
        return result;
    }
}
