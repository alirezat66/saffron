package com.sample.saffron.entity.userData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;


public class SignInResponseData implements EventData,Serializable {
    public String answer; // answer = OK || WAIT_FOR_CHALLENGE || REJECT
    public String sk;
    public String iv;

    public SignInResponseData(String answer, String sk, String iv) {
        this.answer = answer;
        this.sk = sk;
        this.iv = iv;
    }

    public String getAnswer() {

        return answer;
    }

    public String getSk() {
        return sk;
    }

    public String getIv() {
        return iv;
    }

    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
