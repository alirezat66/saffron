package com.sample.saffron.entity.userData;

import com.sample.saffron.interfacer.EventData;

import java.io.Serializable;


public class ProfileInfoResponseData implements EventData,Serializable {
    public String username;
    public String firstname;
    public String lastname;
    public String imageuri;
    public String phone;


    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
