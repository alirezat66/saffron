package com.sample.saffron.entity.messageData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;

public class PrivateMessageData implements EventData,Serializable {
    public String mid;
    public String mtype; // mtype = text || file || photo || voice || video || location || sticker
    public String from;
    public String to;
    public String size;
    public String timestamp;
    public String content;
    public String replyto;
    public String forwarded;
    public String username;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
