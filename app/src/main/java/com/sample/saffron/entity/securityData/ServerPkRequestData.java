package com.sample.saffron.entity.securityData;

import org.json.JSONException;
import org.json.JSONObject;

import com.sample.saffron.interfacer.EventData;


public class ServerPkRequestData implements EventData {
    public String appid;
    public String timestamp;
    public String signature;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        JSONObject json = new JSONObject();
        try {
            json.put("appid", appid);
            json.put("timestamp", timestamp);
            json.put("signature", signature);
        } catch (JSONException e) {
        }
        result = json.toString();
        return result;
    }
}
