package com.sample.saffron.entity.groupData;

import com.sample.saffron.interfacer.EventData;

import org.json.JSONArray;

import java.io.Serializable;

/**
 * Created by alireza on 3/30/2017.
 */

public class HistoryResponseData  implements EventData,Serializable {
    public  String mid;
    public  String content;
    public  String timestamp;
    public  String to;
    public  String replyto;
    public  String forwarded;
    public  String from;
    public  String mtype;
    public  String lastevent;

    public String getMid() {
        return mid;
    }

    public String getContent() {
        return content;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getTo() {
        return to;
    }

    public String getReplyto() {
        return replyto;
    }

    public String getForwarded() {
        return forwarded;
    }

    public String getFrom() {
        return from;
    }

    public String getMtype() {
        return mtype;
    }

    public String getLastevent() {
        return lastevent;
    }

    public JSONArray conversationArray;
    @Override
    public String toJsonString() {
        String result = "";
        //do work (create json from data)
        return result;

    }


}
