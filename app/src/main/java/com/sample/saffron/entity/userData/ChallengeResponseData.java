package com.sample.saffron.entity.userData;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;


public class ChallengeResponseData implements EventData,Serializable {
    public String answer; // answer = OK || WRONG || PHONE_LOCKED
    public String username;
    public String salt;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        return result;
    }
}
