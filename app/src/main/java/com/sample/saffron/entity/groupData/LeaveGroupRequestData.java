package com.sample.saffron.entity.groupData;

import org.json.JSONException;
import org.json.JSONObject;

import com.sample.saffron.interfacer.EventData;

public class LeaveGroupRequestData implements EventData {
    public String username;
    public String groupid;
    public String timestamp;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            json.put("groupid", groupid);
            json.put("timestamp", timestamp);
        } catch (JSONException e) {
        }
        result = json.toString();
        return result;
    }
}
