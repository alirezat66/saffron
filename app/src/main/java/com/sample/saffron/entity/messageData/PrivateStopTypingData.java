package com.sample.saffron.entity.messageData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import com.sample.saffron.interfacer.EventData;

public class PrivateStopTypingData implements EventData,Serializable {
    public String from;
    public String to;
    @Override
    public String toJsonString() {
        String result="";
        //do work (create json from data)
        JSONObject json = new JSONObject();
        try {
            json.put("from", from);
            json.put("to", to);
        } catch (JSONException e) {
        }
        result = json.toString();
        return result;
    }
}
