package com.sample.saffron.client;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.google.gson.JsonObject;
import com.sample.saffron.Database.CounterDb;
import com.sample.saffron.Database.DatabaseHandler;
import com.sample.saffron.Database.PrivateMessageDataBase;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.adapters.ConversationListAdapter;
import com.sample.saffron.entity.groupData.HistoryResponseData;
import com.sample.saffron.entity.messageData.DataChunkData;
import com.sample.saffron.entity.messageData.PrivateMessageData;
import com.sample.saffron.entity.userData.ConversationResponseData;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.helper.SenderFiles;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by alireza on 6/27/2017.
 */

public class UnPack {

    public static void sendEvent(JSONObject data,String sender){
        EventBus eventBus = EventBus.getDefault();
        MessengerBus message = new MessengerBus("unpackfinished",data.toString(),sender);
        eventBus.post(message);
    }
 /*   public void unpack(String sender, String value, Context ctx)
    {
        if(sender.equals("onsent")){
            try {
                JSONObject jsonobj = new JSONObject(value);
                String mid = jsonobj.getString("mid");
                DatabaseHandler dbhandler = new DatabaseHandler(ctx);

                PrivateMessageDataBase pmd =  dbhandler.GetSpecialPMessage(mid);
                dbhandler.updatePercent(mid,100);
                dbhandler.updateState(mid,"sent");
                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("privateupdate",mid);
                eventBus.post(message);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(sender.equals("ondelete")){
            try {
                JSONObject jsonobj  = new JSONObject(value);
                JSONArray answers = null;
                DatabaseHandler db = new DatabaseHandler(ctx);
                EventBus eventBus = EventBus.getDefault();
                if(jsonobj!=null){
                    answers=jsonobj.getJSONArray("messages");
                    for(int i = 0;i<answers.length();i++){
                        JSONObject c = answers.getJSONObject(i);
                        String mid = c.getString("mid");
                        db.deletePMessage(mid);
                        MessengerBus message = new MessengerBus("privatedeleted",mid);
                        eventBus.post(message);
                    }


                }

            }catch (JSONException ex){
                ex.printStackTrace();
            }
        }else if(sender.equals("onContactOnline")){
            try {
                JSONObject jsonobj = new JSONObject(value);
                String mid = jsonobj.getString("username");




                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("onlinecontact",mid);
                eventBus.post(message);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(sender.equals("onStatusRes")){
            try {
                JSONObject jsonobj = new JSONObject(value);





                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("onStatusRes",jsonobj.toString());
                eventBus.post(message);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(sender.equals("onContactOffline")){
            try {
                JSONObject jsonobj = new JSONObject(value);
                String mid = jsonobj.getString("username");




                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("offlinecontact",mid);
                eventBus.post(message);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(sender.equals("ondelivered")){
            try {
                JSONObject jsonobj = new JSONObject(value);
                String mid = jsonobj.getString("mid");
                DatabaseHandler dbhandler = new DatabaseHandler(ctx);
                dbhandler.updateState(mid,"delivered");
                PrivateMessageDataBase pmd =  dbhandler.GetSpecialPMessage(mid);

                dbhandler.updatePercent(mid,100);


                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("privateupdate",mid);
                eventBus.post(message);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(sender.equals("onseen")){
            try {
                JSONObject jsonobj = new JSONObject(value);
                String from = jsonobj.getString("from");
                String to = jsonobj.getString("to");
                DatabaseHandler dbhandler = new DatabaseHandler(ctx);
                ArrayList<PrivateMessageDataBase> unseens = dbhandler.AlllPrivateUnseen(from,to);
                ArrayList<String>mids = new ArrayList<>();
                for(int i = 0  ; i<unseens.size();i++){
                    mids.add(unseens.get(i).getMid());
                }

                dbhandler.updateSeen(from,to);

                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("privateupdate","seen",mids);
                eventBus.post(message);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(sender.equals("pmessagerec")){
            try {
                JSONObject jsonobj = new JSONObject(value);
                HistoryResponseData data = new HistoryResponseData();
                DatabaseHandler db = new DatabaseHandler(ctx);

                data.content = jsonobj.getString("content");
                data.mid = jsonobj.getString("mid");
                data.content = jsonobj.getString("content");
                data.timestamp = jsonobj.getString("timestamp");
                data.to = jsonobj.getString("to");
                data.replyto = jsonobj.getString("replyto");
                data.forwarded = jsonobj.getString("forwarded");
                data.from = jsonobj.getString("from");
                String size = jsonobj.getString("size");
                String fileName = jsonobj.getString("orgFileName");
                int sizeint = 0;
                try{
                    sizeint=Integer.parseInt(size);
                }catch (NumberFormatException ex){
                    ex.printStackTrace();
                }

                data.mtype = jsonobj.getString("mtype");
                if(data.mtype.equals("text")){
                    data.lastevent = "delivered";

                }
                else {
                    data.lastevent="delivered";
                }




                PrivateMessageDataBase pm = new PrivateMessageDataBase(data.mid,data.mtype,data.from,data.to,Long.parseLong(data.timestamp),data.content,size,data.lastevent,data.replyto,data.forwarded,100,((sizeint/100000)+1));
                pm.setFileName(fileName);
                if(!db.isExist(data.mid)){
                    db.InsertState(pm,ctx);
                }

                JSONObject packet = new JSONObject();
                JSONObject data2  = new JSONObject();
                data2.put("mid",data.getMid());
                data2.put("from",data.getTo());
                data2.put("to",data.getFrom());
                packet.put("data",data2);
                packet.put("tc",System.currentTimeMillis()/1000);
                packet.put("signature", PreferencesData.getString(ctx,"signature"));

                Security sec = new Security(ctx);
                sec.Pack(packet,"recived");

                ConversationResponseData crd= db.getConversation(data.getFrom());
                if(crd==null){
                    PreferencesData.saveBoolean(ctx,"IsNeedToUpdate",true);
                }else {
                    int unSeen= Integer.parseInt(crd.getUnseen());
                    db.updateConversation(crd.getTarget(),data.getContent(),unSeen+1);
                }
                EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("privateupdated",value,sender);
                eventBus.post(message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if(sender.equals("binaryresp")){
            EventBus eventBus = EventBus.getDefault();
            MessengerBus message = new MessengerBus("unpackfinished",value,sender);
            eventBus.post(message);
            try{
                JSONObject dt = new JSONObject(value);
                String mid = dt.getString("mid");
                DatabaseHandler db = new DatabaseHandler(ctx);
                PrivateMessageDataBase pm = db.GetSpecialPMessage(mid);
                SenderFiles sf = new SenderFiles();
                sf.SendFirst(value,pm.getMtype());
            }catch (JSONException e){
                e.printStackTrace();
            }


        }else if(sender.equals("binaryacc")){
            try {
                Log.e("myapp","binaryacc unpack");

                JSONObject dt = new JSONObject(value);
                DataChunkData dataChunkData = new DataChunkData();
                dataChunkData.mid = dt.getString("mid");
                Log.e("myapp","binaryacc mid="+dataChunkData.mid);
                DatabaseHandler db = new DatabaseHandler(ctx);
                PrivateMessageDataBase pmd = db.GetSpecialPMessage(dataChunkData.mid);
                CounterDb counterdb = db.GetSpecialCounter(dataChunkData.mid);
                int counter=0;
                if(counterdb==null){
                    counterdb = new CounterDb(dataChunkData.mid,1);
                    db.InsertCounter(counterdb);
                    counter=1;
                }else {
                    counter=counterdb.getCounter();
                    counter++;
                    db.updatecount(dataChunkData.mid,counter);
                }

                SenderFiles sf = new SenderFiles();
                sf.SendNThPart(value,counter,pmd.getMtype());
             *//*   EventBus eventBus = EventBus.getDefault();
                MessengerBus message = new MessengerBus("unpackfinished",value,sender);
                eventBus.post(message);*//*
            }catch (JSONException e){
                e.printStackTrace();
            }
        }else if(sender.equals("onProfileRes")){
            EventBus eventBus = EventBus.getDefault();
            MessengerBus message = new MessengerBus("onProfileRes",value);
            eventBus.post(message);

        }
        else {

            EventBus eventBus = EventBus.getDefault();
            MessengerBus message = new MessengerBus("unpackfinished",value,sender);
            eventBus.post(message);

        }
    }*/


}
