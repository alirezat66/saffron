package com.sample.saffron.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.sample.saffron.Database.DatabaseHandler;
import com.sample.saffron.Database.PrivateMessageDataBase;
import com.sample.saffron.activities.AppController;
import com.sample.saffron.activities.Constants;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.activities.MyMainActivity;
import com.sample.saffron.activities.SaffronEmitter;
import com.sample.saffron.entity.securityData.TimeSyncRequestData;
import com.sample.saffron.entity.userData.SignInResponseData;
import com.sample.saffron.helper.JsonToAns;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.sender.senderPackets;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by alireza on 9/1/2017.
 */

public class LoginProcess {
    public void Sync(){


            TimeSyncRequestData timeReq  = new TimeSyncRequestData();
            timeReq.timestamp  = String.valueOf((System.currentTimeMillis()/1000) );
            SaffronEmitter emitter = new SaffronEmitter();
            emitter.EmitTimeSyncRequest(timeReq);


    }
    public void OnSyncFinish(String offs){
        long offset = Long.parseLong(offs);


            senderPackets sp = new senderPackets(AppController.getCurrentContext());
            String phone = PreferencesData.getString(AppController.getCurrentContext(),"phone","");
            sp.SenderAccessRequestData(phone);

    }
    public void OnChallengeRes(String challenge){
        senderPackets senderPacket =  new senderPackets(AppController.getCurrentContext());
        String phone = PreferencesData.getString(AppController.getCurrentContext(),"phone","");

        String pass=PreferencesData.getString(AppController.getCurrentContext(),"password","");

        senderPacket.SenderChallengeRequestDate(phone, challenge, pass);
    }
    public void OnEventChallengeRes(String res){
        if(res.equals(Constants.CHALLENGERES_OK)){
            senderPackets sender = new senderPackets(AppController.getCurrentContext());
            String userName = PreferencesData.getString(AppController.getCurrentContext(),"userName","");

            JSONObject data  = sender.PackSignInStepOne(userName);

            SaffronEmitter emitter = new SaffronEmitter();
            emitter.EmitSignInRequest(data);

            /*
            Security s = new Security(AppController.getCurrentContext());
            s.Sign(data);
*/
        }else {

            EventBus eventBus = EventBus.getDefault();
            MessengerBus messengerBus = new MessengerBus("challengeNotOk","");
            eventBus.post(messengerBus);

        }

    }
    public void OnSingFinished(String signiture){
        senderPackets sp = new senderPackets(AppController.getCurrentContext());

        String challenge = PreferencesData.getString(AppController.getCurrentContext(),"challenge","");
        String salt = PreferencesData.getString(AppController.getCurrentContext(),"salt","");
        String userName = PreferencesData.getString(AppController.getCurrentContext(),"userName","");
        sp.PackSingInStepTwo(signiture,challenge,salt,userName);
    }

    public void OnSignInFinish(JSONObject data){
        try {


            Context context = AppController.getCurrentContext();
            DatabaseHandler db = new DatabaseHandler(context);
            if(data.getString("answer").equals("OK")) {
                PreferencesData.saveString(context, "sk", data.getString("sk"));
                PreferencesData.saveString(context, "iv", data.getString("iv"));
                PreferencesData.saveBoolean(context, "logedin", true);
                Intent intent = new Intent(context, MyMainActivity.class);
                Activity activity = AppController.getCurrentActivity();
                if (activity != null) {
                    activity.finish();
                }
                context.startActivity(intent);


                ArrayList<PrivateMessageDataBase> allunsend = db.unSendTextMessages(PreferencesData.getString(context, "userName"));
                for (int i = 0; i < allunsend.size(); i++) {
                    JSONObject data1 = new JSONObject();
                    JSONObject packet = new JSONObject();
                    PrivateMessageDataBase myModel = allunsend.get(i);


                    data1.put("mtype", "text");
                    data1.put("from", myModel.getSender());
                    data1.put("to", myModel.getReciever());

                    data1.put("size", myModel.getSize());
                    data1.put("content", myModel.getContent());
                    data1.put("timestamp", myModel.getTimestamp());
                    if (myModel.getReplyto().equals("")) {
                        data1.put("replyto", "");
                    } else {
                        data1.put("replyto", myModel.getReplyto());
                    }
                    data1.put("forwarded", "");

                    data1.put("mid", myModel.getMid());

                    packet.put("data", data1);
                    packet.put("tc", System.currentTimeMillis() / 1000);
                    packet.put("signature", PreferencesData.getString(context, "signature"));

                    SaffronEmitter emitter = new SaffronEmitter();
                    emitter.EmitPrivateMessage(data1);

                }
            }

          else {
                EventBus eventBus = EventBus.getDefault();
                MessengerBus messengerBus = new MessengerBus("challengeNotOk","");
                eventBus.post(messengerBus);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

}

}
