package com.sample.saffron.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sample.saffron.Database.DatabaseHandler;
import com.sample.saffron.R;
import com.sample.saffron.adapters.ConversationListAdapter;
import com.sample.saffron.entity.userData.ConversationResponseData;

import java.util.ArrayList;

/**
 * Created by alireza on 8/25/2017.
 */

public class ChoseForwardDialog extends Dialog implements AdapterView.OnItemClickListener {
    public Context context;
    ListView list;

    dialofListener listener;
    Activity activity;
    String target;
    ArrayList<ConversationResponseData> convList =new ArrayList<>();

    public ChoseForwardDialog(Context context,Activity activity,String target) {

        super(context);
        this.context = context;
        this.activity=activity;
        this.target=target;


    }

    @Override
    public void onBackPressed() {
        listener.rejected();
        super.onBackPressed();
    }

    public void setListener(dialofListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_chose_peaple);
        list = (ListView) findViewById(R.id.list);

        DatabaseHandler db = new DatabaseHandler(context);
        convList = db.AllPrivateConversation(target);
        ConversationListAdapter convAdapter = new ConversationListAdapter(activity, convList);
        list.setAdapter(convAdapter);

        list.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listener. accepted(convList.get(position).getTarget());

    }
}
