package com.sample.saffron.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sample.saffron.R;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChoseDialog extends Dialog implements
        View.OnClickListener {

   CircleImageView imgVideo,imgMusic,imgGallery,imgFiles,ivSticker;
    String dialogType,text;
    dialofListener listener;

    public Context context;
    Typeface tf;

    int order_id;

    public dialofListener getListener() {
        return listener;
    }

    public void setListener(dialofListener listener) {
        this.listener = listener;
    }

    public ChoseDialog(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        this.context = context;
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        this.text=text;
        this.dialogType=dialogType;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_chose_file);
        imgVideo = (CircleImageView) findViewById(R.id.ivVideo);
        imgMusic = (CircleImageView) findViewById(R.id.ivMusic);
        imgGallery = (CircleImageView) findViewById(R.id.ivGallery);
        imgFiles = (CircleImageView) findViewById(R.id.ivFiles);
        ivSticker = (CircleImageView) findViewById(R.id.ivSticker);
        imgVideo.setOnClickListener(this);
        imgVideo.setOnClickListener(this);
        imgMusic.setOnClickListener(this);
        imgGallery.setOnClickListener(this);
        imgFiles.setOnClickListener(this);
        ivSticker.setOnClickListener(this);
        setCancelable(true);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivVideo:
                listener.accepted(1);
                break;
            case R.id.ivMusic:
                listener.accepted(2);
                break;
            case R.id.ivGallery:
                listener.accepted(3);
                break;
            case R.id.ivFiles:
                listener.accepted(4);
                break;
            case R.id.ivSticker:
                listener.accepted(5);
                break;
            default:
                listener.rejected();
                break;
        }
      //  dismiss();
    }

    @Override
    public void onBackPressed() {
        listener.rejected();
        super.onBackPressed();
    }
}
