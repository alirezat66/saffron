package com.sample.saffron.Database;

/**
 * Created by alireza on 8/19/2017.
 */

public class CounterDb {
    String mid;
    int counter;

    public CounterDb(String mid, int counter) {
        this.mid = mid;
        this.counter = counter;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
