package com.sample.saffron.Database;

import com.sample.saffron.interfacer.EventData;

import java.io.Serializable;

public class PrivateMessageDataBase {
    public int id;
    public String mid;
    public String mtype; // mtype = text || file || photo || voice || video || location || sticker
    public String sender;
    public String reciever;
    public long timestamp;

    public String content;
    public String size;

    public String events;
    public String replyto;
    public String forwarded;
    public int percent;
    public int count;
    public String fileName;

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }


    public PrivateMessageDataBase(int id, String mid, String mtype, String sender, String reciever, long timestamp, String content, String size, String events, String replyto, String forwarded, int percent,int count,String fileName) {
        this.id = id;
        this.mid = mid;
        this.mtype = mtype;
        this.sender = sender;
        this.reciever = reciever;
        this.timestamp = timestamp;
        this.content = content;
        this.size = size;
        this.events = events;
        this.replyto = replyto;
        this.forwarded = forwarded;
        this.percent=percent;
        this.count = count;
        this.fileName=fileName;
    }

    public PrivateMessageDataBase(String mid, String mtype, String sender, String reciever, long timestamp, String content, String size, String events, String replyto, String forwarded,int percent,int count) {
        this.mid = mid;
        this.mtype = mtype;
        this.sender = sender;
        this.reciever = reciever;
        this.timestamp = timestamp;
        this.content = content;
        this.size = size;
        this.events = events;
        this.replyto = replyto;
        this.forwarded = forwarded;
        this.percent=percent;
        this.count=count;
    }

    public int getCount() {
        return count;
    }

    public int getId() {
        return id;
    }

    public String getMid() {
        return mid;
    }

    public String getMtype() {
        return mtype;
    }

    public String getSender() {
        return sender;
    }

    public String getReciever() {
        return reciever;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getContent() {
        return content;
    }

    public String getSize() {
        return size;
    }

    public String getEvents() {
        return events;
    }

    public String getReplyto() {
        return replyto;
    }

    public String getForwarded() {
        return forwarded;
    }

    public int getPercent() {
        return percent;
    }
}
