package com.sample.saffron.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.entity.messageData.PrivateMessageData;
import com.sample.saffron.entity.userData.ConversationResponseData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Azerila on 3/7/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION =4;
    private static final String DATABASE_NAME="SaffronDb";

    private static final String TABLE_PRIVATE_Message="PrivateMessages";
    private static final String KEY_PId="id";
    private static final String KEY_PMid="mid";
    private static final String KEY_PMtype="mtype";
    private static final String KEY_PSender="sender";
    private static final String KEY_PREeceiver="reviecer";
    private static final String KEY_PSendDate="senddate";
    private static final String KEY_PContent="content";
    private static final String KEY_PSize="psize";
    private static final String KEY_PEvent="pevent";
    private static final String KEY_Preply="replyto";
    private static final String KEY_Pforwarded="forwardedto";
    private static final String KEY_Percent="percent";
    private static final String KEY_Count="count";
    private static final String KEY_fileName="fileName";


    private static String Tablle_File_Counter = "counterTable";
    private static final String KEY_CounterMid="mid";
    private static final String KEY_Counter="counter";

    private static final String TABLE_CONVERSATIONS="Conversations";



    public  static final  String KEY_Ctype         ="ctype";
    public  static final  String KEY_Ctarget       ="ctarget";
    public  static final  String KEY_Cfname        ="cfname";
    public  static final  String KEY_Clname        ="clastname";
    public  static final  String KEY_CimgUrl       ="cimgurl";
    public  static final  String KEY_Cphone        ="cphone";
    public  static final  String KEY_Cstate        ="cstate";
    public  static final  String KEY_Cunseen       ="cunseen";
    public  static final  String KEY_Clastonline   ="clasonline";
    public  static final  String KEY_ClastMessage  ="clastmessage";


    private static String CREATE_TABLE_Private="CREATE TABLE "
            +TABLE_PRIVATE_Message+" ("+KEY_PId+" INTEGER PRIMARY KEY AUTOINCREMENT,"+KEY_PMid+" TEXT,"
            +KEY_PMtype+" TEXT,"+KEY_PSender+" TEXT,"+KEY_PREeceiver+" TEXT,"+KEY_PSendDate+" INTEGER,"+ KEY_PContent+" TEXT,"+KEY_PSize+" TEXT,"+KEY_PEvent+" TEXT,"+KEY_Preply+" TEXT,"+KEY_Pforwarded+" TEXT,"+KEY_Percent+" INTEGER,"+KEY_Count+" INTEGER,"+KEY_fileName+" TEXT)";
    private static String CREATE_TABLE_Counter="CREATE TABLE "
            +Tablle_File_Counter+" ("+KEY_CounterMid+" TEXT,"+KEY_Counter+" INTEGER)";

    private static String CREATE_TABLE_CONVERSATIONS="CREATE TABLE "+TABLE_CONVERSATIONS+" ("+KEY_Ctarget+" TEXT PRIMARY KEY,"+KEY_Ctype+" TEXT ,"+
            KEY_Cfname+" TEXT,"+KEY_Clname+" TEXT,"+KEY_CimgUrl+" TEXT,"+KEY_Cphone+" TEXT,"+KEY_Cstate+" TEXT,"+KEY_Cunseen+" TEXT,"+KEY_Clastonline+" TEXT,"+KEY_ClastMessage+" TEXT)";
    public DatabaseHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_Private);
        db.execSQL(CREATE_TABLE_CONVERSATIONS);
        db.execSQL(CREATE_TABLE_Counter);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRIVATE_Message);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONVERSATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + Tablle_File_Counter);

        onCreate(db);
    }
    public long InsertCounter(CounterDb counterdb){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues stateVal = new ContentValues();
        stateVal.put(KEY_CounterMid,counterdb.getMid());
        stateVal.put(KEY_Counter,counterdb.getCounter());

        return db.insert(Tablle_File_Counter,null,stateVal);
    }
    public long InsertState(PrivateMessageDataBase pm,Context context){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues stateVal = new ContentValues();
        stateVal.put(KEY_PMid,pm.getMid());
        stateVal.put(KEY_PMtype,pm.getMtype());
        stateVal.put(KEY_PSender,pm.getSender());
        stateVal.put(KEY_PREeceiver,pm.getReciever());
        long offset = PreferencesData.getLong(context,"offset",0);
        stateVal.put(KEY_PSendDate,(pm.getTimestamp()+offset));
        stateVal.put(KEY_PContent,pm.getContent());
        stateVal.put(KEY_PSize,pm.getSize());
        stateVal.put(KEY_PEvent,pm.getEvents());
        stateVal.put(KEY_Preply,pm.getReplyto());
        stateVal.put(KEY_Pforwarded,pm.getForwarded());
        stateVal.put(KEY_Percent,pm.getPercent());
        stateVal.put(KEY_Count,pm.getCount());
        stateVal.put(KEY_fileName,pm.getFileName());
        return db.insert(TABLE_PRIVATE_Message,null,stateVal);
    }

    public long InsertConversation(ConversationResponseData pm){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues stateVal = new ContentValues();
        stateVal.put(KEY_Ctype                 ,pm.getType());
        stateVal.put(KEY_Ctarget          ,pm.getTarget());
        stateVal.put(KEY_Cfname                ,pm.getFname());
        stateVal.put(KEY_Clname                   ,pm.getLname());
        stateVal.put(KEY_CimgUrl              ,pm.getImgUrl());
        stateVal.put(KEY_Cphone               ,pm.getPhone());
        stateVal.put(KEY_Cstate                   ,pm.getState());
        stateVal.put(KEY_Cunseen                      ,pm.getUnseen());
        stateVal.put(KEY_Clastonline          ,pm.getLastonline());
        stateVal.put(KEY_ClastMessage             ,pm.getLastMessage());
        return db.insert(TABLE_CONVERSATIONS,null,stateVal);
    }
    public ConversationResponseData getConversation(String target){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ConversationResponseData> poiObjects=new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_CONVERSATIONS  +" WHERE "+KEY_Ctarget +"='"+target+"'";
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    ConversationResponseData object = new ConversationResponseData(cursor.getString(0),cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9));
                    poiObjects.add(object);
                } while (cursor.moveToNext());
            }
        }catch (IllegalStateException ex){
            ex.printStackTrace();
        }
        db.close();
        if(poiObjects.size()!=0) {
            return poiObjects.get(0);
        }else {
            return null;
        }
    }
    public  boolean isExist(String mid){
        SQLiteDatabase db = this.getWritableDatabase();

        SQLiteStatement s = db.compileStatement( "select count(*) from  "+TABLE_PRIVATE_Message+" where "+KEY_PMid+" ='"+ mid+"';" );

        long count = s.simpleQueryForLong();
        db.close();
        if(count>0){
            return true;
        }else {
            return false;
        }
    }
    public ArrayList<PrivateMessageDataBase> unSendTextMessages(String from){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<PrivateMessageDataBase> poiObjects=new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_PRIVATE_Message +" WHERE (("+KEY_PSender +" = '"+from +"' AND "+KEY_PMtype+" ='text' AND " +KEY_PEvent+ " = 'save' )) ORDER BY "+KEY_PSendDate+"  ;";
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    PrivateMessageDataBase object = new PrivateMessageDataBase(cursor.getInt(0),cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getLong(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getInt(11),cursor.getInt(12),cursor.getString(13));
                    poiObjects.add(object);
                } while (cursor.moveToNext());
            }
        }catch (IllegalStateException ex){

            ex.printStackTrace();
        }
        db.close();
        return poiObjects;
    }
    public void deletePMessage(String mid){
        String selectQuery = "DELETE FROM " + TABLE_PRIVATE_Message+" WHERE "+KEY_PMid +" = '"+mid+"';";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }

    public CounterDb GetSpecialCounter(String mid){
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery  = "select  *  from  "+Tablle_File_Counter+" where "+KEY_CounterMid+" ='"+ mid+"';" ;
        ArrayList<CounterDb>poiObjects = new ArrayList<>();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    CounterDb object = new CounterDb(cursor.getString(0),cursor.getInt(1));
                    poiObjects.add(object);
                } while (cursor.moveToNext());
            }
        }catch (IllegalStateException ex){
            ex.printStackTrace();
        }
        db.close();
        if(poiObjects.size()>0){
            return poiObjects.get(0);
        }else {
            return null;
        }

    }

    public PrivateMessageDataBase GetSpecialPMessage(String mid){
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery  = "select  *  from  "+TABLE_PRIVATE_Message+" where "+KEY_PMid+" ='"+ mid+"';" ;
        ArrayList<PrivateMessageDataBase>poiObjects = new ArrayList<>();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    PrivateMessageDataBase object = new PrivateMessageDataBase(cursor.getInt(0),cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getLong(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getInt(11),cursor.getInt(12),cursor.getString(13));
                    poiObjects.add(object);
                } while (cursor.moveToNext());
            }
        }catch (IllegalStateException ex){
            ex.printStackTrace();
        }
        db.close();
        if(poiObjects.size()>0){
            return poiObjects.get(0);
        }else {
            return null;
        }

    }
    public ArrayList<PrivateMessageDataBase> AlllPrivate(String from , String to){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<PrivateMessageDataBase> poiObjects=new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_PRIVATE_Message +" WHERE (("+KEY_PSender +" = '"+from +"' AND "+KEY_PREeceiver+" ='"+to+"') OR( "+KEY_PREeceiver+" ='"+from+"' AND "+KEY_PSender+" ='"+to+"')) ORDER BY "+KEY_PSendDate+"  ;";
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    PrivateMessageDataBase object = new PrivateMessageDataBase(cursor.getInt(0),cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getLong(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getInt(11),cursor.getInt(12),cursor.getString(13));
                    poiObjects.add(object);
                } while (cursor.moveToNext());
            }
        }catch (IllegalStateException ex){
            ex.printStackTrace();
        }
        db.close();
        return poiObjects;
    }
    public ArrayList<PrivateMessageDataBase> AlllPrivateUnseen(String from , String to){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<PrivateMessageDataBase> poiObjects=new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_PRIVATE_Message +" WHERE (("+KEY_PSender +" = '"+to +"' AND "+KEY_PREeceiver+" ='"+from+"' AND " +KEY_PEvent+ " != 'seen' )) ORDER BY "+KEY_PSendDate+"  ;";
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    PrivateMessageDataBase object = new PrivateMessageDataBase(cursor.getInt(0),cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getLong(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getInt(11),cursor.getInt(12),cursor.getString(13));
                    poiObjects.add(object);
                } while (cursor.moveToNext());
            }
        }catch (IllegalStateException ex){

            ex.printStackTrace();
        }
        db.close();
        return poiObjects;
    }
    public ArrayList<ConversationResponseData> AllConversation(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ConversationResponseData> poiObjects=new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_CONVERSATIONS ;
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    ConversationResponseData object = new ConversationResponseData(cursor.getString(0),cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9));
                    poiObjects.add(object);
                } while (cursor.moveToNext());
            }
        }catch (IllegalStateException ex){
            ex.printStackTrace();
        }
        db.close();
        return poiObjects;
    }
    public ArrayList<ConversationResponseData> AllPrivateConversation(String target){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ConversationResponseData> poiObjects=new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_CONVERSATIONS +" WHERE " + KEY_Ctype+" = 'private'   AND " +KEY_Ctarget+" !='"+target+"'" ;
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    ConversationResponseData object = new ConversationResponseData(cursor.getString(0),cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9));
                    poiObjects.add(object);
                } while (cursor.moveToNext());
            }
        }catch (IllegalStateException ex){
            ex.printStackTrace();
        }
        db.close();
        return poiObjects;
    }

    public void updateState( String mid,String state){
        String selectQuery = "UPDATE " + TABLE_PRIVATE_Message+" SET "+KEY_PEvent+" = '"+state+"'  WHERE "+KEY_PMid+" = '"+mid+"' AND "+KEY_PEvent +"!='seen';";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }
    public void updateForwarded(String mid,String res){
        String selectQuery = "UPDATE " + TABLE_PRIVATE_Message+" SET "+KEY_Pforwarded+" = '"+res+"'  WHERE  "+KEY_PMid+" = '"+mid+"';";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }
    public void updateSeen( String from,String to){
        String selectQuery = "UPDATE " + TABLE_PRIVATE_Message+" SET "+KEY_PEvent+" = 'seen'  WHERE "+KEY_PSender+" = '"+to+"' AND "+KEY_PEvent +"!='seen'  AND "+KEY_PREeceiver +" = '"+from+"' ;";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }
    public void updatePercent( String mid,int percent){
        String selectQuery = "UPDATE " + TABLE_PRIVATE_Message+" SET "+KEY_Percent+" = '"+percent+"'  WHERE  "+KEY_PMid+" = '"+mid+"';";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }
    public void updatecount( String mid,int count){
        String selectQuery = "UPDATE " + Tablle_File_Counter+" SET "+KEY_Counter+" = '"+count+"'  WHERE  "+KEY_CounterMid+" = '"+mid+"';";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }
    public void updateConversation(String target, String content, int i) {
        String selectQuery = "UPDATE " + TABLE_CONVERSATIONS+" SET "+KEY_ClastMessage+" = '"+content+"', "   +KEY_Cunseen+" = "+i+" "   +   "  WHERE  "+KEY_Ctarget+" = '"+target+"';";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();

    }
}




