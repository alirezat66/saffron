package com.sample.saffron.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.sample.saffron.activities.AppController;
import com.sample.saffron.R;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.activities.SaffronEmitter;
import com.sample.saffron.adapters.ConversationListAdapter;
import com.sample.saffron.adapters.MyListViewAdapter;
import com.sample.saffron.entity.groupData.HistoryRequestData;
import com.sample.saffron.entity.userData.ContactMemberData;
import com.sample.saffron.entity.userData.ConversationResponseData;
import com.sample.saffron.helper.MessengerBus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;


public class setting extends Fragment {

    Context context ;
    View v;

    List<String> myList;
    ListView listView;
    MyListViewAdapter adapter;
    EventBus bus  = EventBus.getDefault();

    @Override
    public void onStart() {
        bus.register(this);
        SaffronEmitter saffronEmitter = new SaffronEmitter();
        ContactMemberData contactMemberData = new ContactMemberData();

        contactMemberData.username =  PreferencesData.getString(getContext(),"username");

        saffronEmitter.EmitterMemberRequest(contactMemberData);

        super.onStart();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }
    @Override
    public void onDetach() {
        bus.unregister(this);
        super.onDetach();
    }
    public void onEvent(MessengerBus event) {
        if (event.getMessage().equals("members")) {
            ConversationResponseData convResp = new ConversationResponseData();
            convResp.conversationArray = event.getJsonArray();

            if (convResp.conversationArray.length() != 0) {

                myList = new ArrayList<String>();

                for (int i = 0; i < convResp.conversationArray.length(); i++) {
                    ConversationResponseData ans = new ConversationResponseData();
                    try {
                        JSONObject data = convResp.conversationArray.getJSONObject(i);
                        myList.add(data.getString("username"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                // Add item in list
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter=new MyListViewAdapter(getContext(),R.layout.item_member,myList);
                        listView.setAdapter(adapter);
                    }
                });

                // define Choice mode for multiple  delete

            }
        }
    }
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.fragment_setting, container, false);
        context= AppController.getCurrentContext();
        makeView( v);
        return v;
    }

    @Override
    public void onResume() {
        makeView(v);
        super.onResume();
    }

    private void makeView(View v) {
        listView = (ListView)  v.findViewById(R.id.listview);
        // Pass value to MyListViewAdapter  Class adapter = new  MyListViewAdapter(this, R.layout.listview_item, myList);
        // Binds the Adapter to the ListView
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new  AbsListView.MultiChoiceModeListener() {

            @Override
            public boolean  onPrepareActionMode(ActionMode mode, Menu menu) {
                // TODO  Auto-generated method stub
                return false;
            }

            @Override
            public void  onDestroyActionMode(ActionMode mode) {
                // TODO  Auto-generated method stub

            }

            @Override
            public boolean  onCreateActionMode(ActionMode mode, Menu menu) {
                // TODO  Auto-generated method stub
                mode.getMenuInflater().inflate(R.menu.item_select_two, menu);
                return true;

            }

            @Override
            public boolean  onActionItemClicked(final ActionMode mode,
                                                MenuItem item) {
                // TODO  Auto-generated method stub
                switch  (item.getItemId()) {
                    case R.id.selectAll:
                        //
                        final int checkedCount  = myList.size();
                        // If item  is already selected or checked then remove or
                        // unchecked  and again select all
                        adapter.removeSelection();
                        for (int i = 0; i <  checkedCount; i++) {
                            listView.setItemChecked(i,   true);
                            //  listviewadapter.toggleSelection(i);
                        }
                        // Set the  CAB title according to total checked items

                        // Calls  toggleSelection method from ListViewAdapter Class

                        // Count no.  of selected item and print it
                        mode.setTitle(checkedCount  + "  Selected");
                        return true;
                    case R.id.delete:
                        SparseBooleanArray selected = adapter
                                .getSelectedIds();
                        for (int i =  (selected.size() - 1); i >= 0; i--) {
                            if  (selected.valueAt(i)) {
                                String  selecteditem = adapter
                                        .getItem(selected.keyAt(i));
                                // Remove  selected items following the ids
                                Toast.makeText(getContext(),selecteditem,Toast.LENGTH_SHORT).show();
                            }
                        }

                        // Add  dialog for confirmation to delete selected item
                        // record.
/*
                        AlertDialog.Builder  builder = new AlertDialog.Builder(
                                getActivity());
                        builder.setMessage("Do you  want to delete selected record(s)?");

                        builder.setNegativeButton("No", new  DialogInterface.OnClickListener() {

                            @Override
                            public void  onClick(DialogInterface dialog, int which) {
                                // TODO  Auto-generated method stub

                            }
                        });
                        builder.setPositiveButton("Yes", new  DialogInterface.OnClickListener() {

                            @Override
                            public void  onClick(DialogInterface dialog, int which) {
                                // TODO  Auto-generated method stub
                                SparseBooleanArray selected = adapter
                                        .getSelectedIds();
                                for (int i =  (selected.size() - 1); i >= 0; i--) {
                                    if  (selected.valueAt(i)) {
                                        String  selecteditem = adapter
                                                .getItem(selected.keyAt(i));
                                        // Remove  selected items following the ids
                                        adapter.remove(selecteditem);
                                    }
                                }

                                // Close CAB
                                mode.finish();
                                selected.clear();

                            }
                        });
                        AlertDialog alert =  builder.create();
                        alert.setIcon(R.drawable.ic_attach_24dp);// dialog  Icon
                        alert.setTitle("Confirmation"); // dialog  Title
                        alert.show();
*/
                        return true;
                    default:
                        return false;
                }

            }

            @Override
            public void  onItemCheckedStateChanged(ActionMode mode,
                                                   int position, long id, boolean checked) {
                // TODO  Auto-generated method stub
                final int checkedCount  = listView.getCheckedItemCount();
                // Set the  CAB title according to total checked items
                mode.setTitle(checkedCount  + "  Selected");
                // Calls  toggleSelection method from ListViewAdapter Class
                adapter.toggleSelection(position);
            }
        });


    }


}