package com.sample.saffron.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.test.mock.MockPackageManager;
import android.util.Base64;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.sample.saffron.activities.ActivityHistory;
import com.sample.saffron.activities.AppController;
import com.sample.saffron.R;
import com.sample.saffron.activities.GalleryUtil;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.activities.SaffronEmitter;
import com.sample.saffron.adapters.ConversationListAdapter;
import com.sample.saffron.adapters.MyListViewAdapter;
import com.sample.saffron.client.Security;
import com.sample.saffron.entity.groupData.HistoryRequestData;
import com.sample.saffron.entity.userData.ContactMemberData;
import com.sample.saffron.entity.userData.ConversationResponseData;
import com.sample.saffron.entity.userData.UpdateProfileInfoRequestData;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.struct.userStruct.StructUpdateProfileInfoRequest;
import com.sample.saffron.typo.InfoOfServer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import cn.gavinliu.android.lib.shapedimageview.ShapedImageView;
import de.greenrobot.event.EventBus;


public class Profile extends Fragment implements View.OnClickListener{

    Context context ;
    View v;
    ProgressBar progressBar;
    ShapedImageView imgProfile;
    EditText edtUserName,edtFName,edtLName,edtPhone,edtPassCode,edtRetryPassCode;
    Button btnSave;
    String b64="";
    String imgType;
    EventBus bus  = EventBus.getDefault();
    String uName,fName,lName;
    SwitchCompat mySwitch;
    private final int GALLERY_ACTIVITY_CODE = 100;
    private static final int REQUEST_CODE_PERMISSION = 2;
    String[] mPermission = {
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };
    @Override
    public void onStart() {
        bus.register(this);


        super.onStart();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }
    @Override
    public void onDetach() {
        bus.unregister(this);
        super.onDetach();
    }
    public void onEvent(MessengerBus event) {
        if (event.getMessage().equals("onProfileResProfile")) {


                try {
                    final JSONObject object = new JSONObject(event.getSender());
                    final String username = object.getString("username");
                    final String firstname = object.getString("firstname");
                    final String lastname = object.getString("lastname");
                    final String phone = object.getString("phone");
                    final String profileimage = object.getString("imageuri");
                    String password="";
                    final boolean NullPassword = object.isNull("Passcode");
                    if(!NullPassword){
                        password= object.getString("Passcode");
                    }
                    uName=username;
                    fName=firstname;
                    lName=lastname;
                    final String finalPassword = password;
                    AppController.getCurrentActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(NullPassword){
                                mySwitch.setChecked(false);
                                edtPassCode.setVisibility(View.GONE);
                                edtRetryPassCode.setVisibility(View.GONE);
                            }else {
                                mySwitch.setChecked(true);
                                edtPassCode.setText(finalPassword);
                                edtRetryPassCode.setText(finalPassword);
                            }
                            progressBar.setVisibility(View.GONE);

                            edtFName.setText(firstname);
                            edtLName.setText(lastname);
                            edtPhone.setText(phone);
                            edtUserName.setText(username);
                            String img = InfoOfServer.MultiMediaIpServer+"/uploads/"+profileimage;
                            Glide.with(getContext()).load(img).error(R.drawable.ic_person).into(imgProfile);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }



        }else if(event.getMessage().equals("updateCompleted")) {
            try {
                AppController.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
                JSONObject object = new JSONObject(event.getSender());
                JSONArray array = object.getJSONArray("answers");
                for(int i = 0 ; i<array.length();i++){
                    JSONObject obj  = array.getJSONObject(i);
                    if(obj.getString("result").equals("OK")){
                        if(obj.getString("utype").equals("firstname")){
                            Toast.makeText(context, "first name updated completely", Toast.LENGTH_SHORT).show();
                        }else if(obj.getString("utype").equals("lastname")){
                            Toast.makeText(context, "last name updated completely", Toast.LENGTH_SHORT).show();

                        }else if(obj.getString("utype").equals("username")){
                            Toast.makeText(context, "username updated completely", Toast.LENGTH_SHORT).show();

                        }else if(obj.getString("utype").equals("image")){
                            Toast.makeText(context, "image updated completely", Toast.LENGTH_SHORT).show();

                        }
                    }else {
                        if(obj.getString("utype").equals("firstname")){
                            Toast.makeText(context, "some problem has accrued in update first name ", Toast.LENGTH_SHORT).show();
                        }else if(obj.getString("utype").equals("lastname")){
                            Toast.makeText(context, "some problem has accrued in update last name ", Toast.LENGTH_SHORT).show();

                        }else if(obj.getString("utype").equals("username")){
                            Toast.makeText(context, "username that u chosen for your profile , chosen by other person ", Toast.LENGTH_SHORT).show();

                        }else if(obj.getString("utype").equals("image")){
                            Toast.makeText(context, "some problem has accrued in update image ", Toast.LENGTH_SHORT).show();

                        }
                        if(mySwitch.isChecked()){
                            PreferencesData.saveString(context,"password",edtPassCode.getText().toString());
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.fragment_profile, container, false);
        context= AppController.getCurrentContext();
        makeView( v);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void makeView(View v) {

        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        progressBar.setProgress(60);
        progressBar.setVisibility(View.VISIBLE);
        edtFName = (EditText) v.findViewById(R.id.edtFName);
        edtLName = (EditText) v.findViewById(R.id.edtLName);
        edtUserName  = (EditText) v.findViewById(R.id.edtUserName);
        edtPhone  = (EditText) v.findViewById(R.id.edtPhone);
        edtPassCode  = (EditText) v.findViewById(R.id.edtpasscode);
        edtRetryPassCode  = (EditText) v.findViewById(R.id.retryEdtpasscode);
        mySwitch = (SwitchCompat) v.findViewById(R.id.Switch);
        btnSave = (Button) v.findViewById(R.id.btnSave);
        imgProfile = (ShapedImageView) v.findViewById(R.id.img_profile);
        imgProfile.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    edtPassCode.setVisibility(View.VISIBLE);
                    edtRetryPassCode.setVisibility(View.VISIBLE);
                }else {
                    edtPassCode.setVisibility(View.GONE);
                    edtRetryPassCode.setVisibility(View.GONE);
                }
            }
        });
        SendProfileRequest();
    }

    private void SendProfileRequest(){
        JSONObject packet = new JSONObject();
        JSONObject data = new JSONObject();
        try{
            PreferencesData.saveString(AppController.getCurrentContext(),"profileRequest","profile");
            data.put("username",  PreferencesData.getString(context, "userName"));


            packet.put("data", data);
            packet.put("tc", System.currentTimeMillis() / 1000);
            packet.put("signature", PreferencesData.getString(context, "signature"));
            SaffronEmitter emitter = new SaffronEmitter();
            emitter.EmitProfileInfoRequest(data);
           /* Security sec = new Security(context);
            sec.Pack(packet, "profileReq");*/

        }
        catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnSave){
            ArrayList<StructUpdateProfileInfoRequest> arr = new ArrayList<>();
            if(!lName.equals(edtLName.getText().toString())){
                StructUpdateProfileInfoRequest req = new StructUpdateProfileInfoRequest();
                req.utype="lastname";
                req.value = edtLName.getText().toString();
                arr.add(req);
            }else if(!fName.equals(edtFName.getText().toString())){
                StructUpdateProfileInfoRequest req = new StructUpdateProfileInfoRequest();
                req.utype="firstname";
                req.value = edtFName.getText().toString();
                arr.add(req);
            }else if(!edtUserName.getText().toString().equals(uName)){
                StructUpdateProfileInfoRequest req = new StructUpdateProfileInfoRequest();
                req.utype="username";
                req.value = edtUserName.getText().toString();
                arr.add(req);
            }else if(!b64.equals("")){
                StructUpdateProfileInfoRequest req = new StructUpdateProfileInfoRequest();
                req.utype="image";
                req.value = b64;
                req.imageformat=imgType;
                arr.add(req);
            }

            if(mySwitch.isChecked()){
                if(edtPassCode.getText().toString().equals("")){
                  Toast.makeText(context,"please set your pass or off password",Toast.LENGTH_SHORT).show();
                    return;
                }else if(!edtPassCode.getText().toString().equals(edtRetryPassCode.getText().toString())) {
                    Toast.makeText(context,"pass and retry not same.please set your pass or off password",Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    StructUpdateProfileInfoRequest req = new StructUpdateProfileInfoRequest();
                    req.utype="passcode";
                    req.value = edtPassCode.getText().toString();
                    arr.add(req);
                }
            }else {
                StructUpdateProfileInfoRequest req = new StructUpdateProfileInfoRequest();
                req.utype="passcode";
                req.value = null;
                arr.add(req);
            }
            if(arr.size()>0){
                progressBar.setVisibility(View.VISIBLE);
                SaffronEmitter emitter = new SaffronEmitter();
                UpdateProfileInfoRequestData data  = new UpdateProfileInfoRequestData();
                data.updates=arr;
                emitter.EmitUpdateProfileInfoRequest(data);
            }
        }else if(v.getId()==R.id.img_profile){
            if (checkWriteExternalPermission() && checkReadExternalPermission()) {
                Intent gallery_Intent = new Intent(context, GalleryUtil.class);
                startActivityForResult(gallery_Intent, GALLERY_ACTIVITY_CODE);

            } else {

                ActivityCompat.requestPermissions(AppController.getCurrentActivity(),
                        new String[]{mPermission[0], mPermission[1]}, REQUEST_CODE_PERMISSION);
            }
        }
    }


    private boolean checkWriteExternalPermission() {

        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private boolean checkReadExternalPermission() {

        String permission = "android.permission.READ_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Req Code", "" + requestCode);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length == 2 &&
                    grantResults[0] == MockPackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == MockPackageManager.PERMISSION_GRANTED) {

                Intent gallery_Intent = new Intent(context, GalleryUtil.class);

                startActivityForResult(gallery_Intent, GALLERY_ACTIVITY_CODE);

            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String picturePath = "";
        if (requestCode == GALLERY_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                picturePath = data.getStringExtra("picturePath");
                final String finalPicturePath = picturePath;
                AppController.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.with(AppController.getCurrentContext()).load(finalPicturePath).into(imgProfile);
                    }
                });
                if(picturePath.toLowerCase().contains(".jpg")||picturePath.toLowerCase().contains(".jpeg")){
                    imgType="jpg";
                }else {
                    imgType="png";
                }
                Bitmap bm = BitmapFactory.decodeFile(picturePath);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//added lines
                bm.recycle();
                bm = null;
//added lines
                byte[] b = baos.toByteArray();
                b64= Base64.encodeToString(b, Base64.DEFAULT);

            }
        }
    }
}