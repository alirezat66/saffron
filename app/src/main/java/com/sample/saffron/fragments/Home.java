package com.sample.saffron.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sample.saffron.Database.DatabaseHandler;
import com.sample.saffron.activities.ActivityHistory;
import com.sample.saffron.activities.ActivityHistoryGroup;
import com.sample.saffron.activities.AppController;
import com.sample.saffron.R;
import com.sample.saffron.activities.Constants;
import com.sample.saffron.activities.PreferencesData;
import com.sample.saffron.activities.SaffronEmitter;
import com.sample.saffron.adapters.ConversationListAdapter;

import com.sample.saffron.client.Security;
import com.sample.saffron.entity.userData.ConversationResponseData;
import com.sample.saffron.helper.MessengerBus;
import com.sample.saffron.typo.InfoOfServer;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;


public class Home extends Fragment {
    CircularProgressView test;
    Context context ;
    ListView list;
    boolean isprivate=false;
    private RotateLoading rotateLoading;
    ConversationListAdapter convAdapter;
    ArrayList<ConversationResponseData>convList = new ArrayList<>();
    JSONObject packet = new JSONObject();
    JSONObject data = new JSONObject();
    DatabaseHandler db;
    View v;
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.fragment_home, container, false);

        context= AppController.getCurrentContext();
        Log.e("myapp","homecreate");
        makeView(v);
        Log.e("myapp","view maked");
        return v;
    }

    EventBus bus  = EventBus.getDefault();



    private void makeView(View v) {
        list = (ListView) v.findViewById(R.id.list);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    if(convList.get(position).getType().equals("private")){

                        DatabaseHandler db = new DatabaseHandler(context);

                        Intent intent = new Intent(context,ActivityHistory.class);
                        intent.putExtra("targetname", convList.get(position).getFname()+"  " +convList.get(position).getLname());
                        intent.putExtra("to",  convList.get(position).getTarget());
                        intent.putExtra("image",convList.get(position).getImgUrl());
                        startActivity(intent);

                    }


            }
        });



    }
    @Override
    public void onStart() {

        rotateLoading = (RotateLoading) v.findViewById(R.id.rotateloading);

        PreferencesData.saveBoolean(getContext(),"inhistory",false);
        bus.register(this);
        convList = new ArrayList<>();


        db = new DatabaseHandler(context);
        convList = db.AllConversation();
        if(convList.size()==0){



            rotateLoading.start();
            try {

                packet.put("data",data);
                packet.put("tc",System.currentTimeMillis()/1000);
                packet.put("signature",PreferencesData.getString(getContext(),"signature"));
                SaffronEmitter emitter  = new SaffronEmitter();
                emitter.EmitConversationRequest(packet);
            } catch (JSONException e) {

                e.printStackTrace();
            }
        }else {
            rotateLoading.stop();
            convAdapter = new ConversationListAdapter(getActivity(),convList);
            list.setAdapter(convAdapter);
        }

/*
        saffronEmitter.EmitConversationRequest(conversationRequestData);
*/
        super.onStart();
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onStop() {
        Log.e("myapp","I am stop");
        bus.unregister(this);
        super.onStop();
    }

    @Override
    public void onDetach() {
        bus.unregister(this);
        super.onDetach();
    }

    @Override
    public void onResume() {
        JSONObject packet = new JSONObject();
        JSONObject data = new JSONObject();
        try{

            data.put("username",  PreferencesData.getString(context, "userName"));


          /*  packet.put("data", data);
            packet.put("tc", System.currentTimeMillis() / 1000);
            packet.put("signature", PreferencesData.getString(context, "signature"));*/
            PreferencesData.saveString(AppController.getCurrentContext(),"profileRequest","home");
            SaffronEmitter emitter = new SaffronEmitter();
            emitter.EmitProfileInfoRequest(data);

        }
        catch (JSONException ex){
            ex.printStackTrace();
        }
        super.onResume();
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(Constants.LOGTAG, "RRRRRRRRRRRRRRRRRRRRRRRRRRR");
            Log.d(Constants.LOGTAG, intent.getAction());
            if (intent.getAction().equals("EventConversationRes")) {

                }

            }
            }

    public void onEvent(MessengerBus event) {

        String a = "start";
         if (event.getMessage().equals("conversations")) {

                 try {
                     JSONObject object = new JSONObject(event.getMes());

                     JSONArray jsonArray = object.getJSONArray("conversations");
                     for (int i = 0; i < jsonArray.length(); i++) {
                         ConversationResponseData ans = new ConversationResponseData();

                         JSONObject data = jsonArray.getJSONObject(i);
                         ans.type = data.getString("type");
                         if(ans.type.equals("private")){
                             ans.fname = data.getString("firstname");
                             ans.lname = data.getString("lastname");
                         }else {
                            ans.fname=data.getString("firstname");
                             ans.lname="";
                         }
                         ans.imgUrl = data.getString("imageuri");
                         ans.phone = data.getString("phone");
                         ans.target = data.getString("target");
                         ans.unseen="0";//data.getString("unseen");
                         JSONObject lasobj = data.getJSONObject("lastmessage");
                         String mtype = lasobj.getString("mtype");
                         if(mtype.equals("text")){
                            ans.lastMessage = lasobj.getString("content");
                             if(ans.lastMessage.length()>20){
                                 ans.lastMessage=ans.lastMessage.substring(0,17);
                                 ans.lastMessage+="...";
                             }
                         }else {
                             ans.lastMessage = mtype;
                         }
                         ans.lastonline=lasobj.getString("timestamp");



                         db.InsertConversation(ans);
                         convList.add(ans);
                     }
                     getActivity().runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             ConversationListAdapter adapter = new ConversationListAdapter(getActivity(),convList);
                             list.setAdapter(adapter);
                             rotateLoading.stop();
                             rotateLoading.setVisibility(View.GONE);
                         }
                     });
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }





        }else if(event.getMessage().equals("messagesapp")) {
            if(!PreferencesData.getBoolean(getContext(),"inhistory",false)){
                Intent intent=null;
                if(isprivate){
                    intent = new Intent(context, ActivityHistory.class);
                }else {
                    intent = new Intent(context, ActivityHistoryGroup.class);
                }

                intent.putExtra("jsonstring",event.getJsonArray().toString());
                intent.putExtra("targetname",PreferencesData.getString(getContext(),"target"));
                intent.putExtra("to",PreferencesData.getString(getContext(),"to"));
                getActivity().startActivity(intent);

            }

        }else if(event.getMessage().equals("privateupdated")) {

             db = new DatabaseHandler(context);
             convList.clear();
             convList = db.AllConversation();
             convAdapter = new ConversationListAdapter(getActivity(),convList);
             list.setAdapter(convAdapter);
        }
         else if(event.getMessage().equals("packfinish")){
             if(event.getSender().equals("conversations")){
                 SaffronEmitter emitter  = new SaffronEmitter();
                 JSONObject object = new JSONObject();
                 try {
                     object.put("packet",event.getMes());
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
                 emitter.EmitConversationRequest(object);
             }

         }



    }

}